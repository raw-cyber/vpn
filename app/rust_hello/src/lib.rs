use jni::{
    JNIEnv,
    sys::jstring,
    objects::{JClass},
};


#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn Java_com_example_vpn_1over_1ws_VpnActivity_stringFromJNI(
    env: JNIEnv,
    _class: JClass,
) -> jstring {
    // Create a Rust string containing "Hello, World!"
    let hello_world = "Hello, World!".to_string();

    // Convert the Rust string to a Java string
    let java_string = env.new_string(hello_world).unwrap();

    // Return the Java string to Java code
    java_string.into_inner()
}