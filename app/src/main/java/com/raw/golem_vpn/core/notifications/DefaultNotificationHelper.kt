package com.raw.golem_vpn.core.notifications

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.raw.golem_vpn.R
import com.raw.golem_vpn.VpnActivity
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

private const val VPN_NOTIFICATION_CHANNEL_ID = "RawVpnOverWS"

@Singleton
class DefaultNotificationHelper @Inject constructor(
    @ApplicationContext private val applicationContext: Context
) : NotificationHelper {
    private val notificationManager = NotificationManagerCompat.from(applicationContext)
    private val name: String = "RawChannel"
    private val description = "Golem VPN notification channel"
    private val reqCode = 1

    val mConfigureIntent: PendingIntent = PendingIntent.getActivity(
        applicationContext,
        reqCode,
        Intent(applicationContext, VpnActivity::class.java),
        PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
    )

    init {
        createNotificationChannels()
    }

    private fun createNotificationChannels() {
        val vpnServiceChannel = NotificationChannelCompat.Builder(
            VPN_NOTIFICATION_CHANNEL_ID,
            NotificationManagerCompat.IMPORTANCE_DEFAULT
        )
            .setName(name)
            .setDescription(description)
            .build()

        notificationManager.createNotificationChannelsCompat(listOf(vpnServiceChannel))
    }

    override fun getBaseTimerServiceNotification(): NotificationCompat.Builder {
        return NotificationCompat.Builder(applicationContext, VPN_NOTIFICATION_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_vpn)
            .setContentIntent(mConfigureIntent)
            .setSilent(true)
            .setOnlyAlertOnce(true)
    }

    override fun updateVpnStatus(title: String, content: String): Notification =
        getBaseTimerServiceNotification()
            .setContentTitle(title)
            .setContentText(content)
            .build()
}