package com.raw.golem_vpn.core.vpn

import com.raw.golem_vpn.utils.RawLog
import java.io.IOException

const val DEFAULT_BUFFER_SIZE = 2048
const val YAGNA_SERVICE_DELAY = 3000L // 2 second

const val YAGNA_APPKEY = "38860491229837815842"

class RawYagnaDaemon(
    private val mService: RawVpnService,
) : Runnable {

    private var mCommandManagerConstructor: CommandManagerConstructor =
        CommandManagerConstructor(mService.applicationContext)

    private var process: Process? = null

    /**
     * Callback interface to let the [RawVpnService] know about state of yagna daemon.
     */
    interface OnEstablishListener {
        fun onYagnaStarted(mService: RawVpnService)
        fun onYagnaStopped(mService: RawVpnService)
    }

    private var mOnEstablishListener: OnEstablishListener? = null

    fun setOnEstablishListener(listener: OnEstablishListener) {
        mOnEstablishListener = listener
    }

    override fun run() {
        try {
            runYagnaDaemon()
        } catch (e: IOException) {
            RawLog.d(tag, "IOException " + e.message)
            e.printStackTrace()
        } catch (e: InterruptedException) {
            RawLog.d(tag, "InterruptedException " + e.message)
        } finally {
            RawLog.d(tag, "Stop yagna")
            stopYagnaDaemon()
        }
    }

    private fun runYagnaDaemon() {
        val command = mCommandManagerConstructor.getRunServiceCommand()
        RawLog.d(tag, "Yagna command: $command")
        process = Runtime.getRuntime().exec(command, mCommandManagerConstructor.getEnvsConfig())
        Thread.sleep(YAGNA_SERVICE_DELAY)

        val outputThread = Thread {
            try {
                process!!.inputStream.bufferedReader().useLines { lines ->
                    lines.forEach { line ->
                        println(line)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val errorThread = Thread {
            try {
                process!!.errorStream.bufferedReader().useLines { lines ->
                    lines.forEach { line ->
                        println(line)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        if (!isYagnaDaemonRunning()) {
            throw IOException("Yagna daemon is not running")
        }

        mOnEstablishListener?.onYagnaStarted(mService)
        outputThread.start()
        errorThread.start()
        while (!Thread.currentThread().isInterrupted) {
            Thread.sleep(100)
        }
    }

    private fun isYagnaDaemonRunning(): Boolean {
        return process != null && process!!.isAlive
    }

    private fun stopYagnaDaemon() {
        while (process != null && process!!.isAlive) {
            process!!.destroy()
            RawLog.i(tag, "Trying to stop yagna daemon")
            Thread.sleep(100)
        }
        mOnEstablishListener?.onYagnaStopped(mService)
    }

    private val tag: String
        get() = RawYagnaDaemon::class.java.simpleName
}