package com.raw.golem_vpn.core.vpn.ws

import com.raw.golem_vpn.utils.RawLog
import org.java_websocket.WebSocket
import org.java_websocket.drafts.Draft
import org.java_websocket.enums.CloseHandshakeType
import org.java_websocket.enums.HandshakeState
import org.java_websocket.enums.Opcode
import org.java_websocket.enums.ReadyState
import org.java_websocket.exceptions.*
import org.java_websocket.framing.CloseFrame
import org.java_websocket.framing.Framedata
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.handshake.Handshakedata
import org.java_websocket.handshake.ServerHandshake
import java.io.IOException
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import javax.net.ssl.SSLSession

class CustomWebSocketImpl(var webSocket: CustomWebSocketClient, var draft: CustomDraft) :
    WebSocket {

    private var flushandclosestate: Boolean = false
    private var readyState: ReadyState = ReadyState.NOT_YET_CONNECTED
    lateinit var handshakeRequest: ClientHandshake
    private var closemessage: String? = null
    private var closecode: Int? = null
    private var closedremotely: Boolean? = null


    fun write(bufs: List<ByteBuffer>) {
        for (b in bufs) {
            write(b)
        }
    }

    private fun write(buf: ByteBuffer) {
        while (buf.hasRemaining()) {
//            println("Sending packets")
            webSocket.getSocketChannel()?.write(buf)
        }
    }

    fun decode(socketBuffer: ByteBuffer): List<Framedata>? {
//        assert(socketBuffer.hasRemaining())
//        log.trace(
//            "process({}): ({})", socketBuffer.remaining(),
//            if (socketBuffer.remaining() > 1000) "too big to display" else String(
//                socketBuffer.array(),
//                socketBuffer.position(),
//                socketBuffer.remaining()
//            )
//        )
        if (readyState != ReadyState.NOT_YET_CONNECTED) {
            if (readyState == ReadyState.OPEN) {
                return decodeFrames(socketBuffer)
            }
        } else {
//            decodeHandshake(socketBuffer)
//            if (decodeHandshake(socketBuffer) && !isClosing && !isClosed) {
//                assert(
//                    tmpHandshakeBytes.hasRemaining() != socketBuffer.hasRemaining() || !socketBuffer.hasRemaining() // the buffers will never have remaining bytes at the same time
//                )
//                if (socketBuffer.hasRemaining()) {
//                    decodeFrames(socketBuffer)
//                } else if (tmpHandshakeBytes.hasRemaining()) {
//                    decodeFrames(tmpHandshakeBytes)
//                }
//            }
        }
        return null
    }

    fun decodeHandshake(
        socketBufferNew: ByteBuffer,
    ): Boolean {
        val socketBuffer: ByteBuffer = socketBufferNew
//        TODO (Add better buffer handling later)
        try {
            val handshakestate: HandshakeState
            try {
                val tmphandshake: Handshakedata = draft.translateHandshakeHttp(
                    socketBuffer
                )
                if (tmphandshake !is ServerHandshake) {
                    RawLog.d("RawVPN", "Closing due to protocol error: wrong http function")
//                    flushAndClose(CloseFrame.PROTOCOL_ERROR, "wrong http function", false)
                    return false
                }
                handshakestate = draft.acceptHandshakeAsClient(handshakeRequest, tmphandshake)
                if (handshakestate == HandshakeState.MATCHED) {
                    try {
                        RawLog.d("RawVPN", "Opening connection")
                    } catch (e: InvalidDataException) {
                        RawLog.d("RawVPN", "InvalidDataException")
//                        flushAndClose(e.closeCode, e.message, false)
                        return false
                    } catch (e: RuntimeException) {
                        RawLog.d("RawVPN", "RuntimeException")
//                        RawLog.error("Closing since client was never connected", e)
//                        wsl.onWebsocketError(this, e)
//                        flushAndClose(CloseFrame.NEVER_CONNECTED, e.message, false)
                        return false
                    }
                    open()
                    return true
                } else {
                    RawLog.d("RawVPN", "Closing due to protocol error: draft refuses handshake")
//                    close(CloseFrame.PROTOCOL_ERROR, "Refuse handshake", false)
                }
            } catch (e: InvalidHandshakeException) {
                RawLog.d("RawVPN", "InvalidHandshakeException " + e.message)
//                close(e.closeCode, e.message!!, false)
            }
        } catch (e: IncompleteHandshakeException) {
            RawLog.d("RawVPN", "IncompleteHandshakeException " + e.message)
//            if (tmpHandshakeBytes.capacity() == 0) {
//                socketBuffer.reset()
//                var newsize = e.preferredSize
//                if (newsize == 0) {
//                    newsize = socketBuffer.capacity() + 16
//                } else {
//                    assert(e.preferredSize >= socketBuffer.remaining())
//                }
//                tmpHandshakeBytes = ByteBuffer.allocate(newsize)
//                tmpHandshakeBytes.put(socketBufferNew)
//                // tmpHandshakeBytes.flip();
//            } else {
//                tmpHandshakeBytes.position(tmpHandshakeBytes.limit())
//                tmpHandshakeBytes.limit(tmpHandshakeBytes.capacity())
//            }
        }
        return false
    }

    private fun open() {
        RawLog.d(getTag(), "open using draft: $draft")
        readyState = ReadyState.OPEN
    }

    private fun decodeFrames(socketBuffer: ByteBuffer): List<Framedata>? {
        val frames: List<Framedata>
        try {
            frames = draft.translateFrame(socketBuffer)
            for (f in frames) {
                draft.processFrame(this, f)
            }
            return frames.filter { it.opcode == Opcode.BINARY || it.opcode == Opcode.PING || it.opcode == Opcode.PONG }
        } catch (e: LimitExceededException) {
            if (e.limit == Int.MAX_VALUE) {
                RawLog.e(getTag(), "Closing due to invalid size of frame " + e.message)
            }
            close(e)
        } catch (e: InvalidDataException) {
            RawLog.e(getTag(), "Closing due to invalid data in frame " + e.message)
//            close(e)
        }
        return null
    }

    fun close(code: Int, message: String, remote: Boolean) {
        if (readyState != ReadyState.CLOSING && readyState != ReadyState.CLOSED) {
            if (readyState == ReadyState.OPEN) {
                if (code == CloseFrame.ABNORMAL_CLOSE) {
                    assert(!remote)
                    readyState = ReadyState.CLOSING
                    flushAndClose(code, message, false)
                    return
                }
                if (draft.getCloseHandshakeType() != CloseHandshakeType.NONE) {
                    try {
                        if (isOpen) {
                            val closeFrame = CloseFrame()
                            closeFrame.setReason(message)
                            closeFrame.setCode(code)
                            closeFrame.isValid()
                            sendFrame(closeFrame)
                        }
                    } catch (e: InvalidDataException) {
                        RawLog.i(getTag(), "generated frame is invalid " + e.message)
                        flushAndClose(
                            CloseFrame.ABNORMAL_CLOSE,
                            "generated frame is invalid",
                            false
                        )
                    }
                }
                flushAndClose(code, message, remote)
            } else if (code == CloseFrame.FLASHPOLICY) {
                assert(remote)
                flushAndClose(CloseFrame.FLASHPOLICY, message, true)
            } else if (code == CloseFrame.PROTOCOL_ERROR) { // this endpoint found a PROTOCOL_ERROR
                flushAndClose(code, message, remote)
            } else {
                flushAndClose(CloseFrame.NEVER_CONNECTED, message, false)
            }
            readyState = ReadyState.CLOSING
            return
        }
    }

    @Synchronized
    fun flushAndClose(code: Int, message: String, remote: Boolean) {
        if (flushandclosestate) {
            return
        }
        closecode = code
        closemessage = message
        closedremotely = remote
        flushandclosestate = true
//        try {
//            wsl.onWebsocketClosing(this, code, message, remote)
//        } catch (e: java.lang.RuntimeException) {
//            log.error("Exception in onWebsocketClosing", e)
//            wsl.onWebsocketError(this, e)
//        }
//        if (draft != null) draft.reset()
    }

    override fun close(code: Int, message: String?) {
        close(code, message ?: "", false)
    }

    override fun close(code: Int) {
        close(code, "", false)
    }

    private fun close(e: InvalidDataException) {
        close(e.closeCode, e.message!!, false)
    }

    override fun close() {
        close(CloseFrame.NORMAL)
    }

    override fun closeConnection(code: Int, message: String?) {
        closeConnection(code, "", false)
    }

    private fun closeConnection(code: Int, remote: Boolean) {
        closeConnection(code, "", remote)
    }

    fun closeConnection(code: Int, message: String?, remote: Boolean) {
        if (readyState == ReadyState.CLOSED) {
            return
        }
        if (readyState == ReadyState.OPEN) {
            if (code == CloseFrame.ABNORMAL_CLOSE) {
                readyState = ReadyState.CLOSING
            }
        }
        try {
            webSocket.getSocketChannel()?.close()
        } catch (e: IOException) {
            if (e.message != null && e.message == "Broken pipe") {
                RawLog.i(
                    getTag(),
                    "Caught IOException: Broken pipe during closeConnection ${e.message}"
                )
            } else {
                RawLog.e(getTag(), "Exception during channel.close ${e.message}")
            }
        }
        readyState = ReadyState.CLOSED
    }

    override fun send(text: String) {
        send(draft.createFrames(text, true))
    }

    override fun send(bytes: ByteBuffer) {
        send(draft.createFrames(bytes, true))
    }

    override fun send(bytes: ByteArray?) {
        TODO("Not yet implemented")
    }

    private fun send(frames: Collection<Framedata>?) {
        if (!isOpen) {
            throw WebsocketNotConnectedException()
        }
        requireNotNull(frames)
        val outgoingFrames = ArrayList<ByteBuffer>()
        for (f in frames) {
            outgoingFrames.add(draft.createBinaryFrame(f))
        }
        write(outgoingFrames)
    }

    override fun sendFrame(framedata: Framedata) {
        send(listOf(framedata))
    }

    override fun sendFrame(frames: MutableCollection<Framedata>?) {
        TODO("Not yet implemented")
    }

    fun eot() {
        if (readyState == ReadyState.NOT_YET_CONNECTED) {
            closeConnection(CloseFrame.NEVER_CONNECTED, true)
        } else if (flushandclosestate) {
            closeConnection(closecode ?: CloseFrame.NORMAL, closemessage, closedremotely ?: false)
        } else if (draft.getCloseHandshakeType() == CloseHandshakeType.NONE) {
            closeConnection(CloseFrame.NORMAL, true)
        } else if (draft.getCloseHandshakeType() == CloseHandshakeType.ONEWAY) {
            closeConnection(CloseFrame.NORMAL, true)
        } else {
            closeConnection(CloseFrame.ABNORMAL_CLOSE, true)
        }
    }

    override fun sendPing() {
        TODO("Not yet implemented")
    }

    override fun sendFragmentedFrame(op: Opcode?, buffer: ByteBuffer?, fin: Boolean) {
        TODO("Not yet implemented")
    }

    override fun hasBufferedData(): Boolean {
        TODO("Not yet implemented")
    }

    override fun getRemoteSocketAddress(): InetSocketAddress {
        TODO("Not yet implemented")
    }

    override fun getLocalSocketAddress(): InetSocketAddress {
        TODO("Not yet implemented")
    }

    override fun isOpen(): Boolean {
        return readyState == ReadyState.OPEN
    }

    override fun isClosing(): Boolean {
        return readyState == ReadyState.CLOSING
    }

    override fun isFlushAndClose(): Boolean {
        TODO("Not yet implemented")
    }

    override fun isClosed(): Boolean {
        return readyState == ReadyState.CLOSED
    }

    override fun getDraft(): Draft {
        TODO("Not yet implemented")
    }

    override fun getReadyState(): ReadyState {
        return readyState
    }

    override fun getResourceDescriptor(): String {
        TODO("Not yet implemented")
    }

    override fun <T : Any?> setAttachment(attachment: T) {
        TODO("Not yet implemented")
    }

    override fun <T : Any?> getAttachment(): T {
        TODO("Not yet implemented")
    }

    override fun hasSSLSupport(): Boolean {
        TODO("Not yet implemented")
    }

    override fun getSSLSession(): SSLSession {
        TODO("Not yet implemented")
    }

    private fun getTag(): String {
        return CustomWebSocketImpl::class.java.simpleName
    }
}