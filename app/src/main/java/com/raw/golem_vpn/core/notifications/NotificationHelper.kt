package com.raw.golem_vpn.core.notifications

import android.app.Notification
import androidx.core.app.NotificationCompat

interface NotificationHelper {
    fun getBaseTimerServiceNotification(): NotificationCompat.Builder
    fun updateVpnStatus(title: String, content: String): Notification
}