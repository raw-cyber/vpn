package com.raw.golem_vpn.core.vpn.ws

import com.raw.golem_vpn.core.vpn.YAGNA_APPKEY
import com.raw.golem_vpn.utils.RawLog
import io.ktor.websocket.*
import okhttp3.internal.wait
import org.java_websocket.WebSocket
import org.java_websocket.WebSocketImpl
import org.java_websocket.drafts.Draft
import org.java_websocket.enums.Opcode
import org.java_websocket.enums.ReadyState
import org.java_websocket.enums.Role
import org.java_websocket.framing.CloseFrame
import org.java_websocket.framing.Framedata
import org.java_websocket.framing.PongFrame
import org.java_websocket.handshake.HandshakeImpl1Client
import java.io.IOException
import java.net.InetSocketAddress
import java.net.URI
import java.nio.ByteBuffer
import java.nio.channels.SocketChannel
import javax.net.ssl.SSLSession

class CustomWebSocketClient(var serverUri: URI) : WebSocket {
    private val role: Role = Role.CLIENT
    private var draft: CustomDraft6455 = CustomDraft6455(role)
    private var engine: CustomWebSocketImpl = CustomWebSocketImpl(this, draft)
    private var channelFactory: WsSocketChannel = WsSocketChannel()
    private var channel: SocketChannel? = null

    fun openConnection() {
        channel = channelFactory.createSocketChannel(serverUri.host, serverUri.port)
        sendHandshake()

        val buffer: ByteBuffer = ByteBuffer.allocate(1024)
        var handshakeProcessed = false
        RawLog.d(getTag(), "Establishing connection")
        do {
            if (channel!!.read(buffer) > 0) {
                buffer.flip()
                handshakeProcessed = engine.decodeHandshake(buffer)
            }
            Thread.sleep(300)
        } while (!handshakeProcessed)
    }

    fun getSocketChannel(): SocketChannel? {
        return channel
    }

    private fun sendHandshake() {
        var path: String
        val part1: String? = serverUri.getRawPath()
        val part2: String? = serverUri.getRawQuery()
        path = if (part1 == null || part1.length == 0) "/" else part1
        if (part2 != null) path += "?$part2"
        val port: Int = serverUri.port
        val host: String =
            serverUri.host + if (port != WebSocketImpl.DEFAULT_PORT && port != WebSocketImpl.DEFAULT_WSS_PORT) ":$port" else ""
        val handshake = HandshakeImpl1Client()
        handshake.resourceDescriptor = path
        handshake.put("Host", host)
        handshake.put("Authorization", "Bearer $YAGNA_APPKEY")
        val handshakeRequest = draft.postProcessHandshakeRequestAsClient(handshake)
        val req = draft.createHandshake(handshakeRequest)
        engine.handshakeRequest = handshakeRequest
        engine.write(req)
    }

    fun read(): List<Framedata> {
        val binaryFrames: MutableList<Framedata> = ArrayList()
        try {
            val buffer: ByteBuffer = ByteBuffer.allocate(Short.MAX_VALUE.toInt())
            if (channel?.read(buffer)!! > 0) {
                buffer.flip()
                val decodedBinaryFrames: List<Framedata>? = engine.decode(buffer)
                if (decodedBinaryFrames != null) binaryFrames.addAll(decodedBinaryFrames)
            }
            return binaryFrames
        } catch (e: IOException) {
            RawLog.e("RawVPN", "Error: $e Message: ${e.message}")
            RawLog.e("RawVPN", "read()")
            close(CloseFrame.NORMAL, e.message ?: "", false)
        }
        return binaryFrames
        TODO("Move read logic to engine")
    }


    private fun close(code: Int, message: String, remote: Boolean) {
        try {
            engine.close(code, message, remote)
            closeConnection(CloseFrame.NORMAL, message)
        } catch (e: IOException) {
            RawLog.e("RawVPN", "Error: $e Message: ${e.message}")
            RawLog.e("RawVPN", "close()")
            closeConnection(CloseFrame.NORMAL, e.message ?: "")
        }
    }

    override fun close(code: Int, message: String?) {
        close(code, message ?: "", false)
    }

    override fun close(code: Int) {
        close(code, "", false)
    }

    override fun close() {
        close(CloseFrame.NORMAL, "", false)
    }

    override fun closeConnection(code: Int, message: String?) {
        engine.closeConnection(code, message)
    }

    override fun send(text: String) {
        engine.send(text)
    }

    override fun send(bytes: ByteBuffer) {
        engine.send(bytes)
    }

    override fun send(bytes: ByteArray?) {
        TODO("Not yet implemented")
    }

    override fun sendFrame(framedata: Framedata?) {
        TODO("Not yet implemented")
    }

    override fun sendFrame(frames: MutableCollection<Framedata>?) {
        TODO("Not yet implemented")
    }

    override fun sendPing() {
        TODO("Not yet implemented")
    }

    fun sendPing(frame: Framedata) {
        engine.sendFrame(frame)
    }

    fun sendPong() {
        engine.sendFrame(PongFrame())
    }

    override fun sendFragmentedFrame(op: Opcode?, buffer: ByteBuffer?, fin: Boolean) {
        TODO("Not yet implemented")
    }

    override fun hasBufferedData(): Boolean {
        TODO("Not yet implemented")
    }

    override fun getRemoteSocketAddress(): InetSocketAddress {
        TODO("Not yet implemented")
    }

    override fun getLocalSocketAddress(): InetSocketAddress {
        TODO("Not yet implemented")
    }

    override fun isOpen(): Boolean {
        TODO("Not yet implemented")
    }

    override fun isClosing(): Boolean {
        return engine.isClosing
    }

    override fun isFlushAndClose(): Boolean {
        TODO("Not yet implemented")
    }

    override fun isClosed(): Boolean {
        return engine.isClosed
    }

    override fun getDraft(): Draft {
        TODO("Not yet implemented")
    }

    override fun getReadyState(): ReadyState {
        TODO("Not yet implemented")
    }

    override fun getResourceDescriptor(): String {
        TODO("Not yet implemented")
    }

    override fun <T : Any?> setAttachment(attachment: T) {
        TODO("Not yet implemented")
    }

    override fun <T : Any?> getAttachment(): T {
        TODO("Not yet implemented")
    }

    override fun hasSSLSupport(): Boolean {
        TODO("Not yet implemented")
    }

    override fun getSSLSession(): SSLSession {
        TODO("Not yet implemented")
    }

    private fun getTag(): String {
        return CustomWebSocketClient::class.java.simpleName
    }
}