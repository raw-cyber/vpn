package com.raw.golem_vpn.core.vpn.ws

import android.util.Log
import com.raw.golem_vpn.utils.RawLog
import org.java_websocket.enums.*
import org.java_websocket.exceptions.*
import org.java_websocket.extensions.DefaultExtension
import org.java_websocket.extensions.IExtension
import org.java_websocket.framing.*
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.handshake.ClientHandshakeBuilder
import org.java_websocket.handshake.ServerHandshake
import org.java_websocket.util.Base64
import org.java_websocket.util.Charsetfunctions
import java.math.BigInteger
import java.net.DatagramPacket
import java.net.InetAddress
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import kotlin.experimental.inv

class CustomDraft6455(role: Role) : CustomDraft(role) {

    private lateinit var byteBufferList: List<ByteBuffer>

    private var extension: IExtension = DefaultExtension()

    /**
     * Handshake specific field for the key
     */
    private val SEC_WEB_SOCKET_KEY = "Sec-WebSocket-Key"

    /**
     * Handshake specific field for the protocol
     */
    private val SEC_WEB_SOCKET_PROTOCOL = "Sec-WebSocket-Protocol"

    /**
     * Handshake specific field for the extension
     */
    private val SEC_WEB_SOCKET_EXTENSIONS = "Sec-WebSocket-Extensions"

    /**
     * Handshake specific field for the accept
     */
    private val SEC_WEB_SOCKET_ACCEPT = "Sec-WebSocket-Accept"

    /**
     * Handshake specific field for the upgrade
     */
    private val UPGRADE = "Upgrade"

    /**
     * Handshake specific field for the connection
     */
    private val CONNECTION = "Connection"

    private val reuseableRandom = Random()
    private var incompleteframe: ByteBuffer? = null
    private val maxFrameSize = Int.Companion.MAX_VALUE

    init {
        val hasDefault = true
        byteBufferList = ArrayList()
        //We always add the DefaultExtension to implement the normal RFC 6455 specification
        if (!hasDefault) {
            knownExtensions.add(knownExtensions.size, extension)
        }
    }

    override fun acceptHandshakeAsClient(
        request: ClientHandshake, response: ServerHandshake
    ): HandshakeState {
        if (!basicAccept(response)) {
            RawLog.d(
                getTag(),
                "acceptHandshakeAsClient - Missing/wrong upgrade or connection in handshake."
            )
            return HandshakeState.NOT_MATCHED
        }
        if (!request.hasFieldValue(SEC_WEB_SOCKET_KEY) || !response.hasFieldValue(
                SEC_WEB_SOCKET_ACCEPT
            )
        ) {
            RawLog.d(
                getTag(),
                "acceptHandshakeAsClient - Missing Sec-WebSocket-Key or Sec-WebSocket-Accept"
            )
            return HandshakeState.NOT_MATCHED
        }

        val seckeyAnswer = response.getFieldValue(SEC_WEB_SOCKET_ACCEPT)
        var seckeyChallenge = request.getFieldValue(SEC_WEB_SOCKET_KEY)
        seckeyChallenge = generateFinalKey(seckeyChallenge)

        if (seckeyChallenge != seckeyAnswer) {
            RawLog.d(getTag(), "acceptHandshakeAsClient - Wrong key for Sec-WebSocket-Key.")
            return HandshakeState.NOT_MATCHED
        }
        var extensionState = HandshakeState.NOT_MATCHED
        val requestedExtension = response.getFieldValue(SEC_WEB_SOCKET_EXTENSIONS)
        for (knownExtension in knownExtensions) {
            if (knownExtension.acceptProvidedExtensionAsClient(requestedExtension)) {
                extension = knownExtension
                extensionState = HandshakeState.MATCHED
                RawLog.d(getTag(), "acceptHandshakeAsClient - Matching extension found: $extension")
                break
            }
        }
        return HandshakeState.MATCHED
        TODO("Implement this later")
//        val protocolState: HandshakeState =
//            containsRequestedProtocol(response!!.getFieldValue(SEC_WEB_SOCKET_PROTOCOL))
//        if (protocolState == HandshakeState.MATCHED && extensionState == HandshakeState.MATCHED) {
//            return HandshakeState.MATCHED
//        }
        RawLog.d(getTag(), "acceptHandshakeAsClient - No matching extension or protocol found.")
        return HandshakeState.NOT_MATCHED
    }

    override fun acceptHandshakeAsServer(handshakedata: ClientHandshake?): HandshakeState? {
        TODO("Finish this method")
        return null
    }

    override fun createBinaryFrame(framedata: Framedata): ByteBuffer {
        return createByteBufferFromFramedataOptimazed(framedata)
    }

    private fun createByteBufferFromFramedataOptimazed(framedata: Framedata): ByteBuffer {
        val mes = framedata.payloadData
        val mask = role == Role.CLIENT
        val sizebytes: Int = getSizeBytes(mes)
        val buf =
            ByteBuffer.allocate(1 + (if (sizebytes > 1) sizebytes + 1 else sizebytes) + (if (mask) 4 else 0) + mes.remaining())

        var one = (if (framedata.isFin) -128 else 0).toByte()
        one = one.toInt().or(fromOpcode(framedata.opcode).toInt()).toByte()
        one = one.toInt().or(if (framedata.isRSV1) getRSVByte(1).toInt() else 0)
            .or(if (framedata.isRSV2) getRSVByte(2).toInt() else 0)
            .or(if (framedata.isRSV3) getRSVByte(3).toInt() else 0)
            .toByte()
        buf.put(one)

        val payloadlengthbytes: ByteArray = toByteArray(mes.remaining().toLong(), sizebytes)
        assert(payloadlengthbytes.size == sizebytes)

        when (sizebytes) {
            1 -> buf.put((payloadlengthbytes[0].toInt() or getMaskByte(mask).toInt()).toByte())
            2 -> {
                buf.put((126.toByte().toInt() or getMaskByte(mask).toInt()).toByte())
                buf.put(payloadlengthbytes)
            }
            8 -> {
                buf.put((127.toByte().toInt() or getMaskByte(mask).toInt()).toByte())
                buf.put(payloadlengthbytes)
            }
            else -> throw IllegalStateException("Size representation not supported/specified")
        }

        if (mask) {
            val maskkey = ByteBuffer.allocate(4).apply { putInt(reuseableRandom.nextInt()) }.array()
            buf.put(maskkey)
            for (i in 0 until mes.remaining()) {
                buf.put((mes.get().toInt() xor maskkey[i % 4].toInt()).toByte())
            }
        } else {
            buf.put(mes)
            mes.flip()
        }

        assert(buf.remaining() == 0) { buf.remaining() }
        buf.flip()
        return buf
    }


    private fun createByteBufferFromFramedata(framedata: Framedata): ByteBuffer {
        val mes = framedata.payloadData
        val mask = role == Role.CLIENT
        val sizebytes: Int = getSizeBytes(mes)
        val buf =
            ByteBuffer.allocate(1 + (if (sizebytes > 1) sizebytes + 1 else sizebytes) + (if (mask) 4 else 0) + mes.remaining())
        val optcode: Byte = fromOpcode(framedata.opcode)
        var one = (if (framedata.isFin) -128 else 0).toByte()
        one = (one.toInt() or optcode.toInt()).toByte()
        if (framedata.isRSV1) one = (one.toInt() or getRSVByte(1).toInt()).toByte()
        if (framedata.isRSV2) one = (one.toInt() or getRSVByte(2).toInt()).toByte()
        if (framedata.isRSV3) one = (one.toInt() or getRSVByte(3).toInt()).toByte()
        buf.put(one)
        val payloadlengthbytes: ByteArray = toByteArray(mes.remaining().toLong(), sizebytes)
        assert(payloadlengthbytes.size == sizebytes)
        if (sizebytes == 1) {
            buf.put((payloadlengthbytes[0].toInt() or getMaskByte(mask).toInt()).toByte())
        } else if (sizebytes == 2) {
            buf.put((126.toByte().toInt() or getMaskByte(mask).toInt()).toByte())
            buf.put(payloadlengthbytes)
        } else if (sizebytes == 8) {
            buf.put((127.toByte().toInt() or getMaskByte(mask).toInt()).toByte())
            buf.put(payloadlengthbytes)
        } else {
            throw java.lang.IllegalStateException("Size representation not supported/specified")
        }
        if (mask) {
            val maskkey = ByteBuffer.allocate(4)
            maskkey.putInt(reuseableRandom.nextInt())
            buf.put(maskkey.array())
            var i = 0
            while (mes.hasRemaining()) {
                buf.put((mes.get().toInt() xor maskkey[i % 4].toInt()).toByte())
                i++
            }
        } else {
            buf.put(mes)
            //Reset the position of the bytebuffer e.g. for additional use
            mes.flip()
        }
        assert(buf.remaining() == 0) { buf.remaining() }
        buf.flip()
        return buf
    }

    private fun getSizeBytes(mes: ByteBuffer): Int {
        if (mes.remaining() <= 125) {
            return 1
        } else if (mes.remaining() <= 65535) {
            return 2
        }
        return 8
    }

    private fun fromOpcode(opcode: Opcode): Byte {
        return when (opcode) {
            Opcode.CONTINUOUS -> 0
            Opcode.TEXT -> 1
            Opcode.BINARY -> 2
            Opcode.CLOSING -> 8
            Opcode.PING -> 9
            Opcode.PONG -> 10
            else -> throw IllegalArgumentException("Don't know how to handle $opcode")
        }
    }

    @Throws(InvalidFrameException::class)
    private fun toOpcode(opcode: Byte): Opcode {
        return when (opcode) {
            0.toByte() -> Opcode.CONTINUOUS
            1.toByte() -> Opcode.TEXT
            2.toByte() -> Opcode.BINARY
            8.toByte() -> Opcode.CLOSING
            9.toByte() -> Opcode.PING
            10.toByte() -> Opcode.PONG
            else -> throw InvalidFrameException("Unknown opcode " + opcode.toShort())
        }
    }

    private fun getRSVByte(rsv: Int): Byte {
        if (rsv == 1) // 0100 0000
            return 0x40
        if (rsv == 2) // 0010 0000
            return 0x20
        return if (rsv == 3) 0x10 else 0
    }

    private fun getMaskByte(mask: Boolean): Byte {
        return if (mask) (-128.toByte()).toByte() else 0
    }

    private fun toByteArray(`val`: Long, bytecount: Int): ByteArray {
        val buffer = ByteArray(bytecount)
        val highest = 8 * bytecount - 8
        for (i in 0 until bytecount) {
            buffer[i] = (`val` ushr highest - 8 * i).toByte()
        }
        return buffer
    }

    private fun packetIpWrapToEther(
        frame: ByteArray,
        srcMac: ByteArray? = null,
        dstMac: ByteArray? = null
    ): ByteArray {
        if (frame.isEmpty()) {
            throw IllegalArgumentException("Error when wrapping IP packet: Empty packet")
        }
        val ethPacket = ByteArray(frame.size + 14)
        val buf = ByteBuffer.wrap(ethPacket)
        dstMac?.let { buf.put(it) } ?: buf.put(byteArrayOf(0x02, 0x02, 0x02, 0x02, 0x02, 0x02))
        srcMac?.let { buf.put(it) } ?: buf.put(byteArrayOf(0x01, 0x01, 0x01, 0x01, 0x01, 0x01))
        // Check the IP version of the packet
        val ipVersion = (frame[0].toInt() ushr 4) and 0x0F
        if (ipVersion != 4 && ipVersion != 6) {
            throw IllegalArgumentException("Error when wrapping IP packet: Packet version is not IPv4 or IPv6")
        }
        if (ipVersion == 4) {
            buf.putShort(12, 0x0800.toShort())
        } else {
            buf.putShort(12, 0x86dd.toShort())
        }
        buf.position(14)
        buf.put(frame)

        return ethPacket
    }

    override fun createFrames(binary: ByteBuffer, mask: Boolean): List<Framedata> {
        val curframe = BinaryFrame()
        val ethernetPacket = packetIpWrapToEther(binary.array())
        curframe.setPayload(ByteBuffer.wrap(ethernetPacket))
        curframe.setTransferemasked(mask)
        try {
            curframe.isValid()
        } catch (e: InvalidDataException) {
            throw NotSendableException(e)
        }
        return listOf(curframe as Framedata)
    }

    override fun createFrames(text: String, mask: Boolean): List<Framedata> {
        val curframe = TextFrame()
        curframe.setPayload(ByteBuffer.wrap(Charsetfunctions.utf8Bytes(text)))
        curframe.setTransferemasked(mask)
        try {
            curframe.isValid()
        } catch (e: InvalidDataException) {
            throw NotSendableException(e)
        }
        return listOf(curframe as Framedata)
    }

    override fun processFrame(webSocketImpl: CustomWebSocketImpl, frame: Framedata) {
        val curop = frame.opcode
        if (curop == Opcode.CLOSING) {
            processFrameClosing(webSocketImpl, frame)
        } else if (curop == Opcode.PING) {
            val pongFrame = PongFrame()
//            webSocketImpl.getWebSocketListener().onWebsocketPing(webSocketImpl, frame)
        } else if (curop == Opcode.PONG) {
//            webSocketImpl.updateLastPong()
//            webSocketImpl.getWebSocketListener().onWebsocketPong(webSocketImpl, frame)
        } else if (!frame.isFin || curop == Opcode.CONTINUOUS) {
            println("continuous")
//            processFrameContinuousAndNonFin(webSocketImpl, frame, curop)
//        } else if (currentContinuousFrame != null) {
//            log.error("Protocol error: Continuous frame sequence not completed.")
//            throw InvalidDataException(
//                CloseFrame.PROTOCOL_ERROR,
//                "Continuous frame sequence not completed."
//            )
        } else if (curop == Opcode.TEXT) {
            processFrameText(webSocketImpl, frame)
        } else if (curop == Opcode.BINARY) {
            processFrameBinary(webSocketImpl, frame)
        } else {
            RawLog.d(getTag(), "non control or continious frame expected")
            throw InvalidDataException(
                CloseFrame.PROTOCOL_ERROR, "non control or continious frame expected"
            )
        }
    }

    private fun processFrameBinary(
        webSocketImpl: CustomWebSocketImpl, frame: Framedata
    ): Framedata {
        // It is there only for debugging purposes
//        try {
//            val buffer = frame.payloadData.array()
//            val packet = DatagramPacket(buffer, buffer.size)
//
//            // Get the source IP address and port
//            val srcAddress = packet.address
//            val srcPort = packet.port
//
//            // Extract the IP packet from the binary frame
//            val ipPacket = DatagramPacket(buffer, 0, 24)
//            ipPacket.setData(buffer, 0, packet.length)
//
//            // Get the payload as a UDP packet
//            val udpPacket = DatagramPacket(buffer, 0, packet.length - 20, srcAddress, srcPort)
//            udpPacket.setData(buffer, 20, packet.length - 20)
//            RawLog.d(getTag(), "UDP packet received : ${udpPacket.data}")
//        } catch (e: Exception) {
//            RawLog.e(getTag(), "Error while processing binary frame : ${e.message}")
//        }
        return frame
    }

    //    TODO - Only for debugging
    private fun decodePacket(bytes: ByteArray) {
        try {
            val TAG = "PacketDecoderExample"

            // Extract the IP header fields
            val version = (bytes[0].toInt() and 0xF0) shr 4
            val ihl = bytes[0].toInt() and 0x0F
            val totalLength = ByteBuffer.wrap(bytes, 2, 2).order(ByteOrder.BIG_ENDIAN).short.toInt()
            val protocol = bytes[9].toInt()
            val srcAddress = InetAddress.getByAddress(bytes.slice(12..15).toByteArray())
            val dstAddress = InetAddress.getByAddress(bytes.slice(16..19).toByteArray())

            // Extract the UDP header fields
            val srcPort =
                ByteBuffer.wrap(bytes, ihl * 4, 2).order(ByteOrder.BIG_ENDIAN).short.toInt()
            val dstPort =
                ByteBuffer.wrap(bytes, ihl * 4 + 2, 2).order(ByteOrder.BIG_ENDIAN).short.toInt()
            val payloadLength = totalLength - ihl * 4 - 8

            // Extract the payload as a string
            val payload = String(
                bytes.slice(ihl * 4 + 8 until totalLength).toByteArray(),
                StandardCharsets.UTF_8
            )

            // Create a new IP packet and UDP packet
            val ipPacket = DatagramPacket(bytes, bytes.size, dstAddress, srcPort)
            val udpPacket = DatagramPacket(bytes, ihl * 4 + 8, payloadLength, dstAddress, dstPort)

            // Print the extracted fields and payload
            Log.d(TAG, "Version: $version")
            Log.d(TAG, "IHL: $ihl")
            Log.d(TAG, "Total Length: $totalLength")
            Log.d(TAG, "Protocol: $protocol")
            Log.d(TAG, "Source Address: $srcAddress")
            Log.d(TAG, "Destination Address: $dstAddress")
            Log.d(TAG, "Source Port: $srcPort")
            Log.d(TAG, "Destination Port: $dstPort")
            Log.d(TAG, "Payload: $payload")
        } catch (e: Exception) {
            Log.e("PacketDecoderExample", "Error while decoding packet : ${e.message}")
        }
    }

    private fun processFrameText(
        webSocketImpl: CustomWebSocketImpl, frame: Framedata
    ): Framedata {
        RawLog.d(getTag(), "Text frame received : ${frame.payloadData}")
        return frame
    }

    override fun processFrameClosing(webSocketImpl: CustomWebSocketImpl, frame: Framedata) {
        var code = CloseFrame.NOCODE
        var reason = ""
        if (frame is CloseFrame) {
            val cf = frame
            code = cf.closeCode
            reason = cf.message
        }
        if (webSocketImpl.readyState == ReadyState.CLOSING) {
//            // complete the close handshake by disconnecting
            webSocketImpl.closeConnection(code, reason, true)
//        } else {
//            // echo close handshake
//            if (getCloseHandshakeType() == CloseHandshakeType.TWOWAY) webSocketImpl.close(
//                code,
//                reason,
//                true
//            ) else webSocketImpl.flushAndClose(code, reason, false)
        }
//        TODO("Unfinished close frame handling")
    }

    override fun translateFrame(buffer: ByteBuffer): List<Framedata> {
        while (true) {
            val frames: MutableList<Framedata> = LinkedList()
            var cur: Framedata
            if (incompleteframe != null) {
                // complete an incomplete frame
                try {
                    buffer.mark()
                    val availableNextByteCount = buffer.remaining() // The number of bytes received
                    val expectedNextByteCount: Int =
                        incompleteframe!!.remaining() // The number of bytes to complete the incomplete frame
                    if (expectedNextByteCount > availableNextByteCount) {
                        // did not receive enough bytes to complete the frame
                        incompleteframe!!.put(
                            buffer.array(), buffer.position(), availableNextByteCount
                        )
                        buffer.position(buffer.position() + availableNextByteCount)
                        return emptyList()
                    }
                    incompleteframe!!.put(buffer.array(), buffer.position(), expectedNextByteCount)
                    buffer.position(buffer.position() + expectedNextByteCount)
                    cur = translateSingleFrame(
                        incompleteframe!!.duplicate().position(0) as ByteBuffer
                    )
                    frames.add(cur)
                    incompleteframe = null
                } catch (e: IncompleteException) {
                    // extending as much as suggested
                    val extendedframe = ByteBuffer.allocate(checkAlloc(e.preferredSize))
                    assert(extendedframe.limit() > incompleteframe!!.limit())
                    incompleteframe!!.rewind()
                    extendedframe.put(incompleteframe)
                    incompleteframe = extendedframe
                    continue
                }
            }
            while (buffer.hasRemaining()) { // Read as much as possible full frames
                buffer.mark()
                try {
                    cur = translateSingleFrame(buffer)
                    frames.add(cur)
                } catch (e: IncompleteException) {
                    // remember the incomplete data
                    buffer.reset()
                    val pref = e.preferredSize
                    incompleteframe = ByteBuffer.allocate(checkAlloc(pref))
                    incompleteframe!!.put(buffer)
                    break
                }
            }
            return frames
        }
    }

    private fun translateSingleFrame(buffer: ByteBuffer): Framedata {
        val maxPacketSize = buffer.remaining()
        var realPacketSize = 2
        if (maxPacketSize < 2) {
            throw Exception("Invalid frame")
        }
        val b1 = buffer.get()
        val fin = b1.toInt() shr 8 != 0
        val rsv1 = b1.toInt() and 0x40 != 0
        val rsv2 = b1.toInt() and 0x20 != 0
        val rsv3 = b1.toInt() and 0x10 != 0
        val b2 = buffer.get()
        val mask = b2.toInt() and -128 != 0
        var payloadlength = (b2.toInt() and 128.toByte().inv().toInt()).toByte().toInt()
        val optcode = toOpcode((b1.toInt() and 15).toByte())
        if (!(payloadlength >= 0 && payloadlength <= 125)) {
            val payloadData: TranslatedPayloadMetaData = translateSingleFramePayloadLength(
                buffer, optcode, payloadlength, maxPacketSize, realPacketSize
            )
            payloadlength = payloadData.payloadLength
            realPacketSize = payloadData.realPackageSize
        }
        translateSingleFrameCheckLengthLimit(payloadlength.toLong())
        realPacketSize += if (mask) 4 else 0
        realPacketSize += payloadlength
        translateSingleFrameCheckPacketSize(maxPacketSize, realPacketSize)

        if (payloadlength < 0) {
            throw InvalidDataException(CloseFrame.PROTOCOL_ERROR, "Negative count")
        }
        val payload = ByteBuffer.allocate(payloadlength)
        if (mask) {
            val maskskey = ByteArray(4)
            buffer[maskskey]
            for (i in 0 until payloadlength) {
                payload.put((buffer.get().toInt() xor maskskey[i % 4].toInt()).toByte())
            }
        } else {
            payload.put(buffer.array(), buffer.position(), payload.limit())
            buffer.position(buffer.position() + payload.limit())
        }

        val frame = FramedataImpl1.get(optcode)
        frame.isFin = fin
        frame.isRSV1 = rsv1
        frame.isRSV2 = rsv2
        frame.isRSV3 = rsv3
        payload.flip()
        frame.setPayload(payload)
        frame.isValid()
        return frame
    }

    private fun translateSingleFramePayloadLength(
        buffer: ByteBuffer,
        optcode: Opcode,
        oldPayloadlength: Int,
        maxpacketsize: Int,
        oldRealpacketsize: Int
    ): TranslatedPayloadMetaData {
        var payloadlength = oldPayloadlength
        var realpacketsize = oldRealpacketsize
        if (optcode == Opcode.PING || optcode == Opcode.PONG || optcode == Opcode.CLOSING) {
            Log.d(getTag(), "Invalid frame: control frame using reserved opcode $optcode")
            throw InvalidFrameException("more than 125 octets")
        }
        if (payloadlength == 126) {
            realpacketsize += 2 // additional length bytes
            translateSingleFrameCheckPacketSize(maxpacketsize, realpacketsize)
            val sizebytes = ByteArray(3)
            sizebytes[1] = buffer.get()
            sizebytes[2] = buffer.get()
            payloadlength = BigInteger(sizebytes).toInt()
        } else {
            realpacketsize += 8 // additional length bytes
            translateSingleFrameCheckPacketSize(maxpacketsize, realpacketsize)
            val bytes = ByteArray(8)
            for (i in 0..7) {
                bytes[i] = buffer.get()
            }
            val length = BigInteger(bytes).toLong()
            translateSingleFrameCheckLengthLimit(length)
            payloadlength = length.toInt()
        }
        return TranslatedPayloadMetaData(payloadlength, realpacketsize)
    }

    private fun translateSingleFrameCheckLengthLimit(length: Long) {
        if (length > Int.MAX_VALUE) {
            Log.d(getTag(), "Limit exedeed: Payloadsize is to big...")
            throw LimitExceededException("Payloadsize is to big...")
        }
        if (length > maxFrameSize) {
            Log.d(
                getTag(), "Payload limit reached. Allowed: $maxFrameSize Current: $length"
            )
            throw LimitExceededException("Payload limit reached.", maxFrameSize)
        }
        if (length < 0) {
            Log.d(getTag(), "Limit underflow: Payloadsize is to little...")
            throw LimitExceededException("Payloadsize is to little...")
        }
    }

    private fun translateSingleFrameCheckPacketSize(maxpacketsize: Int, realpacketsize: Int) {
        if (maxpacketsize < realpacketsize) {
            Log.d(
                getTag(),
                "Incomplete frame: maxpacketsize ($maxpacketsize) < realpacketsize ($realpacketsize))"
            )
            throw IncompleteException(realpacketsize)
        }
    }

    override fun getCloseHandshakeType(): CloseHandshakeType {
        return CloseHandshakeType.TWOWAY
    }

    override fun postProcessHandshakeRequestAsClient(request: ClientHandshakeBuilder): ClientHandshakeBuilder {
        request.put(UPGRADE, "websocket")
        request.put(
            CONNECTION, UPGRADE
        ) // to respond to a Connection keep alives

        val random = ByteArray(16)
        reuseableRandom.nextBytes(random)
        request.put(SEC_WEB_SOCKET_KEY, Base64.encodeBytes(random))
        request.put("Sec-WebSocket-Version", "13") // overwriting the previous

        val requestedExtensions = StringBuilder()
        for (knownExtension in knownExtensions) {
            if (knownExtension.providedExtensionAsClient != null && knownExtension.providedExtensionAsClient.isNotEmpty()) {
                if (requestedExtensions.isNotEmpty()) {
                    requestedExtensions.append(", ")
                }
                requestedExtensions.append(knownExtension.providedExtensionAsClient)
            }
        }
        if (requestedExtensions.isNotEmpty()) {
            request.put(SEC_WEB_SOCKET_EXTENSIONS, requestedExtensions.toString())
        }
        val requestedProtocols = StringBuilder()
        for (knownProtocol in knownProtocols) {
            if (knownProtocol.providedProtocol.isNotEmpty()) {
                if (requestedProtocols.isNotEmpty()) {
                    requestedProtocols.append(", ")
                }
                requestedProtocols.append(knownProtocol.providedProtocol)
            }
        }
        if (requestedProtocols.isNotEmpty()) {
            request.put(SEC_WEB_SOCKET_PROTOCOL, requestedProtocols.toString())
        }
        return request
    }

    private fun generateFinalKey(`in`: String): String {
        val seckey = `in`.trim { it <= ' ' }
        val acc = seckey + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
        val sh1: MessageDigest = try {
            MessageDigest.getInstance("SHA1")
        } catch (e: NoSuchAlgorithmException) {
            throw IllegalStateException(e)
        }
        return Base64.encodeBytes(sh1.digest(acc.toByteArray()))
    }

    private fun getTag(): String {
        return CustomDraft6455::class.simpleName.toString()
    }

    private class TranslatedPayloadMetaData constructor(
        val payloadLength: Int, val realPackageSize: Int
    )

}