package com.raw.golem_vpn.core.vpn

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.VpnService
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.ParcelFileDescriptor
import android.util.Log
import android.util.Pair
import com.raw.golem_vpn.R
import com.raw.golem_vpn.core.notifications.DefaultNotificationHelper
import com.raw.golem_vpn.data.source.AccountDataSource
import com.raw.golem_vpn.data.source.AgreementDataSource
import com.raw.golem_vpn.data.source.AgreementHandlerRepository
import com.raw.golem_vpn.data.source.MSG_ACCEPT_PAYMENTS
import com.raw.golem_vpn.data.source.MSG_RUN_VPN
import com.raw.golem_vpn.data.source.MSG_STOP_VPN
import com.raw.golem_vpn.data.source.NetworkTrafficCounterDataSource
import com.raw.golem_vpn.data.source.VpnPreferenceKeys
import com.raw.golem_vpn.data.source.VpnServiceManager
import com.raw.golem_vpn.data.source.VpnStatusManager
import com.raw.golem_vpn.data.source.YagnaPreferenceKeys
import com.raw.golem_vpn.utils.RawLog
import com.raw.golem_vpn.utils.VpnStatus
import dagger.hilt.android.AndroidEntryPoint
import io.ktor.client.HttpClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference
import javax.inject.Inject

@AndroidEntryPoint
class RawVpnService : VpnService() {
    private val tag: String = this::class.java.simpleName

    private val mConnectingThread = AtomicReference<Thread?>()
    private val mConnection = AtomicReference<Connection?>()
    private val mYagnaServerThread = AtomicReference<Thread?>()
    private val mNextConnectionId = AtomicInteger(1)
    private var mConfigureIntent: PendingIntent? = null
    private var paymentsJob: Job? = null

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    private lateinit var mMessenger: Messenger

    private val serviceScope = CoroutineScope(SupervisorJob())

    lateinit var yagnaDaemon: RawYagnaDaemon

    @Inject
    lateinit var notificationHelper: DefaultNotificationHelper

    @Inject
    lateinit var vpnManager: VpnServiceManager

    @Inject
    lateinit var accountDataSource: AccountDataSource

    @Inject
    lateinit var agreementDataSource: AgreementDataSource

    @Inject
    lateinit var agreementHandlerRepository: AgreementHandlerRepository

    @Inject
    lateinit var networkTrafficCounterDataSource: NetworkTrafficCounterDataSource

    @Inject
    lateinit var vpnStatusManager: VpnStatusManager

    @Inject
    lateinit var client: HttpClient

    companion object {
        const val ACTION_CONNECT = "com.raw.golem_vpn.START"
        const val ACTION_DISCONNECT = "com.raw.golem_vpn.STOP"
        private val TAG = RawVpnService::class.java.simpleName
        fun prepare(context: Context): Intent? {
            return VpnService.prepare(context)
        }
    }

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    override fun onBind(intent: Intent): IBinder? {
//        Toast.makeText(applicationContext, "binding", Toast.LENGTH_SHORT).show()
        mMessenger = Messenger(IncomingHandler(this))
        return mMessenger.binder
    }

    /**
     * Handler of incoming messages from clients.
     */
    internal class IncomingHandler(
        context: Context,
        private val serviceRef: WeakReference<RawVpnService> = WeakReference(context as RawVpnService),
    ) : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                MSG_RUN_VPN -> {
                    serviceRef.get()?.startVpn(msg.data.getString("address")!!)
                }

                MSG_STOP_VPN -> {
                    serviceRef.get()?.stopVpn()
                }

                MSG_ACCEPT_PAYMENTS -> {
                    serviceRef.get()?.acceptPayments()
                }

                else -> super.handleMessage(msg)
            }
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return if (ACTION_DISCONNECT == intent.action) {
            stopYagna()
            START_NOT_STICKY
        } else {
            runYagna()
            START_STICKY
        }
    }

    override fun onDestroy() {
        stopVpn()
    }

    override fun onLowMemory() {
        super.onLowMemory()
    }

    private fun runYagna() {
        updateForegroundNotification(R.string.idle)
        yagnaDaemon = RawYagnaDaemon(this)
        startYagnaServer(yagnaDaemon)
    }

    private fun startYagnaServer(yagnaServer: RawYagnaDaemon) {
        val yagnaServerThread = Thread(yagnaServer, "Yagna Server Thread")
        setYagnaThread(yagnaServerThread)
        yagnaServer.setOnEstablishListener(object : RawYagnaDaemon.OnEstablishListener {
            override fun onYagnaStarted(mService: RawVpnService) {
                serviceScope.launch {
                    accountDataSource.initPayment()
                    accountDataSource.refreshAccountInfo()
                    accountDataSource.refreshBalance()
                }
                updateYagnaStatus(true)
                vpnManager.checkYagnaStatus()
            }

            override fun onYagnaStopped(mService: RawVpnService) {
                updateYagnaStatus(false)
            }
        })
        yagnaServerThread.start()
    }

    private fun startVpn(server: String) {
        RawLog.d(TAG, "Starting VPN with server: $server")
        startVpnConnection(
            RawVpnConnection(
                this, mNextConnectionId.getAndIncrement(), server
            )
        )
    }

    private fun acceptPayments() {
        paymentsJob = serviceScope.launch {
            try {
                agreementHandlerRepository.acceptPayments()
            } catch (e: Exception) {
                RawLog.e(tag, "Error accepting payments: ${e.message}")
            } finally {
                withContext(NonCancellable) {
                    agreementHandlerRepository.terminateAgreement()
                }
            }
        }
    }

    private fun startVpnConnection(connection: RawVpnConnection) {
        val connectionVpnThread = Thread(connection, "RawVPNOverWS Thread")
        setConnectingThread(connectionVpnThread)
        mConfigureIntent = notificationHelper.mConfigureIntent
        connection.setConfigureIntent(mConfigureIntent)
        connection.setOnEstablishListener(object : RawVpnConnection.OnEstablishListener {
            override fun onEstablish(tunInterface: ParcelFileDescriptor?) {
                onConnectionEstablished(connectionVpnThread, tunInterface)
            }

            override fun onOutgoingPacketsSent(packetsSize: Long) {
                serviceScope.launch {
                    networkTrafficCounterDataSource.updateOutgoingNetworkDataPreference(
                        packetsSize
                    )
                }
            }

            override fun onIncomingPacketsReceived(packetsSize: Long) {
                serviceScope.launch {
                    networkTrafficCounterDataSource.updateIncomingNetworkDataPreference(
                        packetsSize
                    )
                }
            }
        })
        updateVpnStatus(true)
        connectionVpnThread.start()
    }

    private fun onConnectionEstablished(thread: Thread, tunInterface: ParcelFileDescriptor?) {
        mConnectingThread.compareAndSet(null, thread)
        setConnection(Connection(thread, tunInterface))
        vpnStatusManager.updateVpnStatus(VpnStatus.CONNECTED, null)
        updateForegroundNotification(R.string.connected)
    }

    private fun setConnectingThread(thread: Thread?) {
        val oldThread = mConnectingThread.getAndSet(thread)
        oldThread?.interrupt()
    }

    private fun setYagnaThread(thread: Thread?) {
        val oldThread = mYagnaServerThread.getAndSet(thread)
        oldThread?.interrupt()
    }

    private fun setConnection(connection: Connection?) {
        val oldConnection = mConnection.getAndSet(connection)
        if (oldConnection != null) {
            try {
                oldConnection.first!!.interrupt()
                oldConnection.second!!.close()
            } catch (e: IOException) {
                Log.d(TAG, "Closing VPN interface", e)
            }
        }
    }

    override fun onRevoke() {
        super.onRevoke()
    }

    private fun stopVpn() {
        vpnStatusManager.updateVpnStatus(VpnStatus.DISCONNECTING, null)
        RawLog.i(TAG, "Disconnecting from VPN")
        serviceScope.launch {
            paymentsJob?.cancelAndJoin()
            setConnectingThread(null)
            setConnection(null)
            updateVpnStatus(false)
            vpnStatusManager.updateVpnStatus(VpnStatus.DISCONNECTED, null)
        }
    }

    private fun stopYagna() {
        setYagnaThread(null)
        stopForeground(STOP_FOREGROUND_REMOVE)
        updateYagnaStatus(false)
    }

    private fun updateYagnaStatus(status: Boolean) {
        RawLog.d(TAG, "Yagna status: $status")
        serviceScope.launch {
            vpnManager.updatePreference(YagnaPreferenceKeys.STATUS, status)
        }
    }

    private fun updateVpnStatus(status: Boolean) {
        serviceScope.launch {
            vpnManager.updatePreference(
                VpnPreferenceKeys.CONNECTED,
                status
            )
        }
    }

    private fun updateForegroundNotification(message: Int) {
        val notification =
            notificationHelper.updateVpnStatus("Golem VPN", applicationContext.getString(message))
        startForeground(
            1, notification
        )
    }

    private class Connection(thread: Thread?, pfd: ParcelFileDescriptor?) :
        Pair<Thread?, ParcelFileDescriptor?>(thread, pfd)
}