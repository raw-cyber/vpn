package com.raw.golem_vpn.core.vpn.ws

import com.raw.golem_vpn.utils.RawLog
import java.net.InetSocketAddress
import java.net.Socket
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.SocketChannel

class WsSocketChannel {
    private var isOpen: Boolean = false
    private lateinit var channel: SocketChannel
    private lateinit var selector: Selector
    private lateinit var readSelector: Selector
    private lateinit var writeSelector: Selector
    private lateinit var socket: Socket

    fun createSocketChannel(host: String, port: Int): SocketChannel {
        channel = SocketChannel.open()
        channel.configureBlocking(false)
        selector = Selector.open()
        readSelector = Selector.open()
        writeSelector = Selector.open()

        channel.register(selector, SelectionKey.OP_CONNECT)
        channel.connect(InetSocketAddress(host, port))

        selector.select()
        while (!channel.finishConnect()) {
            RawLog.d("RawVPN Socket", "Connection is being established...");
        }
        return channel
    }

    fun close() {
        isOpen = false
        channel.close()
        selector.close()
        readSelector.close()
        writeSelector.close()
        socket.close()
    }
}