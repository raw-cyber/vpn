package com.raw.golem_vpn.core.vpn

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import com.raw.golem_vpn.core.vpn.RawVpnService
import com.raw.golem_vpn.utils.RawLog

class VpnActivityResultContracts : ActivityResultContract<String, Boolean>() {
    override fun createIntent(context: Context, input: String): Intent {
        val intent = RawVpnService.prepare(context)
        return intent!!
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Boolean {
        RawLog.d("VpnActivityResultContracts", "parseResult: $resultCode")
        return resultCode == RESULT_OK
    }
}