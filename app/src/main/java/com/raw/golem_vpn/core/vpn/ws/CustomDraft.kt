package com.raw.golem_vpn.core.vpn.ws

import org.java_websocket.drafts.Draft
import org.java_websocket.enums.CloseHandshakeType
import org.java_websocket.enums.HandshakeState
import org.java_websocket.enums.Opcode
import org.java_websocket.enums.Role
import org.java_websocket.exceptions.IncompleteHandshakeException
import org.java_websocket.exceptions.InvalidDataException
import org.java_websocket.exceptions.InvalidHandshakeException
import org.java_websocket.extensions.IExtension
import org.java_websocket.framing.*
import org.java_websocket.handshake.*
import org.java_websocket.protocols.IProtocol
import org.java_websocket.util.Charsetfunctions
import java.nio.ByteBuffer

abstract class CustomDraft(
    val role: Role,
    val knownExtensions: MutableList<IExtension> = ArrayList(),
    val knownProtocols: MutableList<IProtocol> = ArrayList()
) {
    private var continuousFrameType: Opcode? = null


    fun readLine(buf: ByteBuffer): ByteBuffer? {
        val sbuf = ByteBuffer.allocate(buf.remaining())
        var prev: Byte
        var cur = '0'.code.toByte()
        while (buf.hasRemaining()) {
            prev = cur
            cur = buf.get()
            sbuf.put(cur)
            if (prev == '\r'.code.toByte() && cur == '\n'.code.toByte()) {
                sbuf.limit(sbuf.position() - 2)
                sbuf.position(0)
                return sbuf
            }
        }
        // ensure that there wont be any bytes skipped
        buf.position(buf.position() - sbuf.position())
        return null
    }

    fun readStringLine(buf: ByteBuffer?): String? {
        val b = Draft.readLine(buf)
        return if (b == null) null else Charsetfunctions.stringAscii(b.array(), 0, b.limit())
    }

    @Throws(InvalidHandshakeException::class)
    fun translateHandshakeHttp(buf: ByteBuffer): HandshakeBuilder {
        val handshake: HandshakeBuilder
        var line: String = Draft.readStringLine(buf)
            ?: throw IncompleteHandshakeException(buf.capacity() + 128)
        val firstLineTokens = line.split(" ".toRegex(), limit = 3)
            .toTypedArray() // eg. HTTP/1.1 101 Switching the Protocols
        if (firstLineTokens.size != 3) {
            throw InvalidHandshakeException()
        }
        handshake = translateHandshakeHttpClient(firstLineTokens, line)
        line = Draft.readStringLine(buf)
        while (line != null && line.length > 0) {
            val pair = line.split(":".toRegex(), limit = 2).toTypedArray()
            if (pair.size != 2) throw InvalidHandshakeException("not an http header")
            // If the handshake contains already a specific key, append the new value
            if (handshake.hasFieldValue(pair[0])) {
                handshake.put(
                    pair[0],
                    handshake.getFieldValue(pair[0]) + "; " + pair[1].replaceFirst(
                        "^ +".toRegex(),
                        ""
                    )
                )
            } else {
                handshake.put(pair[0], pair[1].replaceFirst("^ +".toRegex(), ""))
            }
            line = Draft.readStringLine(buf)
        }
        if (line == null) throw IncompleteHandshakeException()
        return handshake
    }

    @Throws(InvalidHandshakeException::class)
    private fun translateHandshakeHttpClient(
        firstLineTokens: Array<String>,
        line: String
    ): HandshakeBuilder {
        // translating/parsing the response from the SERVER
        if ("101" != firstLineTokens[1]) {
            throw InvalidHandshakeException(
                String.format(
                    "Invalid status code received: %s Status line: %s",
                    firstLineTokens[1], line
                )
            )
        }
        if (!"HTTP/1.1".equals(firstLineTokens[0], ignoreCase = true)) {
            throw InvalidHandshakeException(
                String.format(
                    "Invalid status line received: %s Status line: %s",
                    firstLineTokens[0], line
                )
            )
        }
        val handshake: HandshakeBuilder = HandshakeImpl1Server()
        val serverhandshake = handshake as ServerHandshakeBuilder
        serverhandshake.httpStatus = firstLineTokens[1].toShort()
        serverhandshake.httpStatusMessage = firstLineTokens[2]
        return handshake
    }

    @Throws(InvalidHandshakeException::class)
    abstract fun acceptHandshakeAsClient(
        request: ClientHandshake,
        response: ServerHandshake
    ): HandshakeState

    @Throws(InvalidHandshakeException::class)
    abstract fun acceptHandshakeAsServer(handshakedata: ClientHandshake?): HandshakeState?

    protected fun basicAccept(handshakedata: Handshakedata): Boolean {
        return handshakedata.getFieldValue("Upgrade")
            .equals("websocket", ignoreCase = true) && handshakedata.getFieldValue("Connection")
            .lowercase().contains("upgrade")
    }

    abstract fun createBinaryFrame(framedata: Framedata): ByteBuffer

    abstract fun createFrames(binary: ByteBuffer, mask: Boolean): List<Framedata>

    abstract fun createFrames(text: String, mask: Boolean): List<Framedata>


    /**
     * Handle the frame specific to the draft
     * @param webSocketImpl the websocketimpl used for this draft
     * @param frame the frame which is supposed to be handled
     * @throws InvalidDataException will be thrown on invalid data
     */
    @Throws(InvalidDataException::class)
    abstract fun processFrame(webSocketImpl: CustomWebSocketImpl, frame: Framedata)

    @Throws(InvalidDataException::class)
    abstract fun processFrameClosing(webSocketImpl: CustomWebSocketImpl, frame: Framedata)

    @Throws(InvalidDataException::class)
    abstract fun translateFrame(buffer: ByteBuffer): List<Framedata>

    abstract fun getCloseHandshakeType(): CloseHandshakeType


    open fun continuousFrame(op: Opcode, buffer: ByteBuffer, fin: Boolean): List<Framedata>? {
        require(!(op != Opcode.BINARY && op != Opcode.TEXT)) { "Only Opcode.BINARY or  Opcode.TEXT are allowed" }
        var bui: DataFrame? = null
        if (continuousFrameType != null) {
            bui = ContinuousFrame()
        } else {
            continuousFrameType = op
            if (op == Opcode.BINARY) {
                bui = BinaryFrame()
            } else if (op == Opcode.TEXT) {
                bui = TextFrame()
            }
        }
        bui!!.setPayload(buffer)
        bui.isFin = fin
        try {
            bui.isValid()
        } catch (e: InvalidDataException) {
            throw IllegalArgumentException(e) // can only happen when one builds close frames(Opcode.Close)
        }
        if (fin) {
            continuousFrameType = null
        } else {
            continuousFrameType = op
        }
        return listOf(bui as Framedata)
    }

    fun createHandshake(handshakedata: Handshakedata): List<ByteBuffer> {
        return createHandshake(handshakedata, true)
    }

    open fun createHandshake(
        handshakedata: Handshakedata,
        withcontent: Boolean
    ): List<ByteBuffer> {
        val bui = StringBuilder(100)
        if (handshakedata is ClientHandshake) {
            bui.append("GET ").append(handshakedata.resourceDescriptor).append(" HTTP/1.1")
        } else if (handshakedata is ServerHandshake) {
            bui.append("HTTP/1.1 101 ").append(handshakedata.httpStatusMessage)
        } else {
            throw java.lang.IllegalArgumentException("unknown role")
        }
        bui.append("\r\n")
        val it = handshakedata.iterateHttpFields()
        while (it.hasNext()) {
            val fieldname = it.next()
            val fieldvalue = handshakedata.getFieldValue(fieldname)
            bui.append(fieldname)
            bui.append(": ")
            bui.append(fieldvalue)
            bui.append("\r\n")
        }
        bui.append("\r\n")
        val httpheader = Charsetfunctions.asciiBytes(bui.toString())
        val content = if (withcontent) handshakedata.content else null
        val bytebuffer = ByteBuffer.allocate((content?.size ?: 0) + httpheader.size)
        bytebuffer.put(httpheader)
        if (content != null) {
            bytebuffer.put(content)
        }
        bytebuffer.flip()
        return listOf(bytebuffer)
    }

    @Throws(InvalidHandshakeException::class)
    open fun translateHandshake(buf: ByteBuffer?): Handshakedata? {
        return Draft.translateHandshakeHttp(buf, role)
    }

    open fun readVersion(handshakedata: Handshakedata): Int {
        val vers = handshakedata.getFieldValue("Sec-WebSocket-Version")
        if (vers.isNotEmpty()) {
            val v: Int
            return try {
                v = vers.trim { it <= ' ' }.toInt()
                v
            } catch (e: NumberFormatException) {
                -1
            }
        }
        return -1
    }

    @Throws(InvalidHandshakeException::class)
    abstract fun postProcessHandshakeRequestAsClient(request: ClientHandshakeBuilder): ClientHandshakeBuilder

    @Throws(InvalidDataException::class)
    open fun checkAlloc(bytecount: Int): Int {
        if (bytecount < 0) throw InvalidDataException(CloseFrame.PROTOCOL_ERROR, "Negative count")
        return bytecount
    }
}