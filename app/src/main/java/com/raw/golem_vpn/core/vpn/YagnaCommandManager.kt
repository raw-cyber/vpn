package com.raw.golem_vpn.core.vpn

import android.content.Context
import com.raw.golem_vpn.viewmodels.DEFAULT_VPN
import com.raw.golem_vpn.viewmodels.Subnet
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

const val YAGNA_LIB_NAME = "libyagna.so"

class CommandManagerConstructor @Inject constructor(@ApplicationContext context: Context) {
    private val subnet: String = DEFAULT_VPN.toString().lowercase()
    private val basicCommand: String =
        ".${context.applicationInfo.nativeLibraryDir}/${YAGNA_LIB_NAME}"
    private val params =
        "-d ${context.applicationInfo.dataDir}/yagna_out -g unix:${context.applicationInfo.dataDir}/yagna.sock"

    fun getEnvsConfig(): Array<String> {
        return arrayOf(
            "YAGNA_AUTOCONF_APPKEY=$YAGNA_APPKEY", "SUBNET=$subnet", "NETWORK=rinkeby",
            "CENTRAL_NET_HOST=172.20.10.2:15758", "YA_NET_TYPE=central"
        )
    }

    fun getRunServiceCommand(): String {
        return "$basicCommand service run $params"
    }

    fun getStopServiceCommand(): String {
        return "$basicCommand service stop $params"
    }

    fun getGetStatusCommand(): String {
        return "$basicCommand service status $params"
    }

    fun fundAccount(): String {
        return "$basicCommand payment fund $params"
    }

    fun initSender(): String {
        return "$basicCommand payment init --sender $params"
    }

    fun getCreateRequestorAccountCommand(): String {
        return "$basicCommand app-key create requestor $params"
    }

    fun getAccountInfo(): String {
        return "$basicCommand payment status --json $params"
    }
}

class YagnaCommandManager @Inject constructor(
    private val commandManagerConstructor: CommandManagerConstructor
) {
    private val commandExecutor = CommandExecutor(commandManagerConstructor.getGetStatusCommand())
    private val commandThread = Thread(commandExecutor)

    fun start() {
        commandThread.start()
    }

    fun stop() {
        commandThread.interrupt()
    }
}

class CommandExecutor(private val command: String) : Runnable {
    override fun run() {
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        val errBuffer = ByteArray(DEFAULT_BUFFER_SIZE)
        val process = Runtime.getRuntime().exec(command)
        while (!Thread.currentThread().isInterrupted) {
            process.inputStream.read(buffer, 0, buffer.size)
            process.errorStream.read(errBuffer, 0, errBuffer.size)
        }
        process.waitFor()
    }
}