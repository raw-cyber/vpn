package com.raw.golem_vpn.core.vpn

import android.app.PendingIntent
import android.content.pm.PackageManager
import android.os.ParcelFileDescriptor
import android.util.Log
import com.raw.golem_vpn.core.vpn.ws.CustomWebSocketClient
import com.raw.golem_vpn.data.source.LOCAL_IP
import com.raw.golem_vpn.utils.RawLog
import io.ktor.websocket.*
import org.java_websocket.enums.Opcode
import org.java_websocket.exceptions.WebsocketNotConnectedException
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.net.*
import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit

class RawVpnConnection(
    private val mService: RawVpnService, private val mConnectionId: Int,
    private val mServerName: String
) : Runnable {
    /**
     * Callback interface to let the [RawVpnService] know about new connections
     * and update the foreground notification with connection status.
     */
    interface OnEstablishListener {
        fun onEstablish(tunInterface: ParcelFileDescriptor?)
        fun onOutgoingPacketsSent(packetsSize: Long)
        fun onIncomingPacketsReceived(packetsSize: Long)
    }

    private var mConfigureIntent: PendingIntent? = null
    private var mOnEstablishListener: OnEstablishListener? = null
    fun setConfigureIntent(intent: PendingIntent?) {
        mConfigureIntent = intent
    }

    fun setOnEstablishListener(listener: OnEstablishListener) {
        mOnEstablishListener = listener
    }

    override fun run() {
        var attempt = 0
        try {
            Log.i(tag, "Starting")
            while (attempt < 10) {
                ++attempt
                val threadId = Thread.currentThread().id
                val interrupted = Thread.currentThread().isInterrupted
                RawLog.d(tag, "Thread id: $threadId, interrupted: $interrupted, attempt: $attempt")
                if (interrupted) {
                    Log.i(tag, "Interrupted, stopping")
                    break
                }
                // Reset the counter if we were connected.
                if (run(mServerName)) {
                    attempt = 0
                }
                // Sleep for a while. This also checks if we got interrupted.
                Thread.sleep(3000)
            }
            Log.i(tag, "Giving up")
        } catch (e: IOException) {
            Log.e(javaClass.name, "IOException. Connection failed, exiting " + e.message)
        } catch (e: InterruptedException) {
            Log.e(javaClass.name, "InterruptedException.Connection failed, exiting " + e.message)
            Log.e(javaClass.name, e.printStackTrace().toString())
        } catch (e: IllegalArgumentException) {
            Log.e(
                javaClass.name,
                "IllegalArgumentException. Connection failed, exiting " + e.message
            )
        }
    }

    private fun String.decodeHex(): ByteArray {
        check(length % 2 == 0) { "Must have an even length" }

        return chunked(2)
            .map { it.toInt(16).toByte() }
            .toByteArray()
    }

    @Throws(
        IOException::class,
        InterruptedException::class,
        IllegalArgumentException::class,
        URISyntaxException::class
    )
    fun run(serverName: String): Boolean {
        var connected = false
        var iface: ParcelFileDescriptor? = null
        // Packets to be sent are queued in this input stream.
        var inStream: FileInputStream? = null
        // Packets received need to be written to this output stream.
        var outStream: FileOutputStream? = null
        // WebSocket URL
        val uri =
            URI(serverName)
        val wsConnection = CustomWebSocketClient(uri)
        try {
            wsConnection.openConnection()
            Thread.sleep(500)
            iface = establishConnection()
            Log.d(tag, "Interface is: $iface")
            val fd = iface!!.fileDescriptor
            inStream = FileInputStream(fd)
            outStream = FileOutputStream(fd)
            connected = false
            val packet = ByteBuffer.allocate(MAX_PACKET_SIZE)
            while (true) {
                var idle = true
                connected = true

                // Read the outgoing packet from the input stream.
                val length = inStream.read(packet.array())
                if (length > 0) {
                    // Write the outgoing packet to the tunnel.
                    packet.limit(length)
                    mOnEstablishListener?.onOutgoingPacketsSent(packet.limit().toLong())
                    wsConnection.send(packet)
                    packet.clear()
                }
                // Read the incoming packet from the channel.
                val frames = wsConnection.read()
                if (frames.size > 0) {
                    val pingFrames = frames.filter { it.opcode == Opcode.PING }
                    val pongFrames = frames.filter { it.opcode == Opcode.PONG }
                    if (pingFrames.isNotEmpty()) wsConnection.sendPong()
                    // Write the incoming packet to the output stream.
                    val binaryFrames = frames.filter { it.opcode == Opcode.BINARY }
                    for (frame in binaryFrames) {
                        mOnEstablishListener?.onIncomingPacketsReceived(frame.payloadData.array().size.toLong())
                        try {
                            val ethernetFrame = frame.payloadData.array()
                            val ipFrame = ethernetFrame.copyOfRange(14, ethernetFrame.size)
                            outStream.write(ipFrame)
                        } catch (e: Exception) {
                            Log.e(tag, "Failed to write packet to tun descriptor", e)
                        }
                    }
                    idle = false
                }
            }
        } catch (ex: Exception) {
            when (ex) {
                is SocketTimeoutException, is IllegalStateException, is ConnectException -> {
                    RawLog.e(tag, "Failed to connect to server $ex")
                }

                is WebsocketNotConnectedException -> {
                    RawLog.e(tag, "WebsocketNotConnectedException $ex")
                }

                is IOException -> {
                    RawLog.e(tag, "IOException $ex")
                    RawLog.e(tag, "${ex.printStackTrace()}")
                }

                else -> {
                    RawLog.e(tag, "${ex.printStackTrace()}")
                }
            }
        } finally {
            if (iface != null) {
                try {
                    iface.close()
                } catch (e: IOException) {
                    Log.e(tag, "Failed to close interface", e)
                }
            }
            if (inStream != null) {
                try {
                    inStream.close()
                } catch (e: IOException) {
                    Log.e(tag, "Failed to close input stream", e)
                }
            }
            wsConnection.close(1000, "Bye bye")
        }
        return connected
    }

    @Throws(IllegalArgumentException::class)
    private fun establishConnection(): ParcelFileDescriptor? {
        var vpnInterface: ParcelFileDescriptor? = null
        val builder = mService.Builder()
        builder.addAddress(LOCAL_IP, 24)
        builder.addRoute("0.0.0.0", 0)
        builder.setMtu(1200)
        //prevent VPN connections from looping to itself
        try {
            Log.d(tag, "Package name: " + mService.packageName)
            builder.addDisallowedApplication(mService.packageName)
        } catch (e: PackageManager.NameNotFoundException) {
            throw RuntimeException("this should never happen")
        }
        builder.setSession(mServerName).setConfigureIntent(mConfigureIntent!!)
        synchronized(mService) {
            vpnInterface = builder.establish()
            if (mOnEstablishListener != null) {
                mOnEstablishListener!!.onEstablish(vpnInterface)
            }
//            RawLog.i(tag, "New interface: $vpnInterface (only 0.0.0.0 route)")
        }
        return vpnInterface
    }

    private val tag: String
        get() = RawVpnConnection::class.java.simpleName + "[" + mConnectionId + "]"

    companion object {
        /**
         * Maximum packet size is constrained by the MTU, which is given as a signed short.
         */
        private const val MAX_PACKET_SIZE = Short.MAX_VALUE.toInt()

        /**
         * Time to wait in between losing the connection and retrying.
         */
        private val RECONNECT_WAIT_MS = TimeUnit.SECONDS.toMillis(3)
    }
}