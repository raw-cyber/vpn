package com.raw.golem_vpn

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class VpnApplication : Application() {}