package com.raw.golem_vpn.data.source

import com.raw.golem_vpn.data.DemandRequest

interface DemandDataSource {
    suspend fun createDemand(demandRequest: DemandRequest): String?
    suspend fun cancelDemand(demandId: String): Boolean
    suspend fun createCounterProposal(proposalId: String)
}