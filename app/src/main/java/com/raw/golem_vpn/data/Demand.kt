package com.raw.golem_vpn.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DemandRequest(
    val properties: DemandProperties,
    val constraints: String,
) {
    companion object {
        fun getConstraints(subnet: String): String {
            val defaultConstrains =
                "(&(golem.node.debug.subnet=%%SUBNET%%)(golem.com.payment.platform.erc20-rinkeby-tglm.address=*)(golem.com.pricing.model=linear)(golem.runtime.name=outbound)(golem.runtime.capabilities=outbound))"
            return defaultConstrains.replace("%%SUBNET%%", subnet)
        }
    }
}

@Serializable
data class DemandProperties(
    @SerialName("golem.com.payment.debit-notes.accept-timeout?") val acceptTimeout: Int,
    @SerialName("golem.node.debug.subnet") val subnet: String,
    @SerialName("golem.com.payment.chosen-platform") val chosenPlatform: String,
    @SerialName("golem.com.payment.platform.erc20-rinkeby-tglm.address") val senderAddress: String,
    @SerialName("golem.srv.comp.expiration") val expiration: Long,
)