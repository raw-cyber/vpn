package com.raw.golem_vpn.data.source

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.raw.golem_vpn.data.AgreementRequest
import com.raw.golem_vpn.data.Allocation
import com.raw.golem_vpn.data.DemandRequest
import com.raw.golem_vpn.data.ProposalEvent
import com.raw.golem_vpn.data.createBaseExecCommand
import com.raw.golem_vpn.data.source.DefaultAgreementHandlerRepository.NegotiationsKeys.DEMAND_ID
import com.raw.golem_vpn.di.IoDispatcher
import com.raw.golem_vpn.utils.AgreementStatus
import com.raw.golem_vpn.utils.NetworkConfigurationStatus
import com.raw.golem_vpn.utils.ProposalStatus
import com.raw.golem_vpn.utils.RawLog
import com.raw.golem_vpn.utils.VpnStatus
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter


const val NEGOTIATIONS_KEY = "negotiations"
const val DEMAND_KEY = "demand_id"

const val LOCAL_IP = "192.168.8.1"
const val REMOTE_IP = "192.168.8.7"

class DefaultAgreementHandlerRepository(
    private val remoteAccountDataSource: AccountDataSource,
    private val remoteAgreementDataSource: AgreementDataSource,
    private val remoteDemandDataSource: DemandDataSource,
    private val remoteProposalsDataSource: ProposalsDataSource,
    private val remoteNetworkDataSource: NetworksDataSource,
    private val vpnStatusManager: VpnStatusManager,
    private val vpnServiceManager: VpnServiceManager,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    @ApplicationContext private val applicationContext: Context
) : AgreementHandlerRepository {

    object NegotiationsKeys {
        val DEMAND_ID = stringPreferencesKey(DEMAND_KEY)
    }

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = NEGOTIATIONS_KEY)

    override val demandId: Flow<String> = applicationContext.dataStore.data.map { preferences ->
        preferences[DEMAND_ID] ?: ""
    }

    private val allocationId = MutableStateFlow("")

    override suspend fun findProviderManual(demand: DemandRequest) {
        coroutineScope {
            launch(ioDispatcher) {
                val demandId = remoteDemandDataSource.createDemand(demand) ?: return@launch
                applicationContext.dataStore.edit { preferences ->
                    preferences[DEMAND_ID] = demandId
                }
                remoteProposalsDataSource.refreshProposals(demandId)
            }
        }
    }

    private suspend fun getProposalsLocal(demandId: String): List<ProposalEvent> {
        val proposals = try {
            var proposalEvents = emptyList<ProposalEvent>()
            while (true) {
                proposalEvents = remoteProposalsDataSource.getProposals(demandId)
                if (proposalEvents.isEmpty()) {
                    delay(1000)
                } else {
                    break
                }
            }
            proposalEvents
        } catch (e: CancellationException) {
            return emptyList()
        } catch (e: Exception) {
            RawLog.d("findProvider", "Exception: ${e.message}")
            emptyList()
        }
        return proposals
    }

    override suspend fun findProvider(demand: DemandRequest) {
        vpnStatusManager.updateVpnStatus(VpnStatus.CONNECTING, "")
        vpnStatusManager.updateAgreementStatus(AgreementStatus.PENDING, "Looking for proposals")
        val demandId = remoteDemandDataSource.createDemand(demand) ?: return
        var agreementSigned = false
        var proposals = emptyList<ProposalEvent>()
        var counterProposalEvent = emptyList<ProposalEvent>()

        while (!agreementSigned) {
            delay(1000)
            proposals = getProposalsLocal(demandId)
            if (proposals.isEmpty()) {
                vpnStatusManager.updateProposalStatus(
                    ProposalStatus.PENDING, "Looking for proposals"
                )
                continue
            }
            RawLog.d("findProvider", "Proposals: ${proposals.size}")
            vpnStatusManager.updateAgreementStatus(
                AgreementStatus.PENDING,
                "Found " +
                        if (proposals.size > 1) "${proposals.size} proposals"
                        else "${proposals.size} proposal"
            )
            remoteDemandDataSource.createCounterProposal(proposals.last().proposal.proposalId)
            RawLog.d("findProvider", "Counter proposal created")
            while (counterProposalEvent.isEmpty()) {
                delay(1000)
                counterProposalEvent =
                    getProposalsLocal(demandId).filter { proposalEvent -> proposalEvent.proposal.state == "Draft" }
                RawLog.d("findProvider", "Counter proposals: ${counterProposalEvent.size}")
                RawLog.d(
                    "findProvider",
                    "Counter proposals: ${counterProposalEvent.firstOrNull()?.proposal}"
                )
            }
            val counterProposal = counterProposalEvent.first()
            val agreementValidTo = LocalDateTime.now(ZoneOffset.UTC).plusSeconds(60)

            // Format the current time as a string in the desired format
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'")

            val agreementValidToFormatted = agreementValidTo.format(formatter).toString()
            val agreementRequest =
                AgreementRequest(counterProposal.proposal.proposalId, agreementValidToFormatted)
            val allocationValidaTo = LocalDateTime.now(ZoneOffset.UTC).plusHours(24)
            val allocationValidToFormatted = allocationValidaTo.format(formatter).toString()
            val identity = remoteAccountDataSource.getAccountInfo()?.identity ?: ""
            val allocation = Allocation(
                address = identity, totalAmount = "10", timeout = allocationValidToFormatted
            )
            agreementSigned = makeAgreement(allocation, agreementRequest)

        }
        vpnStatusManager.updateAgreementStatus(AgreementStatus.ACCEPTED, "Agreement signed")
        val demandCancellationStatus = remoteDemandDataSource.cancelDemand(demandId)
        RawLog.d("findProvider", "Demand cancelled: $demandCancellationStatus")
        vpnStatusManager.updateNetworkStatus(
            NetworkConfigurationStatus.PENDING,
            "Setting up network"
        )
        vpnServiceManager.acceptPayments()
        val networkResponse = remoteNetworkDataSource.createNetwork(
            ip = "192.168.8.0/24", mask = "255.255.255.0", gateway = REMOTE_IP
        )

        if (networkResponse == null) {
            RawLog.d("findProvider", "Network response is null")
            vpnStatusManager.updateNetworkStatus(
                NetworkConfigurationStatus.FAILED,
                "Network set up failed"
            )
            return
        }
        vpnStatusManager.updateNetworkStatus(
            NetworkConfigurationStatus.CONFIGURED,
            "Network created"
        )


        val execCommand = createBaseExecCommand(networkResponse.id, LOCAL_IP, REMOTE_IP)
        val command = Json.encodeToJsonElement(execCommand).toString()
        val agreementActivity = remoteAgreementDataSource.getActivityStream().take(1).first()
        val agreement = remoteAgreementDataSource.getAgreementStream().take(1).first()
        if (agreementActivity == null) {
            RawLog.d("findProvider", "Agreement activity is null")
            return
        }
        if (agreement == null) {
            RawLog.d("findProvider", "Agreement is null")
            return
        }
        remoteNetworkDataSource.execCommandInActivity(
            agreementActivity.activityId, command
        ).also {
            remoteNetworkDataSource.assignIpToNetwork(
                networkResponse.id, agreement.offer.providerId, REMOTE_IP
            )
        }
        val networkAddress =
            "ws://127.0.0.1:7465/net-api/v2/vpn/net/${networkResponse.id}/raw/from/${LOCAL_IP}/to/${REMOTE_IP}"
        vpnServiceManager.updatePreference(VpnPreferenceKeys.ADDRESS, networkAddress)
            .also { vpnServiceManager.connectToVpn() }
    }

    override suspend fun makeCounterProposal(proposalId: String) {
        remoteDemandDataSource.createCounterProposal(proposalId)
    }

    override suspend fun acceptPayments() {
        remoteAgreementDataSource.acceptPayments(allocationId.value)
    }

    override suspend fun makeAgreement(
        allocation: Allocation, agreement: AgreementRequest
    ): Boolean {
        allocationId.value = remoteAgreementDataSource.createAllocation(allocation) ?: ""
        return remoteAgreementDataSource.makeAgreement(agreement)
    }

    override suspend fun terminateAgreement() {
        remoteAgreementDataSource.terminateAgreement()
        remoteAgreementDataSource.payInvoice(allocationId.value)
        remoteAgreementDataSource.releaseAllocation(allocationId.value)
    }

    override fun getProposalStream(proposalId: String): Flow<ProposalEvent?> {
        return remoteProposalsDataSource.getProposalStream(proposalId)
    }

    override fun getProposalsStream(): Flow<List<ProposalEvent>> {
        return remoteProposalsDataSource.getProposalsStream()
    }

    override suspend fun getProposal(demandId: String, proposalId: String): ProposalEvent? {
        return remoteProposalsDataSource.getProposal(demandId, proposalId)
    }

    override suspend fun getProposals(demandId: String): List<ProposalEvent> {
        return remoteProposalsDataSource.getProposals(demandId)
    }

    override suspend fun refreshProposals(demandId: String) {
        remoteProposalsDataSource.refreshProposals(demandId)
    }

}