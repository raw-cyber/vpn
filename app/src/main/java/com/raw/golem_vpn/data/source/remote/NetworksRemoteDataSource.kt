package com.raw.golem_vpn.data.source.remote

import com.raw.golem_vpn.data.*
import com.raw.golem_vpn.data.source.NetworksDataSource
import com.raw.golem_vpn.utils.RawLog
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject


class NetworksRemoteDataSource @Inject constructor(
    private val client: HttpClient
) : NetworksDataSource {
    private val tag: String = this::class.java.simpleName
    private var NETWORK_SERVICE_DATA = LinkedHashMap<String, Network>()
    private val observableNetworks = MutableStateFlow<List<Network>>(listOf())

    override fun getNetworksStream(): Flow<List<Network>> {
        return observableNetworks
    }

    override suspend fun getNetworks(): List<Network> {
        return client.get("/net-api/v2/vpn/net").body()
    }

    override suspend fun refreshNetworks() {
        observableNetworks.value = getNetworks()
    }

    override suspend fun createNetwork(ip: String, mask: String, gateway: String): Network? {
        var network: Network? = null
        try {
            val res = client.post("/net-api/v2/vpn/net") {
                contentType(ContentType.Application.Json)
                setBody(Network(ip, mask, gateway))
            }
            network = res.body<Network>()
            RawLog.d(tag, "createNetwork Status code ${res.status.value}")
            checkNetwork(network)
        } catch (ConnectException: Exception) {
            RawLog.e(tag, "ConnectException: ${ConnectException.message}")
        }
        return network
    }

    private suspend fun checkNetwork(network: Network): Boolean {
        val res = client.get("/net-api/v2/vpn/net/${network.id}/addresses")
        if (res.status.isSuccess()) {
            RawLog.d(tag, "checkNetwork Response: ${res.body<List<NetworkList>>()}")
        } else {
            RawLog.d(tag, "Network check failed")
        }
        return res.status.isSuccess()
    }

    override suspend fun execCommandInActivity(activityId: String, command: String): Boolean {
        val res = client.post("/activity-api/v1/activity/${activityId}/exec") {
            setBody(command)
        }
        if (res.status.isSuccess()) {
            RawLog.d(tag, "execCommandInActivity: Command execution success")
            val responseBatchId = res.body<String>().replace("\"", "")
            while (!checkBatch(activityId, responseBatchId)) {
                delay(300)
            }
        } else {
            RawLog.d(tag, "execCommandInActivity: Command execution failed")
        }
        return res.status.isSuccess()
    }


    private suspend fun checkBatch(activityId: String, batchId: String): Boolean {
        val res = client.get("/activity-api/v1/activity/${activityId}/exec/${batchId}")
        val response = res.body<List<BatchCheck>>()
        RawLog.d(tag, "Batch check response: $response")
        for (batch in response) {
            if (batch.isBatchFinished) {
                return true
            }
        }
        return false
    }

    override suspend fun assignIpToNetwork(
        networkId: String,
        providerId: String,
        ipAddress: String
    ): Boolean {
        val networkAssignment = NetworkAssignment(providerId, ipAddress)
        val res = client.post("/net-api/v2/vpn/net/${networkId}/nodes") {
            setBody(networkAssignment)
        }
        if (res.status.isSuccess()) {
            val nodes = client.get("/net-api/v2/vpn/net/${networkId}/nodes")
            RawLog.d(tag, "Nodes: response: ${nodes.body<String>()}")
        } else {
            RawLog.d(tag, "assignIpToNetwork: Failed")
        }
        return res.status.isSuccess()
    }
}