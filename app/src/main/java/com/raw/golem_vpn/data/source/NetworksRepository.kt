package com.raw.golem_vpn.data.source

import com.raw.golem_vpn.data.Network
import kotlinx.coroutines.flow.Flow

interface NetworksRepository {

    fun getNetworksStream(): Flow<List<Network>>
    suspend fun getNetworks(): List<Network>
    suspend fun refreshNetworks()
    suspend fun configureNetwork(ip: String, mask: String, gateway: String): Network?
    suspend fun execCommandInActivity(activityId: String, command: String): Boolean
    suspend fun assignIpToNetwork(networkId: String, providerId: String, ipAddress: String): Boolean
}