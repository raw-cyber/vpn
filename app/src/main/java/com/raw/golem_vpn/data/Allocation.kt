package com.raw.golem_vpn.data

import kotlinx.serialization.Serializable

@Serializable
data class Allocation(
    val address: String,
    val totalAmount: String,
    val timeout: String,
    val paymentPlatform: String = "erc20-rinkeby-tglm",
    val makeDeposit: Boolean = false,
)

@Serializable
data class AllocationResponse(
    val allocationId: String,
    val address: String,
    val paymentPlatform: String,
    val totalAmount: Int,
    val spentAmount: Int,
    val remainingAmount: Int,
    val timestamp: String,
    val timeout: String,
    val makeDeposit: Boolean
)