package com.raw.golem_vpn.data.source

import com.raw.golem_vpn.data.Network
import kotlinx.coroutines.flow.Flow

class DefaultNetworksRepository(
    private val remoteNetworksRepository: NetworksDataSource
) : NetworksRepository {
    override fun getNetworksStream(): Flow<List<Network>> {
        return remoteNetworksRepository.getNetworksStream()
    }

    override suspend fun getNetworks(): List<Network> {
        return remoteNetworksRepository.getNetworks()
    }

    override suspend fun refreshNetworks() {
        return remoteNetworksRepository.refreshNetworks()
    }

    override suspend fun configureNetwork(
        ip: String,
        mask: String,
        gateway: String
    ): Network? {
        return remoteNetworksRepository.createNetwork(ip = ip, mask = mask, gateway = gateway)
    }

    override suspend fun execCommandInActivity(activityId: String, command: String): Boolean {
        return remoteNetworksRepository.execCommandInActivity(
            activityId = activityId,
            command = command
        )
    }

    override suspend fun assignIpToNetwork(networkId: String, providerId: String, ipAddress: String): Boolean {
        return remoteNetworksRepository.assignIpToNetwork(
            networkId = networkId,
            providerId = providerId,
            ipAddress = ipAddress
        )
    }
}