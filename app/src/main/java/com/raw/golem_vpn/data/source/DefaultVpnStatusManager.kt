package com.raw.golem_vpn.data.source

import com.raw.golem_vpn.data.AgreementStatusData
import com.raw.golem_vpn.data.NetworkStatusData
import com.raw.golem_vpn.data.ProposalStatusData
import com.raw.golem_vpn.data.StatusData
import com.raw.golem_vpn.data.VpnStatusData
import com.raw.golem_vpn.utils.AgreementStatus
import com.raw.golem_vpn.utils.NetworkConfigurationStatus
import com.raw.golem_vpn.utils.ProposalStatus
import com.raw.golem_vpn.utils.VpnStatus
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

class DefaultVpnStatusManager @Inject constructor() : VpnStatusManager {
    private val observableVpnStatus = MutableStateFlow(VpnStatusData(VpnStatus.DISCONNECTED))
    private val observableProposalStatus = MutableStateFlow(ProposalStatusData(ProposalStatus.NONE))
    private val observableAgreementStatus =
        MutableStateFlow(AgreementStatusData(AgreementStatus.NONE))
    private val observableNetworkConfigurationStatus =
        MutableStateFlow(NetworkStatusData(NetworkConfigurationStatus.NOT_CONFIGURED))

    override fun getVpnStatusStream(): Flow<VpnStatusData> {
        return observableVpnStatus
    }

    override fun getProposalStatusStream(): Flow<ProposalStatusData> {
        return observableProposalStatus
    }

    override fun getAgreementStatusStream(): Flow<AgreementStatusData> {
        return observableAgreementStatus
    }

    override fun getNetworkStatusStream(): Flow<NetworkStatusData> {
        return observableNetworkConfigurationStatus
    }

    override fun updateVpnStatus(vpnStatus: VpnStatus, message: String?) {
        observableVpnStatus.value = VpnStatusData(vpnStatus, message ?: "")
    }

    override fun updateProposalStatus(proposalStatus: ProposalStatus, message: String?) {
        observableProposalStatus.value = ProposalStatusData(proposalStatus, message ?: "")
    }

    override fun updateAgreementStatus(agreementStatus: AgreementStatus, message: String?) {
        observableAgreementStatus.value = AgreementStatusData(agreementStatus, message ?: "")
    }

    override fun updateNetworkStatus(networkStatus: NetworkConfigurationStatus, message: String?) {
        observableNetworkConfigurationStatus.value =
            NetworkStatusData(networkStatus, message ?: "")
    }

}
