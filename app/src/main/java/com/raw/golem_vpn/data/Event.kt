package com.raw.golem_vpn.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.random.Random

@Serializable
data class ProposalEvent(
    val eventType: String,
    val eventDate: String,
    val proposal: Proposal,
)

@Serializable
data class Proposal(
    val proposalId: String,
    val issuerId: String,
    val state: String,
    val timestamp: String,
    val properties: ProposalProperties,
    val constraints: String,
    val prevProposalId: String? = null,
)

@Serializable
data class InvoiceEvent(
    val invoiceId: String,
    val eventType: String,
    val eventDate: String,
)

@Serializable
data class Invoice(
    val invoiceId: String,
    val issuerId: String,
    val recipientId: String,
    val payeeAddr: String,
    val payerAddr: String,
    val paymentPlatform: String,
    val timestamp: String,
    val agreementId: String,
    val activityIds: List<String>,
    val amount: String,
    val paymentDueDate: String,
    val status: String
)


@Serializable
data class ProposalProperties(
    @SerialName("golem.activity.caps.transfer.protocol") val golemActivityCapsTransferProtocol: List<String>,
    @SerialName("golem.com.payment.debit-notes.accept-timeout?") val golemComPaymentDebitNotesAcceptTimeout: Int,
    @SerialName("golem.com.payment.platform.erc20-rinkeby-tglm.address") val golemComPaymentPlatformErc20RinkebyTglmAddress: String,
    @SerialName("golem.com.payment.platform.zksync-rinkeby-tglm.address") val golemComPaymentPlatformZksyncRinkebyTglmAddress: String,
    @SerialName("golem.com.pricing.model") val golemComPricingModel: String,
    @SerialName("golem.com.pricing.model.linear.coeffs") val golemComPricingModelLinearCoeffs: List<Double>,
    @SerialName("golem.com.scheme") val golemComScheme: String,
    @SerialName("golem.com.scheme.payu.debit-note.interval-sec?") val golemComSchemePayuDebitNoteIntervalSec: Int? = null,
    @SerialName("golem.com.scheme.payu.payment-timeout-sec?") val golemComSchemePayuPaymentTimeoutSec: Int? = null,
    @SerialName("golem.com.usage.vector") val golemComUsageVector: List<String>,
    @SerialName("golem.inf.cpu.architecture") val golemInfCpuArchitecture: String,
    @SerialName("golem.inf.cpu.cores") val golemInfCpuCores: Int,
    @SerialName("golem.inf.cpu.threads") val golemInfCpuThreads: Int,
    @SerialName("golem.inf.mem.gib") val golemInfMemGib: Double,
    @SerialName("golem.inf.storage.gib") val golemInfStorageGib: Double,
    @SerialName("golem.node.debug.subnet") val golemNodeDebugSubnet: String,
    @SerialName("golem.node.id.name") val golemNodeIdName: String,
    @SerialName("golem.node.net.is-public") val golemNodeNetIsPublic: Boolean,
    @SerialName("golem.runtime.capabilities") val golemRuntimeCapabilities: List<String>,
    @SerialName("golem.runtime.name") val golemRuntimeName: String,
    @SerialName("golem.runtime.version") val golemRuntimeVersion: String,
    @SerialName("golem.srv.caps.multi-activity") val golemSrvCapsMultiActivity: Boolean,
    @SerialName("golem.srv.caps.payload-manifest") val golemSrvCapsPayloadManifest: Boolean,
)

class ProposalEventGenerator {
    companion object {
        fun generateProposals(count: Int): List<ProposalEvent> {
            val proposals = mutableListOf<ProposalEvent>()
            for (i in 0 until count) {
                proposals.add(generateProposalEvent())
            }
            return proposals
        }


        private fun generateProposalEvent(): ProposalEvent {
            val eventType = "ProposalEvent"
            val eventDate = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)
            val proposal = generateProposal()
            return ProposalEvent(eventType, eventDate, proposal)
        }

        private fun generateProposal(): Proposal {
            val proposalId = Random.nextInt(1000).toString()
            val issuerId = Random.nextInt(1000).toString()
            val state = "pending"
            val timestamp = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)
            val properties = generateProposalProperties()
            val constraints = "constraints"
            return Proposal(proposalId, issuerId, state, timestamp, properties, constraints)
        }

        private fun generateProposalProperties(): ProposalProperties {
            return ProposalProperties(
                listOf("protocol1", "protocol2", "protocol3"),
                Random.nextInt(1000),
                "address1",
                "address2",
                "model",
                listOf(0.1, 0.2, 0.3),
                "scheme",
                Random.nextInt(1000),
                Random.nextInt(1000),
                listOf("vector1", "vector2", "vector3"),
                "x86",
                4,
                8,
                16.0,
                100.0,
                "subnet",
                "node",
                true,
                listOf("capability1", "capability2", "capability3"),
                "runtime",
                "version",
                golemSrvCapsMultiActivity = true,
                golemSrvCapsPayloadManifest = true,
            )
        }
    }
}