package com.raw.golem_vpn.data.source

import androidx.datastore.preferences.core.Preferences
import kotlinx.coroutines.flow.Flow

interface DataStoreRepository {
    fun onBoardingState(): Flow<Boolean>
    suspend fun updatePreference(key: Preferences.Key<String>, value: String)
    suspend fun updatePreference(key: Preferences.Key<Boolean>, value: Boolean)
    suspend fun updatePreference(key: Preferences.Key<Int>, value: Int)
}