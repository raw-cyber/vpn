package com.raw.golem_vpn.data.source

import androidx.datastore.preferences.core.Preferences
import com.raw.golem_vpn.data.NetworkTraffic
import kotlinx.coroutines.flow.Flow

interface NetworkTrafficCounterDataSource {
    fun getNetworkTrafficStream(): Flow<NetworkTraffic>
    suspend fun updateIncomingNetworkDataPreference(value: Long)
    suspend fun updateOutgoingNetworkDataPreference(value: Long)
}