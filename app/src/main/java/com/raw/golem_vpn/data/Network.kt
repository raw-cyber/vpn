package com.raw.golem_vpn.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class Network(
    val ip: String,
    val mask: String,
    val gateway: String,
    @SerialName("id") val idOrNull: String? = "",
    @Transient val connected: Boolean = false,
) {

    val id: String
        get() = idOrNull ?: ""

    override fun toString(): String {
        return "Network(id='$id', port=$mask, address='$ip')"
    }

    val isActive: Boolean
        get() = connected
}


@Serializable
data class NetworkList(
    val ip: String,
)

@Serializable
data class NetworkActivity(
    val activityId: String,
)

@Serializable
data class NetworkAssignment(
    val id: String,
    val ip: String,
)