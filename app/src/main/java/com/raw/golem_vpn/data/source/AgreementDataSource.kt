package com.raw.golem_vpn.data.source

import com.raw.golem_vpn.data.Agreement
import com.raw.golem_vpn.data.AgreementRequest
import com.raw.golem_vpn.data.Allocation
import com.raw.golem_vpn.data.NetworkActivity
import kotlinx.coroutines.flow.Flow

interface AgreementDataSource {
    fun getAgreementStream(): Flow<Agreement?>
    fun getActivityStream(): Flow<NetworkActivity?>
    suspend fun makeAgreement(agreementRequest: AgreementRequest): Boolean
    suspend fun terminateAgreement()
    suspend fun acceptPayments(allocationId: String)
    suspend fun payInvoice(allocationId: String)
    suspend fun createAllocation(allocation: Allocation): String?
    suspend fun releaseAllocation(allocationId: String)
}