package com.raw.golem_vpn.data.source

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import javax.inject.Inject

const val DATA_STORE = "data_store"

object DataKey {
    val onBoardingKey = booleanPreferencesKey(name = "on_boarding_completed")
}

class
DefaultDataStoreRepository @Inject constructor(
    @ApplicationContext private val applicationContext: Context,
) : DataStoreRepository {


    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = DATA_STORE)

    override fun onBoardingState(): Flow<Boolean> {
        return applicationContext.dataStore.data.catch {
            if (it is Exception) {
                emit(emptyPreferences())
            } else {
                throw it
            }
        }.map { preferences ->
            preferences[DataKey.onBoardingKey] ?: false
        }
    }

    override suspend fun updatePreference(key: Preferences.Key<String>, value: String) {
        applicationContext.dataStore.edit { preferences ->
            preferences[key] = value
        }
    }

    override suspend fun updatePreference(key: Preferences.Key<Boolean>, value: Boolean) {
        applicationContext.dataStore.edit { preferences ->
            preferences[key] = value
        }
    }

    override suspend fun updatePreference(key: Preferences.Key<Int>, value: Int) {
        applicationContext.dataStore.edit { preferences ->
            preferences[key] = value
        }
    }
}