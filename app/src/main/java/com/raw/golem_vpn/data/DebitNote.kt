package com.raw.golem_vpn.data

import kotlinx.serialization.Serializable

@Serializable
data class BasicDebitNote(
    val debitNoteId: String,
    val eventDate: String,
    val eventType: String
)

@Serializable
data class DebitNote(
    val debitNoteId: String,
    val issuerId: String,
    val recipientId: String,
    val payeeAddr: String,
    val payerAddr: String,
    val paymentPlatform: String,
    val timestamp: String,
    val agreementId: String,
    val activityId: String,
    val totalAmountDue: String,
    val usageCounterVector: List<Double>,
    val status: String
)

@Serializable
data class AcceptDebitNoteRequest(
    val totalAmountAccepted: String,
    val allocationId: String,
)