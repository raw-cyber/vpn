package com.raw.golem_vpn.data.source.local

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.raw.golem_vpn.data.NetworkTraffic
import com.raw.golem_vpn.data.source.NetworkTrafficCounterDataSource
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

const val NETWORK_PREF_DATA_KEY = "network_pref_data_key"

object NetworkTrafficCounterPrefKeys {
    val TOTAL_OUTGOING_TRAFFIC = longPreferencesKey("total_outgoing_traffic")
    val TOTAL_INCOMING_TRAFFIC = longPreferencesKey("total_incoming_traffic")
    val OUTGOING_TRAFFIC = longPreferencesKey("outgoing_traffic")
    val INCOMING_TRAFFIC = longPreferencesKey("incoming_traffic")
}

class NetworkTrafficCounterLocalDataSource @Inject constructor(
    @ApplicationContext private val applicationContext: Context,
) : NetworkTrafficCounterDataSource {

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = NETWORK_PREF_DATA_KEY)

    override fun getNetworkTrafficStream(): Flow<NetworkTraffic> {
        return applicationContext.dataStore.data.map { preferences ->
            NetworkTraffic(
                preferences[NetworkTrafficCounterPrefKeys.INCOMING_TRAFFIC] ?: 0,
                preferences[NetworkTrafficCounterPrefKeys.OUTGOING_TRAFFIC] ?: 0,
                preferences[NetworkTrafficCounterPrefKeys.TOTAL_INCOMING_TRAFFIC] ?: 0,
                preferences[NetworkTrafficCounterPrefKeys.TOTAL_OUTGOING_TRAFFIC] ?: 0,
            )
        }
    }

    override suspend fun updateOutgoingNetworkDataPreference(value: Long) {
        applicationContext.dataStore.edit { preferences ->
            preferences[NetworkTrafficCounterPrefKeys.OUTGOING_TRAFFIC] = value
        }
        applicationContext.dataStore.edit {
            val totalOutgoingTraffic = it[NetworkTrafficCounterPrefKeys.TOTAL_OUTGOING_TRAFFIC] ?: 0
            it[NetworkTrafficCounterPrefKeys.TOTAL_OUTGOING_TRAFFIC] = totalOutgoingTraffic + value
        }
    }

    override suspend fun updateIncomingNetworkDataPreference(value: Long) {
        applicationContext.dataStore.edit { preferences ->
            preferences[NetworkTrafficCounterPrefKeys.INCOMING_TRAFFIC] = value
        }
        applicationContext.dataStore.edit {
            val totalIncomingTraffic = it[NetworkTrafficCounterPrefKeys.TOTAL_INCOMING_TRAFFIC] ?: 0
            it[NetworkTrafficCounterPrefKeys.TOTAL_INCOMING_TRAFFIC] = totalIncomingTraffic + value
        }
    }
}
