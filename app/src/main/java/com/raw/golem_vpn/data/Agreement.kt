package com.raw.golem_vpn.data

import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName

@Serializable
data class AgreementRequest(
    val proposalId: String,
    val validTo: String,
)

@Serializable
data class TerminateAgreementRequest(val message: String, val extra: Map<String, String>? = null)

@Serializable
data class AcceptInvoiceRequest(
    val totalAmountAccepted: String,
    val allocationId: String,
)

@Serializable
data class Agreement(
    val agreementId: String,
    val offer: Offer,
)

@Serializable
data class Offer(
    val providerId: String,
    val properties: OfferProperties,
)

@Serializable
data class ActivityRequest(
    val agreementId: String,
    val requestorPubKey: String? = null,
)

@Serializable
data class OfferProperties(
    @SerialName("golem.activity.caps.transfer.protocol")
    val golemActivityCapsTransferProtocol: List<String>,

    @SerialName("golem.com.payment.debit-notes.accept-timeout?")
    val golemComPaymentDebitNotesAcceptTimeout: Int,

    @SerialName("golem.com.payment.platform.erc20-rinkeby-tglm.address")
    val golemComPaymentPlatformErc20RinkebyTglmAddress: String,

    @SerialName("golem.com.payment.platform.zksync-rinkeby-tglm.address")
    val golemComPaymentPlatformZksyncRinkebyTglmAddress: String,

    @SerialName("golem.com.pricing.model")
    val golemComPricingModel: String,

    @SerialName("golem.com.pricing.model.linear.coeffs")
    val golemComPricingModelLinearCoeffs: List<Double>,

    @SerialName("golem.com.scheme")
    val golemComScheme: String,

    @SerialName("golem.com.usage.vector")
    val golemComUsageVector: List<String>,

    @SerialName("golem.inf.cpu.architecture")
    val golemInfCpuArchitecture: String,

    @SerialName("golem.inf.cpu.cores")
    val golemInfCpuCores: Int,

    @SerialName("golem.inf.cpu.threads")
    val golemInfCpuThreads: Int,

    @SerialName("golem.inf.mem.gib")
    val golemInfMemGib: Double,

    @SerialName("golem.inf.storage.gib")
    val golemInfStorageGib: Double,

    @SerialName("golem.node.debug.subnet")
    val golemNodeDebugSubnet: String,

    @SerialName("golem.node.id.name")
    val golemNodeIdName: String,

    @SerialName("golem.node.net.is-public")
    val golemNodeNetIsPublic: Boolean,

    @SerialName("golem.runtime.capabilities")
    val golemRuntimeCapabilities: List<String>,

    @SerialName("golem.runtime.name")
    val golemRuntimeName: String,

    @SerialName("golem.runtime.version")
    val golemRuntimeVersion: String,

    @SerialName("golem.srv.caps.multi-activity")
    val golemSrvCapsMultiActivity: Boolean,

    @SerialName("golem.srv.caps.payload-manifest")
    val golemSrvCapsPayloadManifest: Boolean
) {
    fun getPricing(): PricingData {
        val expectedParams = arrayOf(
            "golem.usage.duration_sec",
            "golem.usage.network.in-mib",
            "golem.usage.network.out-mib"
        )
        if (golemComUsageVector.size != expectedParams.size) {
            throw Exception("Invalid usage vector")
        }

        expectedParams.forEach { expectedParam ->
            if (!golemComUsageVector.toList().contains(expectedParam)) {
                throw Exception("Expected param $expectedParam not found in usage vector")
            }
        }
        val pricing =
            (golemComUsageVector.toList() + "start").zip(golemComPricingModelLinearCoeffs.toList())
                .toMap()

        return PricingData(
            pricePerSec = pricing["golem.usage.duration_sec"] ?: 0.0,
            priceOutMb = pricing["golem.usage.network.out-mib"] ?: 0.0,
            priceInMb = pricing["golem.usage.network.in-mib"] ?: 0.0
        )
    }
}

data class UsageVectorData(val bytesSent: Double, val bytesReceived: Double, val duration: Double)

data class PricingData(val pricePerSec: Double, val priceOutMb: Double, val priceInMb: Double)
