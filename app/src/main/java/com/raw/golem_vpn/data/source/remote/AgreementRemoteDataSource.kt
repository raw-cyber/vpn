package com.raw.golem_vpn.data.source.remote

import com.raw.golem_vpn.data.*
import com.raw.golem_vpn.data.source.AgreementDataSource
import com.raw.golem_vpn.utils.RawLog
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withTimeout
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.time.Duration
import java.time.Instant
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import javax.inject.Inject

class AgreementRemoteDataSource @Inject constructor(
    private val client: HttpClient,
) : AgreementDataSource {
    private val tag: String = this::class.java.simpleName

    private val observableAgreement = MutableStateFlow<Agreement?>(null)
    private val observableActivity = MutableStateFlow<NetworkActivity?>(null)

    override fun getAgreementStream(): Flow<Agreement?> {
        return observableAgreement
    }

    private fun updateAgreement(agreement: Agreement) {
        observableAgreement.value = agreement
    }

    override fun getActivityStream(): Flow<NetworkActivity?> {
        return observableActivity
    }

    private fun updateActivity(activity: NetworkActivity) {
        observableActivity.value = activity
    }

    override suspend fun makeAgreement(agreementRequest: AgreementRequest): Boolean {
        try {
            val agreementResponse = client.post("/market-api/v1/agreements") {
                setBody(agreementRequest)
            }
            if (!agreementResponse.status.isSuccess()) {
                RawLog.d(tag, "Agreement creation failed")
                return false
            }
            val agreementId = agreementResponse.body<String>().replace("\"", "")
            val agreementConfirmationResponse =
                client.post("/market-api/v1/agreements/$agreementId/confirm")
            if (!agreementConfirmationResponse.status.isSuccess()) {
                RawLog.d(tag, "Agreement confirmation failed")
                return false
            }

            val agreement =
                client.get("/market-api/v1/agreements/$agreementId").body<Agreement>()
            updateAgreement(agreement)
            delay(200)
            val activityStatus = createActivity(agreementId)
            RawLog.d(tag, "Agreement confirmation success")
            return activityStatus
        } catch (e: Exception) {
            RawLog.d(tag, "makeAgreement Exception: ${e.message}")
            e.printStackTrace()
        }
        return false
    }

    override suspend fun terminateAgreement() {
        observableAgreement.value?.let {
            try {
                val res = client.post("market-api/v1/agreements/${it.agreementId}/terminate") {
                    setBody(
                        TerminateAgreementRequest(
                            message = "Terminating agreement",
                        )
                    )
                }
                if (res.status.isSuccess()) {
                    RawLog.d(tag, "Agreement termination success")
                } else {
                    RawLog.d(tag, "Agreement termination failed")
                }
                RawLog.d(
                    tag,
                    "Agreement termination status ${res.status} Response: ${res.body<String>()}"
                )
            } catch (e: Exception) {
                RawLog.d(tag, "Exception: ${e.message}")
            }
        }
    }

    override suspend fun acceptPayments(allocationId: String) {
        RawLog.e(tag, "Start acceptPayments")
        val agreement = getAgreementStream().first()
        RawLog.d(tag, "Agreement: $agreement")
        if (agreement == null) {
            RawLog.d(tag, "Agreement is null")
            return
        }
        val activity = getActivityStream().first()
        RawLog.d(tag, "Activity: $activity")
        if (activity == null) {
            RawLog.d(tag, "Activity is null")
            return
        }
        val pricing = agreement.offer.properties.getPricing()
        try {
            while (true) {
                delay(1000)
                val ts = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.UTC)
                    .format(DateTimeFormatter.ISO_INSTANT).replace("+00:00", "Z")
                val events: List<BasicDebitNote> = try {
                    client.get(
                        "/payment-api/v1/debitNoteEvents?afterTimestamp=$ts&timseout=15"
                    ).body()
                } catch (e: Exception) {
                    RawLog.e(tag, "Failed to fetch debit note events: ${e.message}")
                    emptyList()
                }
                val debitNoteEvents = events.filter {
                    it.eventType == "DebitNoteReceivedEvent"
                }
                debitNoteEvents.forEach { event ->
                    val debitNote: DebitNote = try {
                        client.get(
                            "/payment-api/v1/debitNotes/${event.debitNoteId}"
                        ).body()
                    } catch (e: Exception) {
                        RawLog.e(tag, "Failed to fetch debit note: ${e.message}")
                        return@forEach
                    }
                    if (debitNote.agreementId != agreement.agreementId) {
                        RawLog.d(tag, "Debit note agreementId does not match")
                        RawLog.d(
                            tag,
                            "Debit note agreementId: ${debitNote.agreementId} and agreementId: ${agreement.agreementId}"
                        )
                        return@forEach
                    }
                    if (debitNote.activityId != activity.activityId) {
                        RawLog.d(tag, "Debit note activityId does not match")
                        RawLog.d(
                            tag,
                            "Debit note activityId: ${debitNote.activityId} and activityId: ${activity.activityId}"
                        )
                        return@forEach
                    }
                    if (debitNote.usageCounterVector.size != agreement.offer.properties.golemComUsageVector.size) {
                        RawLog.d(tag, "Debit note usageCounterVector size does not match")
                        RawLog.d(
                            tag,
                            "Debit note usageCounterVector size: ${debitNote.usageCounterVector.size} and usageVector size: ${agreement.offer.properties.golemComUsageVector.size}"
                        )
                        return@forEach
                    }
                    RawLog.d(tag, "Debit note: $debitNote")
                    val usage =
                        agreement.offer.properties.golemComUsageVector.zip(debitNote.usageCounterVector)
                            .toMap()
                    val usageData = UsageVectorData(
                        bytesReceived = usage["golem.usage.network.in-mib"] ?: 0.0,
                        bytesSent = usage["golem.usage.network.out-mib"] ?: 0.0,
                        duration = usage["golem.usage.duration_sec"] ?: 0.0,
                    )

                    println("Usage counter vector: $usage")
                    println(
                        "Usage data: $usageData. Total price ${pricing.pricePerSec * usageData.duration}" +
                                "Total bytes sent ${usageData.bytesSent} and received ${usageData.bytesReceived}" +
                                "Total price for bytes sent ${pricing.priceOutMb * usageData.bytesSent} and received ${pricing.priceInMb * usageData.bytesReceived}"
                    )
                    val totalPrice = pricing.pricePerSec * usageData.duration +
                            pricing.priceOutMb * usageData.bytesSent +
                            pricing.priceInMb * usageData.bytesReceived
                    println("Total price: $totalPrice")

                    try {
                        val res =
                            client.post("/payment-api/v1/debitNotes/${debitNote.debitNoteId}/accept") {
                                setBody(
                                    AcceptDebitNoteRequest(
                                        totalAmountAccepted = debitNote.totalAmountDue,
                                        allocationId = allocationId,
                                    )
                                )
                            }
                        if (res.status.isSuccess()) {
                            RawLog.d(tag, "Debit note accepted")
                        } else {
                            RawLog.d(
                                tag,
                                "Failed to accept debit note ${res.status}. Status: ${res.body<String>()}"
                            )
                        }
                    } catch (e: Exception) {
                        RawLog.e(tag, "Failed to accept debit note: ${e.message}")
                    }

                }
            }
        } catch (e: Exception) {
            RawLog.d(tag, "Exception: ${e.message}")
        }
    }

    override suspend fun payInvoice(allocationId: String) {
        val agreementId = observableAgreement.value?.agreementId
        val startTs = Instant.now().truncatedTo(ChronoUnit.SECONDS)
        val ts = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.UTC)
            .format(DateTimeFormatter.ISO_INSTANT).replace("+00:00", "Z")
        var timeoutLeft: Long
        withTimeout(
            15000
        ) {
            while (true) {
                RawLog.e(tag, "Start payInvoice")
                val events: List<InvoiceEvent> = try {
                    delay(500)
//                    TODO - API min 31
                    timeoutLeft = Duration.ofSeconds(15.toLong())
                        .minus(Duration.between(Instant.now(), startTs)).toSeconds()
                    RawLog.e(tag, "Timeout left: $timeoutLeft")
                    val res =
                        client.get("payment-api/v1/invoiceEvents?afterTimestamp=${ts}&timeout=${timeoutLeft}")
                    RawLog.e(tag, "Invoice events response: ${res.body<String>()}")
                    res.body<List<InvoiceEvent>>()
                } catch (e: CancellationException) {
                    return@withTimeout
                } catch (e: Exception) {
                    RawLog.e(tag, "Failed to fetch invoice events: ${e.message}")
                    emptyList()
                }
                RawLog.e(tag, "Invoice events (before filtering): $events")
                val invoiceEvents = events.filter {
                    it.eventType == "InvoiceReceivedEvent"
                }
                RawLog.e(tag, "Invoice events: $invoiceEvents")
                invoiceEvents.forEach {
                    val invoice: Invoice = try {
                        client.get(
                            "/payment-api/v1/invoices/${it.invoiceId}"
                        ).body()
                    } catch (e: Exception) {
                        RawLog.e(tag, "Failed to fetch invoice: ${e.message}")
                        return@forEach
                    }
                    if (invoice.agreementId != agreementId) {
                        RawLog.d(tag, "Invoice agreementId does not match")
                        RawLog.d(
                            tag,
                            "Invoice agreementId: ${invoice.agreementId} and agreementId: $agreementId"
                        )
                        return@forEach
                    }
                    RawLog.d(tag, "Invoice: $invoice")
                    try {
                        val res =
                            client.post("/payment-api/v1/invoices/${invoice.invoiceId}/accept") {
                                setBody(
                                    AcceptInvoiceRequest(
                                        totalAmountAccepted = invoice.amount,
                                        allocationId = allocationId,
                                    )
                                )
                            }
                        if (res.status.isSuccess()) {
                            RawLog.d(tag, "Invoice accepted")
                            return@withTimeout
                        } else {
                            RawLog.d(
                                tag,
                                "Failed to accept invoice ${res.status}. Status: ${res.body<String>()}"
                            )
                        }
                    } catch (e: Exception) {
                        RawLog.e(tag, "Failed to accept invoice: ${e.message}")
                    }
                }
            }
        }
    }

    override suspend fun createAllocation(allocation: Allocation): String? {
        val allocationId = try {
            val allocationResponse = client.post("payment-api/v1/allocations") {
                setBody(allocation)
            }
            allocationResponse.body<AllocationResponse>().allocationId
        } catch (e: Exception) {
            RawLog.d(tag, "createAllocation Exception: ${e.message}")
            null
        }
        RawLog.d(tag, "createAllocation Allocation id is: $allocationId")
        return allocationId
    }

    override suspend fun releaseAllocation(allocationId: String) {
        RawLog.d(tag, "releaseAllocation Allocation id is: $allocationId")
        try {
            val releaseAllocationResponse =
                client.delete("payment-api/v1/allocations/$allocationId")
            if (releaseAllocationResponse.status.isSuccess()) {
                RawLog.d(tag, "Allocation released")
            } else {
                RawLog.d(tag, "Allocation release failed")
            }
        } catch (e: Exception) {
            RawLog.d(tag, "releaseAllocation Exception: ${e.message}")
        }
    }

    private suspend fun createActivity(agreementId: String): Boolean {
        val activity = ActivityRequest(agreementId, null)
        try {
            val res = client.post("/activity-api/v1/activity") {
                setBody(activity)
            }
            if (!res.status.isSuccess()) {
                RawLog.d(
                    tag,
                    "Failed to create activity ${res.status}. Status: ${res.body<String>()}"
                )
                return false
            }
            updateActivity(res.body())
            return true
        } catch (e: Exception) {
            RawLog.d(tag, "createActivity Exception: ${e.message}")
        }
        return false
    }

    private suspend fun createActivityOld(agreementId: String) {
        val agreement = ActivityRequest(agreementId, null)
        val activityJson = Json.encodeToJsonElement(agreement)
        try {
            val res = client.post("/activity-api/v1/activity") {
                setBody(agreement)
            }
            if (res.status.isSuccess()) {
//                TODO - Add statuses to the activity
                RawLog.d(tag, "createActivity: Activity created")
                val activity = res.body<NetworkActivity>()
                RawLog.d(tag, "createActivity response: $activity")
//                vpnServiceManager.updatePreference(
//                    AgreementPreferenceKeys.ACTIVITY_ID, activity.activityId
//                )
                updateActivity(activity)
//                vpnServiceManager.acceptPayments()
            } else {
                RawLog.d(tag, "Activity creation failed")
            }
        } catch (e: Exception) {
            RawLog.d(tag, "Exception: ${e.message}")
        }
    }


}
