package com.raw.golem_vpn.data

data class NetworkTraffic(
    val incoming: Long,
    val outgoing: Long,
    val incomingTotal: Long,
    val outgoingTotal: Long,
)