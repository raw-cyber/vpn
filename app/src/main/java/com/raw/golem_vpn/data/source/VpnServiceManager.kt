package com.raw.golem_vpn.data.source

import androidx.activity.result.contract.ActivityResultContract
import androidx.datastore.preferences.core.Preferences
import com.raw.golem_vpn.data.AgreementData
import com.raw.golem_vpn.data.VpnPreferences
import com.raw.golem_vpn.data.YagnaData
import kotlinx.coroutines.flow.Flow

interface VpnServiceManager {
    val vpnPreferencesFlow: Flow<VpnPreferences>
    val yagnaFlow: Flow<YagnaData>
    val agreementFlow: Flow<AgreementData>

    suspend fun updatePreference(key: Preferences.Key<String>, value: String)
    suspend fun updatePreference(key: Preferences.Key<Boolean>, value: Boolean)
    suspend fun updatePreference(key: Preferences.Key<Int>, value: Int)

    fun getVpnPermissionAction(): ActivityResultContract<String, Boolean>
    fun checkVpnPermissions(): Boolean
    fun startVpnService()
    fun stopVpnService()
    fun connectToVpn()
    fun disconnectFromVpn()
    fun acceptPayments()
    fun checkYagnaStatus()
}
