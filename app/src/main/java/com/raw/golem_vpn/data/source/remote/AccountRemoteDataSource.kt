package com.raw.golem_vpn.data.source.remote

import android.content.Context
import com.raw.golem_vpn.core.vpn.CommandManagerConstructor
import com.raw.golem_vpn.data.Account
import com.raw.golem_vpn.data.BalanceResponse
import com.raw.golem_vpn.data.source.AccountDataSource
import com.raw.golem_vpn.utils.RawLog
import dagger.hilt.android.qualifiers.ApplicationContext
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import java.io.BufferedReader
import java.io.InputStreamReader
import javax.inject.Inject

class AccountRemoteDataSource @Inject constructor(
    private val client: HttpClient,
    @ApplicationContext private val applicationContext: Context
) : AccountDataSource {
    private val tag = this::class.java.name
    private val observableAccountInfo = MutableStateFlow<Account?>(null)
    private val observableBalanceInfo = MutableStateFlow<BalanceResponse?>(null)

    override fun getAccountInfoStream(): Flow<Account?> {
        return observableAccountInfo
    }

    override fun getBalanceStream(): Flow<BalanceResponse?> {
        return observableBalanceInfo
    }

    override suspend fun getAccountInfo(): Account? {
        return observableAccountInfo.value
    }

    override suspend fun getBalance(): BalanceResponse? {
        return observableBalanceInfo.value
    }

    override suspend fun refreshAccountInfo() {
        try {
            observableAccountInfo.value = client.get("me").body<Account>()
        } catch (e: Exception) {
            RawLog.d(tag, "refreshAccountInfo: ${e.message}")
        }
    }

    override suspend fun refreshBalance() {
        try {
            val mCommandManagerConstructor =
                CommandManagerConstructor(applicationContext)
            val command = mCommandManagerConstructor.getAccountInfo()
            val process = Runtime.getRuntime()
                .exec(command, mCommandManagerConstructor.getEnvsConfig())
            val inputStream = process.inputStream
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { line ->
                stringBuilder.append(line)
            }
            val jsonPayload = stringBuilder.toString()
            val balance = Json.decodeFromString(BalanceResponse.serializer(), jsonPayload)
            RawLog.d(tag, "refreshBalance: $balance")
            observableBalanceInfo.value = balance
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override suspend fun yagnaFirstRun(): Boolean {
        val initPaymentReturn = initPayment()
        if (initPaymentReturn) {
            val fundAccountReturn = fundAccount()
            if (fundAccountReturn) {
                return true
            }
        }
        return false
    }

    override suspend fun initPayment(): Boolean {
        val mCommandManagerConstructor = CommandManagerConstructor(applicationContext)
        val command = mCommandManagerConstructor.initSender()
        RawLog.d(tag, "initPayment Yagna command: $command")
        withContext(Dispatchers.IO) {
            val process =
                Runtime.getRuntime().exec(command, mCommandManagerConstructor.getEnvsConfig())
            val inputStream = process.inputStream
            val reader = BufferedReader(InputStreamReader(inputStream))
            var line: String?

            while (reader.readLine().also { line = it } != null) {
                RawLog.d(tag, "Yagna output: $line")
            }

            val errorStream = process.errorStream
            val errorReader = BufferedReader(InputStreamReader(errorStream))
            var errorLine: String?

            while (errorReader.readLine().also { errorLine = it } != null) {
                RawLog.e(tag, "yagna error: $errorLine")
            }

            val exitCode = process.waitFor()
            RawLog.d(tag, "initPayment yagna exit code: $exitCode")
            return@withContext exitCode == 0
        }
        return false
    }


    override suspend fun fundAccount(): Boolean {
        withContext(Dispatchers.IO) {
            val mCommandManagerConstructor = CommandManagerConstructor(applicationContext)
            val command = mCommandManagerConstructor.fundAccount()
            RawLog.d(tag, "fundAccount Yagna command: $command")
            val process =
                Runtime.getRuntime().exec(command, mCommandManagerConstructor.getEnvsConfig())
            val exitCode = process.waitFor()
            RawLog.d(tag, "fundAccount yagna exit code: $exitCode")
            return@withContext exitCode == 0
        }
        return false
    }
}