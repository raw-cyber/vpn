package com.raw.golem_vpn.data

import com.raw.golem_vpn.utils.AgreementStatus
import com.raw.golem_vpn.utils.NetworkConfigurationStatus
import com.raw.golem_vpn.utils.ProposalStatus
import com.raw.golem_vpn.utils.VpnStatus

data class VpnPreferences(
    val address: String = "",
    val port: Int = 80,
    val isConnected: Boolean = false,
    val network: Network? = null,
    val localIpAddress: String? = null,
    val remoteIpAddress: String? = null
)

data class YagnaData(
    val isRunning: Boolean = false,
    val isFirstRun: Boolean = false,
    val identity: String = "",
)

data class AgreementData(
    val activityId: String = "",
    val agreementId: String = "",
    val providerId: String = "",
    val pricePerSec: Double = 0.0,
    val priceOutMb: Double = 0.0,
    val priceInMb: Double = 0.0,
    val bytesSent: Double = 0.0,
    val bytesReceived: Double = 0.0,
    val duration: Double = 0.0,
)

open class StatusData<T>(
    open val status: T,
    open val message: String? = null,
)

data class VpnStatusData(
    override val status: VpnStatus = VpnStatus.DISCONNECTED,
    override val message: String? = null
) : StatusData<VpnStatus>(status, message)

class ProposalStatusData(
    override val status: ProposalStatus = ProposalStatus.PENDING,
    override val message: String? = null
) : StatusData<ProposalStatus>(status, message)

data class AgreementStatusData(
    override val status: AgreementStatus = AgreementStatus.PENDING,
    override val message: String? = null
) : StatusData<AgreementStatus>(status, message)

data class NetworkStatusData(
    override val status: NetworkConfigurationStatus = NetworkConfigurationStatus.NOT_CONFIGURED,
    override val message: String? = null
) : StatusData<NetworkConfigurationStatus>(status, message)