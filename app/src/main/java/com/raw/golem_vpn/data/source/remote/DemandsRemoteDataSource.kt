package com.raw.golem_vpn.data.source.remote

import com.raw.golem_vpn.data.DemandRequest
import com.raw.golem_vpn.data.source.DemandDataSource
import com.raw.golem_vpn.utils.RawLog
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.net.ConnectException
import javax.inject.Inject

class DemandsRemoteDataSource @Inject constructor(
    private val client: HttpClient
) : DemandDataSource {

    private var demandData: DemandRequest? = null
    private var demandId: String? = null
    override suspend fun createDemand(demandRequest: DemandRequest): String? {
        try {
            demandData = demandRequest
            val demandConfigStr = Json.encodeToJsonElement(demandRequest)
            val res = client.post("/market-api/v1/demands") {
                setBody(demandConfigStr)
            }
            demandId = res.body<String>()
            demandId = demandId!!.replace("\"", "")
        } catch (e: ConnectException) {
            RawLog.e(tag, "Connection error: ${e.message}")
        }
        return demandId
    }

    override suspend fun cancelDemand(demandId: String): Boolean {
        try {
            val res = client.delete("/market-api/v1/demands/$demandId")
            RawLog.d(tag, "Cancel demand status code: ${res.status.value}")
            return res.status.value == 204
        } catch (e: ConnectException) {
            RawLog.e(tag, "Connection error: ${e.message}")
        }
        return false
    }

    override suspend fun createCounterProposal(proposalId: String) {
        demandData.let {
            val demandConfigStr = Json.encodeToJsonElement(it)
            val res = client.post("market-api/v1/demands/${demandId}/proposals/$proposalId") {
                setBody(demandConfigStr)
            }
            RawLog.d(tag, "Counter proposal status code: ${res.status.value}")
        }
    }

    private val tag: String = this::class.java.simpleName
}
