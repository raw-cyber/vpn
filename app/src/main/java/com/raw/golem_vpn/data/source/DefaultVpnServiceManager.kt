package com.raw.golem_vpn.data.source

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.VpnService
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import androidx.activity.result.contract.ActivityResultContract
import androidx.core.content.ContextCompat
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.raw.golem_vpn.core.vpn.RawVpnService
import com.raw.golem_vpn.core.vpn.VpnActivityResultContracts
import com.raw.golem_vpn.data.AgreementData
import com.raw.golem_vpn.data.PricingData
import com.raw.golem_vpn.data.UsageVectorData
import com.raw.golem_vpn.data.VpnPreferences
import com.raw.golem_vpn.data.YagnaData
import com.raw.golem_vpn.di.IoDispatcher
import com.raw.golem_vpn.utils.RawLog
import dagger.hilt.android.qualifiers.ApplicationContext
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.http.isSuccess
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch
import java.io.IOException
import java.net.ConnectException
import javax.inject.Inject

const val MSG_RUN_VPN = 1
const val MSG_STOP_VPN = 2
const val MSG_ACCEPT_PAYMENTS = 3

const val VPN_WS_ADDRESS = "ws://127.0.0.1"
const val VPN_WS_PORT = 7465

const val PREF_KEY = "preferences"
const val VPN_WS_ADDRESS_KEY = "vpn_ws_address"
const val VPN_WS_PORT_KEY = "vpn_ws_port"
const val VPN_STATUS_KEY = "vpn_status"

const val YAGNA_STATUS_KEY = "yagna_status"
const val YAGNA_FIRST_RUN = "yagna_first_run"
const val YAGNA_WALLET_ID = "yagna_wallet_id"

const val NETWORK_ACTIVITY_ID = "activity_id"
const val NETWORK_AGREEMENT_ID = "agreement_id"
const val NETWORK_PROVIDER_ID = "provider_id"

object VpnPreferenceKeys {
    val ADDRESS = stringPreferencesKey(VPN_WS_ADDRESS_KEY)
    val PORT = intPreferencesKey(VPN_WS_PORT_KEY)
    val CONNECTED = booleanPreferencesKey(VPN_STATUS_KEY)
}

object YagnaPreferenceKeys {
    val IDENTITY = stringPreferencesKey(YAGNA_WALLET_ID)
    val STATUS = booleanPreferencesKey(YAGNA_STATUS_KEY)
    val FIRST_RUN = booleanPreferencesKey(YAGNA_STATUS_KEY)
}

object AgreementPreferenceKeys {
    val ACTIVITY_ID = stringPreferencesKey(NETWORK_ACTIVITY_ID)
    val AGREEMENT_ID = stringPreferencesKey(NETWORK_AGREEMENT_ID)
    val PROVIDER_ID = stringPreferencesKey(NETWORK_PROVIDER_ID)
}

class DefaultVpnServiceManager @Inject constructor(
    private val client: HttpClient,
    private val coroutineScope: CoroutineScope,
    private val vpnStatusManager: VpnStatusManager,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    @ApplicationContext private val applicationContext: Context,
) : VpnServiceManager {

    private var mService: Messenger? = null

    /** Flag indicating whether we have called bind on the service.  */
    private var bound: Boolean = false

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = PREF_KEY)

    private val mConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            mService = Messenger(service)
            bound = true
        }

        override fun onServiceDisconnected(className: ComponentName) {
            mService = null
            bound = false
        }
    }

    init {
        onStart()
    }

    private fun onStart() {
        RawLog.d("onStart", "Running on start")
        if (!bound) {
            val intent = Intent(applicationContext, RawVpnService::class.java)
            applicationContext.bindService(intent, mConnection, 0)
        }
        checkYagnaStatus()
    }

    override val vpnPreferencesFlow: Flow<VpnPreferences> =
        applicationContext.dataStore.data.catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map { preferences ->
            VpnPreferences(
                address = preferences[VpnPreferenceKeys.ADDRESS] ?: VPN_WS_ADDRESS,
                port = preferences[VpnPreferenceKeys.PORT] ?: VPN_WS_PORT,
                isConnected = preferences[VpnPreferenceKeys.CONNECTED] ?: false,
                localIpAddress = LOCAL_IP,
                remoteIpAddress = REMOTE_IP
            )
        }
    override val yagnaFlow: Flow<YagnaData> = applicationContext.dataStore.data.catch {

    }.map { preferences ->
        YagnaData(
            isRunning = preferences[YagnaPreferenceKeys.STATUS] ?: false,
            isFirstRun = preferences[YagnaPreferenceKeys.FIRST_RUN] ?: true,
            identity = preferences[YagnaPreferenceKeys.IDENTITY] ?: ""
        )
    }
    override val agreementFlow: Flow<AgreementData> = applicationContext.dataStore.data.catch {

    }.map { preferences ->
        val pricing = PricingData(
            pricePerSec = 0.0, priceOutMb = 0.0, priceInMb = 0.0
        )
        val usageVector = UsageVectorData(
            bytesSent = 0.0, bytesReceived = 0.0, duration = 0.0
        )
        AgreementData(
            activityId = preferences[AgreementPreferenceKeys.ACTIVITY_ID] ?: "",
            agreementId = preferences[AgreementPreferenceKeys.AGREEMENT_ID] ?: "",
            providerId = preferences[AgreementPreferenceKeys.PROVIDER_ID] ?: "",
            pricePerSec = pricing.pricePerSec,
            priceOutMb = pricing.priceOutMb,
            priceInMb = pricing.priceInMb,
            bytesSent = usageVector.bytesSent,
            bytesReceived = usageVector.bytesReceived,
            duration = usageVector.duration
        )
    }

    override fun checkVpnPermissions(): Boolean {
        return VpnService.prepare(applicationContext) == null
    }

    override fun startVpnService() {
        val serviceIntent = Intent(applicationContext, RawVpnService::class.java).setAction(
            RawVpnService.ACTION_CONNECT
        )
        ContextCompat.startForegroundService(applicationContext, serviceIntent)
    }

    private fun sendMessageToVpnService(msg: Message) {
        onStart()
        if (!bound) return
        // Create and send a message to the service, using a supported 'what' value
        try {
            mService?.send(msg)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    override fun getVpnPermissionAction(): ActivityResultContract<String, Boolean> {
        return VpnActivityResultContracts()
    }

    override fun stopVpnService() {
        val serviceIntent = Intent(applicationContext, RawVpnService::class.java).setAction(
            RawVpnService.ACTION_DISCONNECT
        )
        applicationContext.startService(serviceIntent)
    }

    override fun connectToVpn() {
        coroutineScope.launch {
            vpnPreferencesFlow.take(1).collect { prefs ->
                val msg: Message = Message.obtain(null, MSG_RUN_VPN, 0, 0)
                msg.data.putString("address", prefs.address)
                sendMessageToVpnService(msg)
            }
        }
    }

    override fun disconnectFromVpn() {
        coroutineScope.launch {
            val msg: Message = Message.obtain(null, MSG_STOP_VPN, 0, 0)
            sendMessageToVpnService(msg)
        }
    }

    override fun acceptPayments() {
        val msg: Message = Message.obtain(null, MSG_ACCEPT_PAYMENTS, 0, 0)
        sendMessageToVpnService(msg)
    }

    override fun checkYagnaStatus() {
        coroutineScope.launch(ioDispatcher) {
            try {
                val response = client.get("me")
                if (response.status.isSuccess()) {
                    updatePreference(YagnaPreferenceKeys.STATUS, true)
                }
            } catch (e: ConnectException) {
                RawLog.d("onStart", "Update status to false")
                updatePreference(YagnaPreferenceKeys.STATUS, false)
            }
        }
    }

    override suspend fun updatePreference(
        key: Preferences.Key<String>, value: String
    ) {
        applicationContext.dataStore.edit { preferences ->
            preferences[key] = value
        }
    }

    override suspend fun updatePreference(
        key: Preferences.Key<Boolean>, value: Boolean
    ) {
        applicationContext.dataStore.edit { preferences ->
            preferences[key] = value
        }
    }

    override suspend fun updatePreference(
        key: Preferences.Key<Int>, value: Int
    ) {
        applicationContext.dataStore.edit { preferences ->
            preferences[key] = value
        }
    }
}