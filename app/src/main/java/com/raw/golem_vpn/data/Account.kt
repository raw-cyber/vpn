package com.raw.golem_vpn.data

import com.raw.golem_vpn.utils.DoubleUtils
import kotlinx.serialization.Serializable

@Serializable
data class Account(val identity: String, val name: String)

@Serializable
data class BalanceResponse(
    val amount: String,
    val driver: String,
    val gas: GasInfo,
    val incoming: IncomingInfo,
    val network: String,
    val outgoing: OutgoingInfo,
    val reserved: String,
    val token: String
) {
    @Serializable
    data class GasInfo(
        val balance: String,
        val currency_long_name: String,
        val currency_short_name: String
    )

    @Serializable
    data class IncomingInfo(
        val accepted: AgreementInfo,
        val confirmed: AgreementInfo,
        val requested: AgreementInfo
    )

    @Serializable
    data class OutgoingInfo(
        val accepted: AgreementInfo,
        val confirmed: AgreementInfo,
        val requested: AgreementInfo
    )

    @Serializable
    data class AgreementInfo(
        val agreementsCount: Int,
        val totalAmount: String
    )

    fun getGlmBalance(): Double {
        return DoubleUtils.roundDouble(amount.toDouble(), 3)
    }

    fun getGasBalance(): Double {
        return DoubleUtils.roundDouble(gas.balance.toDouble(), 3)
    }
}

