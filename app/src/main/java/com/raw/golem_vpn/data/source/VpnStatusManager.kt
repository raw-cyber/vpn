package com.raw.golem_vpn.data.source

import com.raw.golem_vpn.data.AgreementStatusData
import com.raw.golem_vpn.data.NetworkStatusData
import com.raw.golem_vpn.data.ProposalStatusData
import com.raw.golem_vpn.data.VpnStatusData
import com.raw.golem_vpn.utils.AgreementStatus
import com.raw.golem_vpn.utils.NetworkConfigurationStatus
import com.raw.golem_vpn.utils.ProposalStatus
import com.raw.golem_vpn.utils.VpnStatus
import kotlinx.coroutines.flow.Flow

interface VpnStatusManager {

    fun getVpnStatusStream(): Flow<VpnStatusData>
    fun getProposalStatusStream(): Flow<ProposalStatusData>
    fun getAgreementStatusStream(): Flow<AgreementStatusData>
    fun getNetworkStatusStream(): Flow<NetworkStatusData>

    fun updateVpnStatus(vpnStatus: VpnStatus, message: String?)
    fun updateProposalStatus(proposalStatus: ProposalStatus, message: String?)
    fun updateAgreementStatus(agreementStatus: AgreementStatus, message: String?)
    fun updateNetworkStatus(networkStatus: NetworkConfigurationStatus, message: String?)
}