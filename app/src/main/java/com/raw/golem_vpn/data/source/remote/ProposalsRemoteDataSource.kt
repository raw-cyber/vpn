package com.raw.golem_vpn.data.source.remote

import com.raw.golem_vpn.data.ProposalEvent
import com.raw.golem_vpn.data.source.ProposalsDataSource
import com.raw.golem_vpn.utils.RawLog
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.serialization.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import java.net.ConnectException
import javax.inject.Inject

class ProposalsRemoteDataSource @Inject constructor(
    private val client: HttpClient
) : ProposalsDataSource {

    private var PROPOSAL_SERVICE_DATA = LinkedHashMap<String, List<ProposalEvent>>()
    private var observableProposals = MutableStateFlow<List<ProposalEvent>>(emptyList())

    override fun getProposalsStream(): Flow<List<ProposalEvent>> {
        return observableProposals
    }

    override suspend fun getProposals(
        demandId: String,
    ): List<ProposalEvent> {
        val maxEvents = 5
        val pollTimeout = 5 * 1000
        try {
            val response =
                client.get("/market-api/v1/demands/${demandId}/events?maxEvents=${maxEvents}&pollTimeout=${pollTimeout}")
            val newProposals: List<ProposalEvent> = response.body()
            val oldProposals = PROPOSAL_SERVICE_DATA[demandId] ?: emptyList()
            PROPOSAL_SERVICE_DATA[demandId] = oldProposals + newProposals
        } catch (e: JsonConvertException) {
            RawLog.d(tag, "JsonConvertException: ${e.message}")
        } catch (e: ConnectException) {
            RawLog.d(tag, "ConnectException: ${e.message}")
        }

        return PROPOSAL_SERVICE_DATA[demandId]?.toList() ?: emptyList()
    }

    override suspend fun refreshProposals(demandId: String) {
        observableProposals.value = getProposals(demandId)
    }

    override suspend fun getProposal(demandId: String, proposalId: String): ProposalEvent? {
        return PROPOSAL_SERVICE_DATA[demandId]?.first { it.proposal.proposalId == proposalId }
    }

    override fun getProposalStream(proposalId: String): Flow<ProposalEvent> {
        return observableProposals.map { proposals ->
            proposals.first { it.proposal.proposalId == proposalId }
        }
    }

    private val tag: String = this::class.java.simpleName
}