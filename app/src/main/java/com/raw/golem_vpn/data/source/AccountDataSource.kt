package com.raw.golem_vpn.data.source

import com.raw.golem_vpn.data.Account
import com.raw.golem_vpn.data.BalanceResponse
import kotlinx.coroutines.flow.Flow

interface AccountDataSource {
    fun getAccountInfoStream(): Flow<Account?>
    fun getBalanceStream(): Flow<BalanceResponse?>
    suspend fun getAccountInfo(): Account?
    suspend fun getBalance(): BalanceResponse?
    suspend fun refreshAccountInfo()
    suspend fun refreshBalance()
    suspend fun yagnaFirstRun(): Boolean
    suspend fun initPayment(): Boolean
    suspend fun fundAccount(): Boolean
}