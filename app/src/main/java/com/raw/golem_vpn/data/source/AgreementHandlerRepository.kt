package com.raw.golem_vpn.data.source

import com.raw.golem_vpn.data.AgreementRequest
import com.raw.golem_vpn.data.Allocation
import com.raw.golem_vpn.data.DemandRequest
import com.raw.golem_vpn.data.ProposalEvent
import kotlinx.coroutines.flow.Flow

interface AgreementHandlerRepository {
    val demandId: Flow<String>
    fun getProposalStream(proposalId: String): Flow<ProposalEvent?>
    fun getProposalsStream(): Flow<List<ProposalEvent>>
    suspend fun getProposal(demandId: String, proposalId: String): ProposalEvent?
    suspend fun getProposals(demandId: String): List<ProposalEvent>
    suspend fun refreshProposals(demandId: String)
    suspend fun makeCounterProposal(proposalId: String)
    suspend fun findProvider(demand: DemandRequest)
    suspend fun findProviderManual(demand: DemandRequest)
    suspend fun makeAgreement(allocation: Allocation, agreement: AgreementRequest): Boolean
    suspend fun terminateAgreement()
    suspend fun acceptPayments()
}