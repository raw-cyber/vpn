package com.raw.golem_vpn.data.source

import com.raw.golem_vpn.data.ProposalEvent
import kotlinx.coroutines.flow.Flow

interface ProposalsDataSource {
    fun getProposalsStream(): Flow<List<ProposalEvent>>
    fun getProposalStream(proposalId: String): Flow<ProposalEvent>
    suspend fun getProposals(demandId: String): List<ProposalEvent>
    suspend fun getProposal(demandId: String, proposalId: String): ProposalEvent?
    suspend fun refreshProposals(demandId: String)
}