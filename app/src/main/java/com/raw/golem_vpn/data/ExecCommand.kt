package com.raw.golem_vpn.data

import kotlinx.serialization.Serializable

fun createBaseExecCommand(id: String, localIp: String, remoteIp: String): ExecCommand {
    return ExecCommand(
        text = "[{\"deploy\": {\"net\": [{\"id\": \"$id\", \"ip\": \"$localIp/24\", \"mask\": \"255.255.255.0\", \"nodeIp\": \"$remoteIp\"}]}}, {\"start\": {}}]",
    )
}

@Serializable
data class ExecCommand(
    val text: String,
)

@Serializable
data class BatchCheck(val index: Int, val isBatchFinished: Boolean)
