package com.raw.golem_vpn.ui.screens

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.raw.golem_vpn.ui.components.AppButton
import com.raw.golem_vpn.ui.theme.Cyan200
import com.raw.golem_vpn.ui.theme.Purple200
import com.raw.golem_vpn.ui.theme.Purple700
import com.raw.golem_vpn.utils.OnBoardingPage
import com.raw.golem_vpn.viewmodels.OnBoardingViewModel
import kotlinx.coroutines.launch

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun OnBoardingScreen(
    onHomeClick: () -> Unit, viewModel: OnBoardingViewModel = hiltViewModel()
) {
    val pages = listOf(
        OnBoardingPage.First, OnBoardingPage.Second, OnBoardingPage.Third
    )
    val pagerState = rememberPagerState()
    val animationScope = rememberCoroutineScope()
    val pageCount = pages.size

    fun navigateToHome() {
        viewModel.saveOnBoardingState(completed = true)
        onHomeClick()
    }

    Column(modifier = Modifier
        .fillMaxSize()
        .drawBehind {
            val brush = Brush.radialGradient(
                colors = listOf(Purple200, Purple700),
                radius = 1200.0F,
            )
            drawRect(brush = brush)
        }) {
        HorizontalPager(
            pageCount = pageCount,
            modifier = Modifier
                .weight(1f)
                .fillMaxHeight(),
            state = pagerState,
        ) { position ->
            PagerInfoComponent(onBoardingPage = pages[position])
        }
        Row(
            Modifier.align(Alignment.CenterHorizontally), horizontalArrangement = Arrangement.Center
        ) {
            repeat(pageCount) { iteration ->
                val color =
                    if (pagerState.currentPage == iteration) Cyan200 else Color.White.copy(alpha = 0.12f)
                Box(
                    modifier = Modifier
                        .padding(vertical = 0.dp, horizontal = 8.dp)
                        .clip(CircleShape)
                        .background(color)
                        .size(6.dp)
                )
            }
        }
        AppButton(modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 12.dp)
            .align(Alignment.CenterHorizontally), onClick = {
            if (pagerState.currentPage == pageCount - 1) {
                navigateToHome()
            } else {
                animationScope.launch {
                    pagerState.animateScrollToPage(pagerState.currentPage + 1)
                }
            }
        }) {
            Text(
                pages[pagerState.currentPage].buttonText.uppercase(),
                color = Color.White,
                style = MaterialTheme.typography.button
            )
        }
    }
}

@Composable
fun PagerInfoComponent(onBoardingPage: OnBoardingPage) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        onBoardingPage.content()
    }
}