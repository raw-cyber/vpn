package com.raw.golem_vpn.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.*
import androidx.compose.material3.ListItem
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.raw.golem_vpn.R
import com.raw.golem_vpn.data.ProposalEvent
import com.raw.golem_vpn.data.ProposalEventGenerator
import com.raw.golem_vpn.ui.theme.VPNOverWSTheme
import com.raw.golem_vpn.utils.TextUtils
import com.raw.golem_vpn.viewmodels.ProposalDetailsViewModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProposalDetailScreen(
    onBackClick: () -> Unit, viewModel: ProposalDetailsViewModel = hiltViewModel(),
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val snackbarHostState = remember { SnackbarHostState() }
    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(title = {
                Text(
                    text = stringResource(id = R.string.proposal_details),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
            }, navigationIcon = {
                IconButton(onClick = { onBackClick() }) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Localized description"
                    )
                }
            }, actions = {
                IconButton(onClick = { /* doSomething() */ }) {
                    Icon(
                        imageVector = Icons.Filled.Settings,
                        contentDescription = "Localized description"
                    )
                }
            })
        },
    ) { padding ->
        ProposalDetailsContent(
            padding,
            loading = uiState.isLoading,
            proposalEvent = uiState.proposalEvent,
            sendCounterProposal = { viewModel.sendCounterProposal() },
            proposalDraftContent = {
                ProposalDraftsContent(
                    proposalDrafts = uiState.proposalDrafts,
                    onProposalDraftClick = { viewModel.onProposalDraftClicked(it) }
                )
            }
        )
        uiState.userMessage?.let { userMessage ->
            val snackbarText = stringResource(userMessage)
            LaunchedEffect(snackbarHostState, viewModel, snackbarText) {
                snackbarHostState.showSnackbar(snackbarText)
                viewModel.snackbarMessageShown()
            }
        }
    }

}

@Composable
fun ProposalDetailsRow(
    content: @Composable () -> Unit
) {
    Row(
        Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
    ) {
        content()
    }
}

@Composable
fun ProposalDetailsContent(
    paddingValues: PaddingValues,
    loading: Boolean = false,
    proposalEvent: ProposalEvent?,
    proposalDraftContent: @Composable () -> Unit,
    sendCounterProposal: () -> Unit,
) {
    if (loading)
        LinearProgressIndicator(
            modifier = Modifier
                .semantics(mergeDescendants = true) {}
                .padding(top = paddingValues.calculateTopPadding())
                .fillMaxWidth()
        )
    Column(
        Modifier
            .padding(
                top = paddingValues.calculateTopPadding() + 16.dp,
                start = 24.dp,
                end = 24.dp,
            )
            .fillMaxWidth(),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        if (proposalEvent == null) {
            Text(text = "Proposal not found")
        } else {
            ProposalDetailsRow {
                Text(text = "Proposal ID: ")
                Text(text = TextUtils.shortenText(proposalEvent.proposal.proposalId, 10))
            }
            ProposalDetailsRow {
                Text(text = "IssuerId ID: ")
                Text(text = TextUtils.shortenText(proposalEvent.proposal.issuerId, 10))
            }
            ProposalDetailsRow {
                Text(text = "State: ")
                Text(text = proposalEvent.proposal.state)
            }
            ProposalDetailsRow {
                Text(text = "Timestamp: ")
                Text(text = proposalEvent.proposal.timestamp)
            }
            ProposalDetailsRow {
                Text(text = "Constraints: ")
                Text(text = TextUtils.shortenText(proposalEvent.proposal.constraints, 10))
            }
            Spacer(modifier = Modifier.height(20.dp))
            Button(
                onClick = sendCounterProposal,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 20.dp),
                content = {
                    Text(text = "Send counter proposal")
                }
            )
            proposalDraftContent()
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProposalDraftsContent(
    proposalDrafts: List<ProposalEvent?>,
    onProposalDraftClick: (ProposalEvent) -> Unit
) {
    Column {
        for (proposalDraft in proposalDrafts) {
            ListItem(
                headlineText = { Text("Draft") },
                leadingContent = {
                    Icon(
                        Icons.Filled.Menu,
                        contentDescription = "Localized description",
                    )
                },
                trailingContent = {
                    OutlinedButton(onClick = { onProposalDraftClick(proposalDraft!!) }) {
                        Text(text = "Agree")
                    }
                }
            )
            Divider()
        }
    }

}

@Preview
@Composable
fun ProposalDetailsPreview() {
    VPNOverWSTheme {
        Surface {
            ProposalDetailsContent(
                PaddingValues(24.dp), true, ProposalEventGenerator.generateProposals(1)[0], {}
            ) {}
        }
    }
}