package com.raw.golem_vpn.ui.main

import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.raw.golem_vpn.ui.screens.*
import kotlinx.coroutines.CoroutineScope

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VpnNavGraph(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    drawerState: DrawerState = rememberDrawerState(initialValue = DrawerValue.Closed),
    startDestination: String = VpnDestinations.ON_BOARDING_ROUTE,
    navActions: VpnNavigationActions = remember(navController) {
        VpnNavigationActions(navController)
    }
) {
    val currentNavBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = currentNavBackStackEntry?.destination?.route ?: startDestination

    NavHost(
        navController = navController,
        startDestination = startDestination,
        modifier = modifier
    ) {
        composable(VpnDestinations.ON_BOARDING_ROUTE) {
            OnBoardingScreen(
                onHomeClick = {
                    navActions.navigateToConfiguration()
                },
            )
        }
        composable(VpnDestinations.VPN_CONFIGURATION_ROUTE) {
            VpnConfigurationScreen(
                onDebugVpnClick = {
                    navActions.navigateToDebugVpn()
                }
            )
        }
        composable(VpnDestinations.DEBUG_VPN_CONFIGURATION_ROUTE) {
            DebugVpnConfigurationScreen(
                onWsConfigurationClick = {
                    navActions.navigateToWsConfiguration()
                }
            )
        }
        composable(VpnDestinations.WS_CONFIGURATION_ROUTE) {
            WebSocketConfigurationScreen()
        }
        composable(VpnDestinations.NETWORK_CONFIGURATION_ROUTE) {
            AddEditNetworkScreen()
        }
        composable(VpnDestinations.CREATE_DEMAND_ROUTE) {
            CreateEditDemandScreen(
                onBackClick = {
                    navController.popBackStack()
                }
            )
        }
        composable(VpnDestinations.PROPOSAL_DETAIL_ROUTE,
            arguments = listOf(
                navArgument(VpnDestinationsArgs.PROPOSAL_ID_ARG) {
                    type = NavType.StringType
                }
            )) {
            ProposalDetailScreen(
                onBackClick = {
                    navController.popBackStack()
                })
        }
    }
}