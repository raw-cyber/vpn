package com.raw.golem_vpn.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ContentAlpha
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.raw.golem_vpn.ui.theme.VPNOverWSTheme
import com.raw.golem_vpn.utils.LoadingContent
import com.raw.golem_vpn.viewmodels.VpnViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun WebSocketConfigurationScreen(
    modifier: Modifier = Modifier,
    viewModel: VpnViewModel = hiltViewModel(),
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()

    if (uiState.isLoading) {
        LoadingContent(loading = true,
            empty = true,
            emptyContent = { Text("Empty") },
            modifier = modifier,
            onRefresh = { },
            content = {
                Text("Loading")
            })
    } else {
        Scaffold(
            modifier = Modifier.fillMaxSize(),
        ) { paddingValues ->
            WebSocketConfigurationContent(
                padding = paddingValues,
                address = uiState.address,
                updateAddress = viewModel::updateAddress,
                saveConfiguration = viewModel::saveConfiguration,
                modifier = modifier
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun WebSocketConfigurationContent(
    padding: PaddingValues,
    address: String,
    updateAddress: (String) -> Unit,
    saveConfiguration: () -> Unit,
    modifier: Modifier
) {
    val textFieldColors = TextFieldDefaults.outlinedTextFieldColors(
        focusedBorderColor = Color.Cyan,
        unfocusedBorderColor = Color.Transparent,
        cursorColor = MaterialTheme.colorScheme.secondary.copy(alpha = ContentAlpha.high)
    )
    val buttonsModifier = Modifier.fillMaxWidth()
    Column(
        modifier
            .padding(
                top = padding.calculateTopPadding(), start = padding.calculateStartPadding(
                    LayoutDirection.Ltr
                ), end = padding.calculateEndPadding(LayoutDirection.Ltr)
            )
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text("WebSocket Configuration", fontSize = 30.sp, fontWeight = FontWeight.Bold)
        Spacer(modifier = Modifier.height(20.dp))
        Text(
            "WS address",
            modifier = Modifier.fillMaxWidth(),
        )
        OutlinedTextField(
            value = address,
            modifier = Modifier.fillMaxWidth(),
            onValueChange = updateAddress,
            textStyle = MaterialTheme.typography.labelMedium,
            maxLines = 1,
            colors = textFieldColors
        )
        Spacer(modifier = Modifier.height(20.dp))
        Button(modifier = buttonsModifier, onClick = saveConfiguration) {
            Text("Save configuration")
        }
    }

}


@Preview
@Composable
fun ConfigurationPreview() {
    VPNOverWSTheme {
        Surface {
            WebSocketConfigurationContent(
                padding = PaddingValues(16.dp),
                address = "ws://127.0.0.1:7465/net-api/v2/vpn/net/af0e999158364bb48bf6e6c0111b42f1/raw/192.168.8.7/50671",
                updateAddress = {},
                saveConfiguration = {},
                modifier = Modifier
            )
        }
    }
}