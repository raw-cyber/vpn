package com.raw.golem_vpn.ui.screens

import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.raw.golem_vpn.R
import com.raw.golem_vpn.ui.components.AppButton
import com.raw.golem_vpn.ui.theme.VPNOverWSTheme
import com.raw.golem_vpn.viewmodels.VpnViewModel

@Composable
fun DebugVpnConfigurationScreen(
    onWsConfigurationClick: () -> Unit,
    modifier: Modifier = Modifier,
    viewModel: VpnViewModel = hiltViewModel(),
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val vpnAllow =
        rememberLauncherForActivityResult(contract = viewModel.getVpnConfiguration(), onResult = {
            viewModel.connect()
        })
    Surface(
        modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background
    ) {
        DebugVpnConfigurationContent(
            toggleConnectionButton = { viewModel.connect() },
            toggleDisconnectionButton = { viewModel.disconnect() },
            onYagnaStart = { viewModel.runYagna() },
            onConfigurationClick = onWsConfigurationClick,
            onPaymentInitClick = { viewModel.initPayment() },
            onFundAccountClick = { viewModel.fundAccount() },
            askPermission = { vpnAllow.launch("com.raw.golem_vpn") },
            modifier = modifier
        )
    }
}

@Composable
fun DebugVpnConfigurationContent(
//    connectionState: String,
//    connectionIcon: Painter,
    toggleConnectionButton: () -> Unit,
    toggleDisconnectionButton: () -> Unit,
    onYagnaStart: () -> Unit,
    onConfigurationClick: () -> Unit,
    onPaymentInitClick: () -> Unit,
    onFundAccountClick: () -> Unit,
    askPermission: () -> Unit,
    modifier: Modifier
) {
    Column(
        Modifier
            .padding(all = 20.dp)
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "Golem VPN", fontSize = 30.sp, fontWeight = FontWeight.Bold)
        Spacer(modifier = Modifier.height(20.dp))
//        Row(
//            horizontalArrangement = Arrangement.SpaceBetween,
//            verticalAlignment = Alignment.CenterVertically
//        ) {
//            Text("Status: ")
//            Text(connectionState)
//            Spacer(modifier = Modifier.width(8.dp))
//            Image(
//                painter = connectionIcon,
//                contentDescription = "VPN Connection Status",
//                modifier = Modifier.size(16.dp)
//            )
//        }
        AppButton(modifier = Modifier.fillMaxWidth(), onClick = onYagnaStart) {
            Text("Run yagna")
        }
        AppButton(modifier = Modifier.fillMaxWidth(), onClick = toggleConnectionButton) {
            Text("Connect to VPN")
        }
        AppButton(modifier = Modifier.fillMaxWidth(), onClick = toggleDisconnectionButton) {
            Text("Disconnect from VPN")
        }
        AppButton(modifier = Modifier.fillMaxWidth(), onClick = {
            onConfigurationClick()
        }) {
            Text("WS Configuration")
        }
        AppButton(modifier = Modifier.fillMaxWidth(), onClick = {
            onPaymentInitClick()
        }) {
            Text("Payment init")
        }
        AppButton(modifier = Modifier.fillMaxWidth(), onClick = {
            onFundAccountClick()
        }) {
            Text("Fund account")
        }
        AppButton(
            modifier = Modifier.fillMaxWidth(),
            onClick = {
                askPermission()
            },
        ) {
            Text("Get VPN permissions")
        }
    }
}

@Preview
@Composable
fun DebugVpnConnectedConfigurationPreview() {
    VPNOverWSTheme {
        Surface {
            DebugVpnConfigurationContent(
//                connectionState = "Connected",
//                connectionIcon = painterResource(id = R.drawable.ic_connected_24),
                toggleConnectionButton = { },
                toggleDisconnectionButton = { },
                onConfigurationClick = { },
                onPaymentInitClick = { },
                onFundAccountClick = { },
                onYagnaStart = { },
                askPermission = { },
                modifier = Modifier
            )
        }
    }
}