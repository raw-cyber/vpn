package com.raw.golem_vpn.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.raw.golem_vpn.R

// Set of Material typography styles to start with
val baseTypography = Typography()
//val Rajdhani = FontFamily(
//    Font(R.font.rajdhani_bold, FontWeight.Bold),
//)
val interFontFamily = FontFamily(
    Font(R.font.inter_thin, FontWeight.Thin),
    Font(R.font.inter_extralight, FontWeight.ExtraLight),
    Font(R.font.inter_light, FontWeight.Light),
    Font(R.font.inter, FontWeight.Normal),
    Font(R.font.inter_medium, FontWeight.Medium),
    Font(R.font.inter_semibold, FontWeight.SemiBold),
    Font(R.font.inter_bold, FontWeight.Bold),
)
val Typography = Typography(
    displayLarge = baseTypography.displayLarge.copy(
        color = TextLightPrimary,
        fontFamily = interFontFamily,
    ),
    headlineLarge = baseTypography.headlineLarge.copy(
        color = TextLightPrimary,
        fontWeight = FontWeight.SemiBold,
        fontFamily = interFontFamily,
    ),
    headlineMedium = baseTypography.headlineMedium.copy(
        color = TextLightPrimary,
        fontFamily = interFontFamily,
    ),
    headlineSmall = baseTypography.headlineSmall.copy(
        color = TextLightPrimary,
        fontFamily = interFontFamily,
        fontWeight = FontWeight.SemiBold,
    ),
    titleLarge = baseTypography.titleLarge.copy(
        color = TextLightPrimary,
        fontFamily = interFontFamily,
    ),
    titleMedium = baseTypography.titleMedium.copy(
        color = TextLightPrimary,
        fontFamily = interFontFamily,
        fontWeight = FontWeight.SemiBold,
    ),
    bodyLarge = baseTypography.bodyLarge.copy(
        color = TextLightPrimary,
        fontFamily = interFontFamily,
    ),
    bodyMedium = baseTypography.bodyMedium.copy(
        color = TextLightPrimary,
        fontFamily = interFontFamily,
    ),
    bodySmall = baseTypography.bodySmall.copy(
        color = TextLightPrimary,
        fontFamily = interFontFamily,
    ),
    labelLarge = baseTypography.labelLarge.copy(
        color = TextLightPrimary,
        fontFamily = interFontFamily,
    ),
    labelSmall = baseTypography.labelSmall.copy(
        color = TextLightPrimary,
        fontFamily = interFontFamily,
        fontWeight = FontWeight.Normal,
        fontSize = 10.sp,
        letterSpacing = 0.2.sp
    ),
)