package com.raw.golem_vpn.ui.screens

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Search
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.raw.golem_vpn.R
import com.raw.golem_vpn.viewmodels.CreateEditDemandViewModel
import com.raw.golem_vpn.viewmodels.DEFAULT_VPN
import com.raw.golem_vpn.viewmodels.Subnet
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateEditDemandScreen(
    onBackClick: () -> Unit,
    viewModel: CreateEditDemandViewModel = hiltViewModel()
) {

    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        floatingActionButton = {
            FloatingActionButton(onClick = {
                viewModel.createDemand()
                onBackClick()
            }) {
                Icon(Icons.Outlined.Search, stringResource(id = R.string.create_demand))
            }
        }
    ) { paddingValues ->
        CreateEditDemandContent(
            paddingValues,
            acceptTimeout = uiState.acceptTimeout,
            subnet = uiState.subnet,
            chosenPlatform = uiState.chosenPlatform,
            senderAddress = uiState.senderAddress,
            expirationDate = uiState.expirationDate,
            expirationTime = uiState.expirationTime,
            updateAcceptTimeout = viewModel::updateAcceptTimeout,
            updateSubnet = viewModel::updateSubnet,
            updateChosenPlatform = viewModel::updateChosenPlatform,
            updateExpirationDate = viewModel::updateExpirationDate,
            updateExpirationTime = viewModel::updateExpirationTime,
        )

    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateEditDemandContent(
    paddingValues: PaddingValues,
    acceptTimeout: Int,
    subnet: Subnet,
    chosenPlatform: String,
    senderAddress: String,
    expirationDate: String,
    expirationTime: String,
    updateAcceptTimeout: (Int) -> Unit,
    updateSubnet: (Subnet) -> Unit,
    updateChosenPlatform: (String) -> Unit,
    updateExpirationDate: (String) -> Unit,
    updateExpirationTime: (String) -> Unit,
) {
    Column(
        modifier = Modifier
            .padding(paddingValues)
            .fillMaxSize()
            .padding(16.dp)
    ) {
        OutlinedTextField(
            value = acceptTimeout.toString(),
            onValueChange = { updateAcceptTimeout(it.toInt()) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            label = { Text(text = stringResource(id = R.string.accept_timeout)) },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
        OutlinedTextField(
            value = subnet.toString(),
            onValueChange = { updateSubnet(DEFAULT_VPN) },
            readOnly = true,
            label = { Text(text = stringResource(id = R.string.subnet)) },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
        OutlinedTextField(
            value = chosenPlatform,
            onValueChange = { updateChosenPlatform(it) },
            label = { Text(text = stringResource(id = R.string.chosen_platform)) },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
        OutlinedTextField(
            readOnly = true,
            value = senderAddress,
            onValueChange = {},
            label = { Text(text = stringResource(id = R.string.sender_address)) },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
        DatePicker(
            value = expirationDate,
            onValueChange = { updateExpirationDate(it) },
            label = stringResource(id = R.string.expiration_date),
        )
        Spacer(modifier = Modifier.height(16.dp))
        TimePicker(
            value = expirationTime,
            onValueChange = { updateExpirationTime(it) },
            label = stringResource(id = R.string.expiration_time),
        )
        Spacer(modifier = Modifier.height(16.dp))
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TimePicker(
    label: String,
    value: String,
    onValueChange: (String) -> Unit,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    pattern: String = "HH:mm",
    is24HourView: Boolean = true,
) {
    val formatter = DateTimeFormatter.ofPattern(pattern)
    val time = if (value.isNotBlank()) LocalTime.parse(value, formatter) else LocalTime.now()
    val dialog = TimePickerDialog(
        LocalContext.current,
        { _, hour, minute -> onValueChange(LocalTime.of(hour, minute).toString()) },
        time.hour,
        time.minute,
        is24HourView,
    )

    TextField(
        value = value,
        enabled = false,
        onValueChange = { onValueChange(it) },
        label = { Text(label) },
        modifier = Modifier
            .clickable { dialog.show() }
            .fillMaxWidth(),
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DatePicker(
    label: String,
    value: String,
    onValueChange: (String) -> Unit = {},
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    pattern: String = "yyyy-MM-dd",
) {
    val formatter = DateTimeFormatter.ofPattern(pattern)
    val date = if (value.isNotBlank()) LocalDate.parse(value, formatter) else LocalDate.now()
    val dialog = DatePickerDialog(
        LocalContext.current,
        { _, year, month, dayOfMonth ->
            onValueChange(LocalDate.of(year, month + 1, dayOfMonth).toString())
        },
        date.year,
        date.monthValue - 1,
        date.dayOfMonth,
    )

    TextField(
        value = value,
        enabled = false,
        onValueChange = { onValueChange(it) },
        label = { Text(label) },
        modifier = Modifier
            .clickable { dialog.show() }
            .fillMaxWidth(),
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
    )
}

@Preview
@Composable
fun TimePicketPreview() {
    TimePicker(
        label = "Time",
        value = "12:00",
        onValueChange = {},
    )
}

@Preview
@Composable
fun DatePickerPreview() {
    DatePicker(
        label = "Date",
        value = "2021-01-01",
        onValueChange = {},
    )
}
