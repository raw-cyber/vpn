package com.raw.golem_vpn.ui.components

import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.res.stringResource
import com.raw.golem_vpn.viewmodels.BaseViewModel


@Composable
fun ShowSnackbarIfNotNull(
    snackbarHostState: SnackbarHostState,
    viewModel: BaseViewModel,
    userMessage: Int?
) {
    userMessage?.let {
        val snackbarText = stringResource(it)
        LaunchedEffect(snackbarHostState, viewModel, snackbarText) {
            snackbarHostState.showSnackbar(snackbarText)
            viewModel.snackbarMessageShown()
        }
    }
}

@Composable
fun ShowSnackbarIfNotNull(
    snackbarHostState: SnackbarHostState,
    viewModel: BaseViewModel,
    userMessage: String?
) {
    userMessage?.let {
        LaunchedEffect(snackbarHostState, viewModel, userMessage) {
            snackbarHostState.showSnackbar(userMessage, duration = SnackbarDuration.Short)
            viewModel.snackbarMessageShown()
        }
    }
}