package com.raw.golem_vpn.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material.ScaffoldState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.raw.golem_vpn.R
import com.raw.golem_vpn.viewmodels.AddEditNetworkViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddEditNetworkScreen(
    modifier: Modifier = Modifier,
    viewModel: AddEditNetworkViewModel = hiltViewModel(),
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    val snackbarHostState = remember { SnackbarHostState() }

    Scaffold(
        modifier = modifier.fillMaxSize(),
        snackbarHost = { SnackbarHost(snackbarHostState) },
        floatingActionButton = {
            FloatingActionButton(onClick = viewModel::configureNetwork) {
                Icon(Icons.Filled.Done, stringResource(id = R.string.create_network))
            }
        }
    ) { paddingValues ->
        AddEditNetworkContent(
            paddingValues,
            ip = uiState.ip,
            mask = uiState.mask,
            gateway = uiState.gateway,
            onIpChanged = viewModel::onIpChanged,
            onMaskChanged = viewModel::onMaskChanged,
            onGatewayChanged = viewModel::onGatewayChanged
        )
        // Check for user messages to display on the screen
        uiState.userMessage?.let { userMessage ->
            val snackbarText = stringResource(userMessage)
            LaunchedEffect(snackbarHostState, viewModel, snackbarText) {
                snackbarHostState.showSnackbar(snackbarText)
                viewModel.snackbarMessageShown()
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddEditNetworkContent(
    paddingValues: PaddingValues,
    ip: String = "",
    mask: String = "",
    gateway: String = "",
    onIpChanged: (String) -> Unit,
    onMaskChanged: (String) -> Unit,
    onGatewayChanged: (String) -> Unit,

    ) {
    Column(
        modifier = Modifier
            .padding(paddingValues)
            .fillMaxSize()
            .padding(16.dp)
    ) {
        OutlinedTextField(
            value = ip,
            onValueChange = onIpChanged,
            label = { Text(text = "IP") },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
        OutlinedTextField(
            value = mask,
            onValueChange = onMaskChanged,
            label = { Text(text = "Mask") },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
        OutlinedTextField(
            value = gateway,
            onValueChange = onGatewayChanged,
            label = { Text(text = "Gateway") },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))
    }
}