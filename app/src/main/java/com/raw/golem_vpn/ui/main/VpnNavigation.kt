package com.raw.golem_vpn.ui.main

import androidx.navigation.NavHostController
import com.raw.golem_vpn.ui.main.VpnDestinations.NETWORK_CONFIGURATION_ROUTE
import com.raw.golem_vpn.ui.main.VpnDestinationsArgs.PROPOSAL_ID_ARG
import com.raw.golem_vpn.ui.main.VpnScreens.CREATE_DEMAND_SCREEN
import com.raw.golem_vpn.ui.main.VpnScreens.DEBUG_VPN_CONFIGURATION_SCREEN
import com.raw.golem_vpn.ui.main.VpnScreens.NETWORK_CONFIGURATION_SCREEN
import com.raw.golem_vpn.ui.main.VpnScreens.ON_BOARDING_SCREEN
import com.raw.golem_vpn.ui.main.VpnScreens.PROPOSAL_SCREEN
import com.raw.golem_vpn.ui.main.VpnScreens.VPN_CONFIGURATION_SCREEN
import com.raw.golem_vpn.ui.main.VpnScreens.WS_CONFIGURATION_SCREEN

/**
 * Screens used in [VpnDestinations]
 */
private object VpnScreens {
    const val VPN_CONFIGURATION_SCREEN = "vpn_configuration"
    const val NETWORK_CONFIGURATION_SCREEN = "network_configuration"
    const val DEBUG_VPN_CONFIGURATION_SCREEN = "debug_vpn_configuration"
    const val WS_CONFIGURATION_SCREEN = "ws_configuration"
    const val CREATE_DEMAND_SCREEN = "create_demand"
    const val PROPOSAL_SCREEN = "proposal_screen"
    const val ON_BOARDING_SCREEN = "on_boarding_screen"
}

/**
 * Arguments used in [VpnDestinations] routes
 */
object VpnDestinationsArgs {
    const val PROPOSAL_ID_ARG = "proposal_id"
}

/**
 * Destinations used in the [com.raw.golem_vpn.VpnActivity]
 */
object VpnDestinations {
    const val VPN_CONFIGURATION_ROUTE = VPN_CONFIGURATION_SCREEN
    const val NETWORK_CONFIGURATION_ROUTE = NETWORK_CONFIGURATION_SCREEN
    const val DEBUG_VPN_CONFIGURATION_ROUTE = DEBUG_VPN_CONFIGURATION_SCREEN
    const val WS_CONFIGURATION_ROUTE = WS_CONFIGURATION_SCREEN
    const val CREATE_DEMAND_ROUTE = CREATE_DEMAND_SCREEN
    const val ON_BOARDING_ROUTE = ON_BOARDING_SCREEN
    const val PROPOSAL_DETAIL_ROUTE = "$PROPOSAL_SCREEN?$PROPOSAL_ID_ARG={$PROPOSAL_ID_ARG}"
}

/**
 * Models the navigation actions in the app.
 */

class VpnNavigationActions(private val navController: NavHostController) {
    fun navigateToConfiguration() {
        navController.navigate(VPN_CONFIGURATION_SCREEN)
    }

    fun navigateToAddEditNetwork() {
        navController.navigate(NETWORK_CONFIGURATION_ROUTE)
    }

    fun navigateToDebugVpn() {
        navController.navigate(DEBUG_VPN_CONFIGURATION_SCREEN)
    }

    fun navigateToWsConfiguration() {
        navController.navigate(WS_CONFIGURATION_SCREEN)
    }

    fun navigateToCreateDemand() {
        navController.navigate(CREATE_DEMAND_SCREEN)
    }

    fun navigateToProposalDetails(proposalId: String) {
        navController.navigate("$PROPOSAL_SCREEN?$PROPOSAL_ID_ARG=$proposalId")
    }

    fun navigateToOnBoarding() {
        navController.navigate(ON_BOARDING_SCREEN)
    }
}