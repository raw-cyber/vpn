package com.raw.golem_vpn.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import com.raw.golem_vpn.utils.primaryDarkColor


private val LightColorScheme = lightColorScheme(
    primary = Golem200,
    secondary = Teal200,
)

private val DarkColorScheme = darkColorScheme(
    primary = Golem200,
    secondary = Teal200
)

@Composable
fun VPNOverWSTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colorScheme = if (darkTheme) {
        DarkColorScheme
    } else {
        LightColorScheme
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content,
        shapes = Shapes
    )
}