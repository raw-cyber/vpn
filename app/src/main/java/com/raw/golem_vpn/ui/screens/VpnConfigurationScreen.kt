package com.raw.golem_vpn.ui.screens

import android.Manifest
import android.os.Build
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.NotificationManagerCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.raw.golem_vpn.R
import com.raw.golem_vpn.data.Account
import com.raw.golem_vpn.data.Agreement
import com.raw.golem_vpn.data.BalanceResponse
import com.raw.golem_vpn.data.NetworkTraffic
import com.raw.golem_vpn.ui.components.AppButton
import com.raw.golem_vpn.ui.components.ShowSnackbarIfNotNull
import com.raw.golem_vpn.ui.components.TrafficInfoComponent
import com.raw.golem_vpn.ui.components.formatBytes
import com.raw.golem_vpn.ui.theme.VPNOverWSTheme
import com.raw.golem_vpn.utils.TextUtils
import com.raw.golem_vpn.utils.VpnStatus
import com.raw.golem_vpn.viewmodels.AccountInfoViewModel
import com.raw.golem_vpn.viewmodels.AgreementDetailsViewModel
import com.raw.golem_vpn.viewmodels.BalanceViewModel
import com.raw.golem_vpn.viewmodels.NegotiationsViewModel
import com.raw.golem_vpn.viewmodels.NetworkTrafficCounterViewModel
import com.raw.golem_vpn.viewmodels.VpnViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VpnConfigurationScreen(
    onDebugVpnClick: () -> Unit,
    accountInfoViewModel: AccountInfoViewModel = hiltViewModel(),
    balanceViewModel: BalanceViewModel = hiltViewModel(),
    vpnViewModel: VpnViewModel = hiltViewModel(),
    networkTrafficViewModel: NetworkTrafficCounterViewModel = hiltViewModel(),
    agreementDetailsViewModel: AgreementDetailsViewModel = hiltViewModel()
) {
    val vpnUiState by vpnViewModel.uiState.collectAsStateWithLifecycle()
    val accountUiState by accountInfoViewModel.uiState.collectAsStateWithLifecycle()
    val balanceUiState by balanceViewModel.uiState.collectAsStateWithLifecycle()
    val networkTrafficUiState by networkTrafficViewModel.uiState.collectAsStateWithLifecycle()
    val agreementDetailsUiState by agreementDetailsViewModel.uiState.collectAsStateWithLifecycle()
    val snackbarHostState = remember { SnackbarHostState() }

    LaunchedEffect(Unit) {
        vpnViewModel.runYagna()
    }

    val vpnLauncher =
        rememberLauncherForActivityResult(contract = vpnViewModel.getVpnConfiguration(),
            onResult = {
                vpnViewModel.checkVpnPermissions()
            })

    val areNotificationsEnabled =
        NotificationManagerCompat.from(LocalContext.current).areNotificationsEnabled()

    val notificationsLauncher =
        rememberLauncherForActivityResult(contract = ActivityResultContracts.RequestPermission(),
            onResult = { isGranted ->
                println("Notifications permission granted: $isGranted")
            })
    if (!areNotificationsEnabled) {
        SideEffect {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                notificationsLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(
                        text = stringResource(id = R.string.app_name),
                        maxLines = 1,
                    )
                },
                navigationIcon = {},
                actions = {
                    IconButton(onClick = { onDebugVpnClick() }) {
                        Icon(
                            imageVector = Icons.Filled.Settings,
                            contentDescription = "Localized description"
                        )
                    }
                },
            )
        },
        snackbarHost = { SnackbarHost(snackbarHostState) },
    ) { it ->
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(it)
        ) {
            VpnConfigurationContent(
                paddingValues = it,
                accountInfoContent = {
                    AccountInfoContent(
                        loading = accountUiState.loading,
                        accountInfo = accountUiState.account,
                        onAccountInfoClick = {
                            accountUiState.account?.let { account ->
                                accountInfoViewModel.copyWalletAddress(
                                    account.identity
                                )
                            }
                        }
                    )
                },
                balanceInfoContent = {
                    BalanceInfoContent(
                        loading = balanceUiState.loading,
                        balance = balanceUiState.balance,
                        onBalanceClick = {
                            balanceViewModel.refresh()
                        }
                    )
                },
                vpnStatus = {
                    VpnStatusContent(
                        status = vpnUiState.vpnStatusData.status,
                        statusText = vpnUiState.vpnStatusData.status.toString().lowercase()
                    )
                },
                infoComponent = {
                    InformationComponent(
                        agreement = agreementDetailsUiState.agreement,
                        networkTraffic = networkTrafficUiState.networkTraffic,
                    )
                },
                vpnContent = {
                    if (!vpnUiState.permissionsGranted) {
                        SideEffect {
                            vpnLauncher.launch("com.raw.golem_vpn")
                        }
                    }
                    VpnControlsContent(
                        vpnStatus = vpnUiState.vpnStatusData.status,
                        onVpnClick = {
                            vpnViewModel.connect()

                        },
                        onDisconnectVpnClick = {
                            vpnViewModel.disconnect()
                        },
                    )
                }
            )
        }
        ShowSnackbarIfNotNull(snackbarHostState, vpnViewModel, vpnUiState.userMessage)
    }
}


@Composable
fun VpnConfigurationContent(
    paddingValues: PaddingValues,
    accountInfoContent: @Composable () -> Unit,
    balanceInfoContent: @Composable () -> Unit,
    vpnStatus: @Composable () -> Unit,
    infoComponent: @Composable () -> Unit,
    vpnContent: @Composable () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(paddingValues),
        verticalArrangement = Arrangement.Top,
    ) {
        accountInfoContent()
        balanceInfoContent()
        Spacer(modifier = Modifier.weight(1f, true))
        vpnStatus()
        Spacer(modifier = Modifier.weight(1f, true))
        infoComponent()
        Spacer(modifier = Modifier.weight(1f, true))
        vpnContent()
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AccountInfoContent(
    loading: Boolean = false,
    accountInfo: Account?,
    onAccountInfoClick: () -> Unit,
) {
    if (loading) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp)
        ) {
            CircularProgressIndicator(
                modifier = Modifier
                    .align(Alignment.Center)
            )
        }
    } else {
        ListItem(
            text = {
                if (accountInfo != null) {
                    Text(
                        text = "Wallet: ${TextUtils.shortenText(accountInfo.identity, 32)}",
                        fontSize = 14.sp,
                        fontWeight = FontWeight.ExtraLight,
                    )
                } else {
                    Text(
                        text = "No account data",
                        fontSize = 14.sp,
                        fontWeight = FontWeight.ExtraLight,
                    )
                }
            },
            trailing = {
                Icon(
                    imageVector = Icons.Outlined.Info,
                    contentDescription = "Localized description",
                )
            },
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = onAccountInfoClick)
        )
    }

}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BalanceInfoContent(
    loading: Boolean = false,
    balance: BalanceResponse?,
    onBalanceClick: () -> Unit,
) {
    if (loading) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp)
        ) {
            CircularProgressIndicator(
                modifier = Modifier
                    .align(Alignment.Center)
            )
        }
    } else {
        ListItem(
            text = {
                Text(
                    text = "Balance: ${balance?.getGlmBalance()} ${balance?.token}",
                    fontSize = 14.sp,
                    fontWeight = FontWeight.ExtraLight,
                )
            },
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = onBalanceClick)
        )
        ListItem(
            text = {
                Text(
                    text = "Gas: ${balance?.getGasBalance()} ${balance?.gas?.currency_short_name}",
                    fontSize = 14.sp,
                    fontWeight = FontWeight.ExtraLight,
                )
            },
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = onBalanceClick)
        )
    }
}

@Composable
fun VpnStatusContent(
    status: VpnStatus = VpnStatus.DISCONNECTED,
    statusText: String = "Loading..."
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.2f)
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                if (status in listOf(VpnStatus.DISCONNECTING, VpnStatus.CONNECTING)) {
                    CircularProgressIndicator(color = MaterialTheme.colorScheme.primary)
                }
                Text(
                    text = statusText,
                    color = Color.Black,
                    style = TextStyle(fontSize = 16.sp),
                    modifier = Modifier.padding(top = 8.dp)
                )
            }
        }
    }
}

@Composable
fun InformationComponent(
    localAddress: String? = null,
    remoteAddress: String? = null,
    agreement: Agreement? = null,
    networkTraffic: NetworkTraffic,
) {
    Row(
        modifier = Modifier.height(IntrinsicSize.Max),
        horizontalArrangement = Arrangement.SpaceEvenly,
        verticalAlignment = Alignment.Top

    ) {
        // First card
        TrafficInfoComponent(
            modifier = Modifier
                .weight(1f)
                .padding(16.dp)
                .fillMaxHeight(),
            incomingStream = networkTraffic.incoming,
            outgoingStream = networkTraffic.outgoing,
            incomingTotal = networkTraffic.incomingTotal,
            outgoingTotal = networkTraffic.outgoingTotal,
        )
        // Second card
        Card(
            modifier = Modifier
                .weight(1f)
                .padding(16.dp)
                .fillMaxHeight(),
            elevation = CardDefaults.cardElevation(8.dp)
        ) {
            Column(modifier = Modifier.padding(16.dp)) {
                Text(text = "Provider ID: ${agreement?.offer?.providerId?.let {
                    TextUtils.shortenText(
                        it, 12)
                } ?: "N/A"}")
                Text(text = "Local IP: ${localAddress ?: "N/A"}")
                Text(text = "Remote IP: ${remoteAddress ?: "N/A"}")
            }
        }
    }
}

@Composable
fun VpnControlsContent(
    vpnStatus: VpnStatus,
    onVpnClick: () -> Unit,
    onDisconnectVpnClick: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(vertical = 64.dp),
        verticalArrangement = Arrangement.Bottom,
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceEvenly) {
            val enabled = vpnStatus == VpnStatus.DISCONNECTED
            AppButton(
                modifier = Modifier,
                onClick = { onVpnClick() }, enabled = enabled) {
                Text(
                    text = "Connect to VPN",
                )
            }
            if (!enabled) {
                AppButton(
                    modifier = Modifier,
                    onClick = { onDisconnectVpnClick() }) {
                    Text(
                        text = "Disconnect VPN",
                        fontWeight = FontWeight.Light,
                    )
                }
            }
        }

    }
}

@Preview
@Composable
fun AccountInfoContentPreview() {
    VPNOverWSTheme {
        AccountInfoContent(
            accountInfo = Account(
                "0x0000000",
                "name"
            ),
            onAccountInfoClick = { }
        )
    }
}

@Preview
@Composable
fun LoadingAccountInfoContentPreview() {
    VPNOverWSTheme {
        AccountInfoContent(
            loading = true,
            accountInfo = null,
            onAccountInfoClick = { }
        )
    }
}

@Preview
@Composable
fun InformationContentPreview() {
    VPNOverWSTheme {
        InformationComponent(
            networkTraffic = NetworkTraffic(
                incoming = 0,
                outgoing = 0,
                incomingTotal = 0,
                outgoingTotal = 0,
            )
        )
    }
}