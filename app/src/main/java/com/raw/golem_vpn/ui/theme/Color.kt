package com.raw.golem_vpn.ui.theme

import androidx.compose.ui.graphics.Color

val Golem200 = Color(0xFF3E43B3)
val Golem500 = Color(0xFF141887)
val Golem700 = Color(0xFF05086B)
val Teal200 = Color(0xFF03DAC5)

val Purple200 = Color(0xFF8974E6)
val Purple700 = Color(0xFF211E5A)

val Cyan200 = Color(0xFF34D0BE)
val Cyan500 = Color(0xFF3080F9)

// Text colors
val TextLightPrimary = Color.White