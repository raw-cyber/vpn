import androidx.annotation.DrawableRes
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.raw.golem_vpn.R
import com.raw.golem_vpn.ui.theme.Cyan500

@Composable
fun ImageWithWrapper(
    @DrawableRes imageId: Int,
    wrapperColor: Color,
) {
    Box(
        modifier = Modifier.wrapContentSize()
    ) {
        // Circular box
        Canvas(modifier = Modifier.matchParentSize().blur(100.dp)) {
            drawIntoCanvas { canvas ->
                val radius = size.minDimension / 6
                canvas.drawCircle(center, radius, Paint().apply { color = wrapperColor })
            }
        }
        // Image component
        Image(
            modifier = Modifier
                .wrapContentSize(),
            painter = painterResource(id = imageId),
            contentDescription = "VPN logo",
        )
    }
}

@Preview
@Composable
fun ImageWithWrapperPreview() {
    MaterialTheme {
        ImageWithWrapper(
            imageId = R.drawable.vpn_logo,
            wrapperColor = Cyan500,
        )
    }
}