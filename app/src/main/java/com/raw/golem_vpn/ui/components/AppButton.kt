package com.raw.golem_vpn.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.raw.golem_vpn.ui.theme.Cyan200
import com.raw.golem_vpn.ui.theme.Cyan500
import com.raw.golem_vpn.ui.theme.VPNOverWSTheme

@Composable
fun AppButton(
    modifier: Modifier,
    enabled: Boolean = true,
    onClick: () -> Unit,
    content: @Composable () -> Unit
) {

    Button(
        onClick = onClick,
        modifier = modifier
            .fillMaxWidth()
            .then(modifier),
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.Transparent
        ),
        contentPadding = PaddingValues(),
        enabled = enabled,
        shape = MaterialTheme.shapes.extraSmall
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    brush = Brush.linearGradient(colors = listOf(Cyan200, Cyan500)),
                    shape = MaterialTheme.shapes.extraSmall
                )
                .padding(18.dp),
            contentAlignment = Alignment.Center
        ) {
            content()
        }
    }
}


@Preview
@Composable
fun AppButtonPreview() {
    VPNOverWSTheme {
        AppButton(
            modifier = Modifier,
            onClick = { /*TODO*/ }) {
            Text(text = "Button", style = MaterialTheme.typography.labelMedium)
        }
    }
}