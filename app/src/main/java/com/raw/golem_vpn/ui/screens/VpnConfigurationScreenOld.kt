package com.raw.golem_vpn.ui.screens

import android.Manifest
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.StringRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.NotificationManagerCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.raw.golem_vpn.R
import com.raw.golem_vpn.data.ProposalEvent
import com.raw.golem_vpn.data.ProposalEventGenerator
import com.raw.golem_vpn.ui.components.AppButton
import com.raw.golem_vpn.ui.theme.VPNOverWSTheme
import com.raw.golem_vpn.viewmodels.NegotiationsViewModel
import com.raw.golem_vpn.viewmodels.VpnViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VpnConfigurationScreenOld(
    onCreateDemandClick: () -> Unit,
    onConfigureNetworkClick: () -> Unit,
    onProposalDetailsClick: (ProposalEvent) -> Unit,
    onDebugVpnClick: () -> Unit,
    vpnViewModel: VpnViewModel = hiltViewModel(),
    negotiationsViewModel: NegotiationsViewModel = hiltViewModel(),
) {
    val vpnUiState by vpnViewModel.uiState.collectAsStateWithLifecycle()
    val negotiationsUiState by negotiationsViewModel.uiState.collectAsStateWithLifecycle()
    val snackbarHostState = remember { SnackbarHostState() }

    val vpnAllow = rememberLauncherForActivityResult(contract = vpnViewModel.getVpnConfiguration(),
        onResult = {
            vpnViewModel.checkVpnPermissions()
        })
    val areNotificationsEnabled =
        NotificationManagerCompat.from(LocalContext.current).areNotificationsEnabled()

    val notificationsLauncher =
        rememberLauncherForActivityResult(contract = ActivityResultContracts.RequestPermission(),
            onResult = { isGranted ->
                println("Notifications permission granted: $isGranted")
            })
    if (!areNotificationsEnabled) {
        SideEffect {
            notificationsLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
        }
    }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        snackbarHost = { SnackbarHost(snackbarHostState) },
    ) { it ->
        VpnConfigurationContentOld(
            proposalsContent = {
                ProposalsContent(
                    proposalsLoading = negotiationsUiState.loading,
                    refreshProposals = { negotiationsViewModel.refresh() },
                    onProposalClick = onProposalDetailsClick,
                    proposals = negotiationsUiState.proposals
                )
            },
            permissionsGranted = vpnUiState.permissionsGranted,
            requestPermissions = {
                vpnAllow.launch("com.raw.golem_vpn")
            },
            onDebugVpnClick = onDebugVpnClick,
            vpnIsActivated = vpnUiState.vpnIsActivated,
            yagnaIsRunning = vpnUiState.yagnaIsRunning,
            createDemand = onCreateDemandClick,
            configureNetwork = onConfigureNetworkClick,
            toggleVpn = vpnViewModel::toggleVpn
        )
        negotiationsUiState.userMessage?.let { userMessage ->
            val snackbarText = stringResource(userMessage)
            LaunchedEffect(snackbarHostState, negotiationsViewModel, snackbarText) {
                snackbarHostState.showSnackbar(snackbarText)
                negotiationsViewModel.snackbarMessageShown()
            }
        }
    }
}

@Composable
fun VpnConfigurationContentOld(
    @StringRes title: Int = R.string.app_name,
    proposalsContent: @Composable () -> Unit = {},
    permissionsGranted: Boolean = false,
    vpnIsActivated: Boolean = false,
    yagnaIsRunning: Boolean = false,
    requestPermissions: () -> Unit,
    createDemand: () -> Unit,
    configureNetwork: () -> Unit,
    onDebugVpnClick: () -> Unit,
    toggleVpn: (Boolean) -> Unit,
) {
    Column(
        Modifier
            .padding(all = 20.dp)
            .fillMaxHeight(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Text(text = stringResource(id = title), fontSize = 30.sp, fontWeight = FontWeight.Bold)
        Row(
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.fillMaxHeight(.6f)
            ) {
                proposalsContent()
            }
        }
        Spacer(modifier = Modifier.height(20.dp))
        Row(
            Modifier
                .fillMaxWidth()
                .weight(1f, false),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {

                if (!permissionsGranted) {
                    AppButton(
                        modifier = Modifier.fillMaxWidth(),
                        onClick = requestPermissions,
                    ) {
                        Text(text = "Grant VPN Permissions")
                    }
                }
                AppButton(
                    modifier = Modifier.fillMaxWidth(),
                    onClick = createDemand,
                ) {
                    Text(text = "Create demand")
                }
                AppButton(
                    modifier = Modifier.fillMaxWidth(),
                    onClick = configureNetwork,
                ) {
                    Text(text = "Configure Network")
                }
                AppButton(
                    modifier = Modifier.fillMaxWidth(),
                    onClick = onDebugVpnClick,
                ) {
                    Text(text = "Open DEBUG VPN View")
                }
                Spacer(modifier = Modifier.height(20.dp))
                Row(
                    Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(if (vpnIsActivated) "VPN Connected" else "VPN Disconnected")
                    Switch(
                        checked = vpnIsActivated,
                        onCheckedChange = toggleVpn,
                        colors = SwitchDefaults.colors(
                            checkedThumbColor = MaterialTheme.colorScheme.primary,
                            checkedTrackColor = MaterialTheme.colorScheme.primary
                        )
                    )
                }
                Row(
                    Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(text = "Yagna Status: ")
                    Text(text = if (yagnaIsRunning) "Running" else "Stopped")
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ProposalsContent(
    proposalsLoading: Boolean,
    refreshProposals: () -> Unit,
    onProposalClick: (ProposalEvent) -> Unit,
    proposals: List<ProposalEvent>,
) {
    val state = rememberPullRefreshState(proposalsLoading, refreshProposals)
    Spacer(modifier = Modifier.height(20.dp))
    Box(
        Modifier
            .fillMaxSize()
            .pullRefresh(state), contentAlignment = Alignment.TopCenter
    ) {
        LazyColumn(Modifier.fillMaxWidth()) {
            if (!proposalsLoading) {
                if (proposals.isEmpty()) {
                    item {
                        Text(text = "No proposals found")
                    }
                }
                items(proposals.size) {
                    ListItem {
                        ProposalItem(
                            proposalEvent = proposals[it], onProposalClick = onProposalClick
                        )
                    }
                }
            }
        }
        PullRefreshIndicator(proposalsLoading, state)
    }
}

@Composable
fun ProposalItem(
    proposalEvent: ProposalEvent,
    onProposalClick: (ProposalEvent) -> Unit,
) {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(10.dp)
            .clickable { onProposalClick(proposalEvent) },
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        val proposal = proposalEvent.proposal.proposalId.take(6)
        Text(text = proposal)
        Text(text = proposal + ":" + proposalEvent.proposal.state)
        TextButton(
            onClick = { onProposalClick(proposalEvent) }) {
            Text(text = "Details")
        }
    }
}

@Preview
@Composable
fun ProposalItemPreview() {
    VPNOverWSTheme {
        Surface {
            ProposalItem(
                proposalEvent = ProposalEventGenerator.generateProposals(1)[0],
                onProposalClick = {})
        }
    }
}

@Preview
@Composable
fun ProposalContentPreview() {
    VPNOverWSTheme {
        Surface {
            ProposalsContent(
                proposalsLoading = false,
                refreshProposals = {},
                onProposalClick = {},
                proposals = ProposalEventGenerator.generateProposals(3)
            )
        }
    }
}