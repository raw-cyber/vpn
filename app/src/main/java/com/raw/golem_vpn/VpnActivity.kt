package com.raw.golem_vpn

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.raw.golem_vpn.ui.main.VpnDestinations
import com.raw.golem_vpn.ui.main.VpnNavGraph
import com.raw.golem_vpn.ui.theme.Purple700
import com.raw.golem_vpn.ui.theme.VPNOverWSTheme
import com.raw.golem_vpn.viewmodels.SplashState
import com.raw.golem_vpn.viewmodels.SplashViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class VpnActivity : ComponentActivity() {

    private val viewModel: SplashViewModel by viewModels()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)

        var uiState by mutableStateOf<SplashState>(SplashState.Loading)

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect {
                    uiState = it
                }
            }
        }

        splashScreen.setKeepOnScreenCondition {
            when (uiState) {
                SplashState.Loading -> true
                is SplashState.Success -> false
            }
        }

        setContent {
            VPNOverWSTheme {
                val systemUiController = rememberSystemUiController()
                SideEffect {
                    systemUiController.setStatusBarColor(
                        color = Purple700,
                        darkIcons = false
                    )
                }

                VpnNavGraph(startDestination = startDestination(uiState))
            }
        }
    }

    private fun startDestination(uiState: SplashState): String {
        return when (uiState) {
            SplashState.Loading -> VpnDestinations.VPN_CONFIGURATION_ROUTE
            is SplashState.Success -> uiState.startData.destination
        }
    }

}

