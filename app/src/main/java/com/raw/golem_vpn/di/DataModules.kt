package com.raw.golem_vpn.di

import android.content.Context
import com.raw.golem_vpn.core.vpn.YAGNA_APPKEY
import com.raw.golem_vpn.data.source.*
import com.raw.golem_vpn.data.source.local.NetworkTrafficCounterLocalDataSource
import com.raw.golem_vpn.data.source.remote.AccountRemoteDataSource
import com.raw.golem_vpn.data.source.remote.AgreementRemoteDataSource
import com.raw.golem_vpn.data.source.remote.DemandsRemoteDataSource
import com.raw.golem_vpn.data.source.remote.NetworksRemoteDataSource
import com.raw.golem_vpn.data.source.remote.ProposalsRemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.serialization.json.Json
import org.intellij.lang.annotations.PrintFormat
import javax.inject.Qualifier
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    companion object {
        @Singleton
        @Provides
        fun provideNetworksRepository(
            dataSource: NetworksDataSource,
        ): NetworksRepository {
            return DefaultNetworksRepository(dataSource)
        }

        @Singleton
        @Provides
        fun provideNegotiationsRepository(
            accountDataSource: AccountDataSource,
            agreementDataSource: AgreementDataSource,
            demandDataSource: DemandDataSource,
            proposalsDataSource: ProposalsDataSource,
            networkDataSource: NetworksDataSource,
            vpnStatusManager: VpnStatusManager,
            vpnServiceManager: VpnServiceManager,
            @IoDispatcher dispatcher: CoroutineDispatcher,
            @ApplicationContext applicationContext: Context
        ): AgreementHandlerRepository {
            return DefaultAgreementHandlerRepository(
                accountDataSource,
                agreementDataSource,
                demandDataSource,
                proposalsDataSource,
                networkDataSource,
                vpnStatusManager,
                vpnServiceManager,
                dispatcher,
                applicationContext
            )
        }

        @Provides
        @Singleton
        fun provideDataStoreRepository(
            @ApplicationContext context: Context
        ): DataStoreRepository {
            return DefaultDataStoreRepository(context)
        }
    }
}

@Module
@InstallIn(SingletonComponent::class)
object DataSourceModule {
    @Singleton
    @Provides
    fun provideNetworksRemoteDataSource(client: HttpClient): NetworksDataSource =
        NetworksRemoteDataSource(client)

    @Singleton
    @Provides
    fun provideDemandRemoteDataSource(client: HttpClient): DemandDataSource =
        DemandsRemoteDataSource(client)

    @Singleton
    @Provides
    fun provideProposalsRemoteDataSource(client: HttpClient): ProposalsDataSource =
        ProposalsRemoteDataSource(client)

    @Singleton
    @Provides
    fun provideAccountRemoteDataSource(
        client: HttpClient,
        @ApplicationContext applicationContext: Context
    ): AccountDataSource =
        AccountRemoteDataSource(client, applicationContext)

    @Singleton
    @Provides
    fun provideAgreementRemoteDataSource(
        client: HttpClient,
    ): AgreementDataSource =
        AgreementRemoteDataSource(client)

    @Singleton
    @Provides
    fun provideNetworkTrafficCounterDataSource(
        @ApplicationContext applicationContext: Context
    ): NetworkTrafficCounterDataSource =
        NetworkTrafficCounterLocalDataSource(applicationContext)
}

private const val BASE_URL = "http://127.0.0.1"
private const val PORT = 7465

@Module
@InstallIn(SingletonComponent::class)
object KtorModule {
    @Singleton
    @Provides
    fun provideKtorClient(): HttpClient {
        val client = HttpClient(CIO) {
            install(ContentNegotiation) {
                json(Json {
                    prettyPrint = true
                    ignoreUnknownKeys = true
                    encodeDefaults = true
                })
            }
            defaultRequest {
                url("$BASE_URL:$PORT")
                header("Authorization", "Bearer $YAGNA_APPKEY")
                contentType(ContentType.Application.Json)
            }
        }
        return client
    }
}