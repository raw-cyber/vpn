package com.raw.golem_vpn.di

import com.raw.golem_vpn.core.notifications.DefaultNotificationHelper
import com.raw.golem_vpn.core.notifications.NotificationHelper
import com.raw.golem_vpn.data.source.DefaultVpnServiceManager
import com.raw.golem_vpn.data.source.DefaultVpnStatusManager
import com.raw.golem_vpn.data.source.VpnServiceManager
import com.raw.golem_vpn.data.source.VpnStatusManager
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {
    @Singleton
    @Binds
    abstract fun bindNotificationHelper(notificationHelper: DefaultNotificationHelper): NotificationHelper

    @Singleton
    @Binds
    abstract fun provideVpnServiceManager(vpnServiceManager: DefaultVpnServiceManager): VpnServiceManager

    @Singleton
    @Binds
    abstract fun provideVpnStatusManager(vpnStatusManager: DefaultVpnStatusManager): VpnStatusManager
}