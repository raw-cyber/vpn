package com.raw.golem_vpn.utils

enum class VpnStatus {
    CONNECTED, DISCONNECTED, CONNECTING, DISCONNECTING, ERROR
}

enum class ProposalStatus {
    ACCEPTED, REJECTED, PENDING, NONE
}

enum class AgreementStatus {
    ACCEPTED, FAILED, PENDING, NONE
}

enum class NetworkConfigurationStatus {
    CONFIGURED, NOT_CONFIGURED, REMOVED, PENDING, FAILED
}