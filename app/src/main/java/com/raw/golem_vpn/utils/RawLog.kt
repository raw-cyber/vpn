package com.raw.golem_vpn.utils

import android.util.Log

class RawLog {

    companion object {
        private const val DEBUG = true
        fun d(tag: String, msg: String) {
            if (DEBUG) {
                Log.d(tag, msg)
            }
        }

        fun e(tag: String, msg: String) {
            if (DEBUG) {
                Log.e(tag, msg)
            }
        }

        fun i(tag: String, msg: String) {
            if (DEBUG) {
                Log.i(tag, msg)
            }
        }

        fun v(tag: String, msg: String) {
            if (DEBUG) {
                Log.v(tag, msg)
            }
        }

        fun w(tag: String, msg: String) {
            if (DEBUG) {
                Log.w(tag, msg)
            }
        }

    }
}