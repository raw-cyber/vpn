package com.raw.golem_vpn.utils

import ImageWithWrapper
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.raw.golem_vpn.R
import com.raw.golem_vpn.ui.theme.Cyan500
//import com.raw.golem_vpn.ui.theme.Rajdhani
import com.raw.golem_vpn.ui.theme.interFontFamily

val benefitRows = listOf(
    BenefitRow.First,
    BenefitRow.Second,
    BenefitRow.Third,
)

val featureRows = listOf(
    FeatureRow.First,
    FeatureRow.Second,
    FeatureRow.Third,
)

sealed class FeatureRow(
    @DrawableRes val iconId: Int,
    val title: String,
    val description: String,
) {
    object First : FeatureRow(
        iconId = R.drawable.feature_1,
        title = "Feature 1",
        description = "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters quis ipsum suspendisse ultrices gravida risus commodo viverra",
    )

    object Second : FeatureRow(
        iconId = R.drawable.feature_2,
        title = "Feature 2",
        description = "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters quis ipsum suspendisse ultrices gravida risus commodo viverra",
    )

    object Third : FeatureRow(
        iconId = R.drawable.feature_3,
        title = "Feature 2",
        description = "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters quis ipsum suspendisse ultrices gravida risus commodo viverra",
    )
}

sealed class BenefitRow(
    @DrawableRes val iconId: Int,
    val title: String,
    val description: String,
) {
    object First : BenefitRow(
        iconId = R.drawable.benefit_1,
        title = "Benefit 1",
        description = "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters quis ipsum suspendisse ultrices gravida risus commodo viverra",
    )

    object Second : BenefitRow(
        iconId = R.drawable.benefit_2,
        title = "Benefit 2",
        description = "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters quis ipsum suspendisse ultrices gravida risus commodo viverra",
    )

    object Third : BenefitRow(
        iconId = R.drawable.benefit_3,
        title = "Benefit 3",
        description = "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters quis ipsum suspendisse ultrices gravida risus commodo viverra",
    )
}


sealed class OnBoardingPage(
    val buttonText: String,
    val content: @Composable () -> Unit = {}
) {
    object First : OnBoardingPage(
        buttonText = "Find out more",
        content = {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                Text(
                    "RAW VPN",
                    style = MaterialTheme.typography.headlineLarge.copy(
                        fontWeight = FontWeight.Bold,
                        fontFamily = interFontFamily,
                    )
                )
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.padding(top = 0.dp)
                ) {
                    Text("Powered by", style = MaterialTheme.typography.labelSmall)
                    Image(
                        modifier = Modifier.padding(start = 6.dp, top = 8.dp),
                        painter = painterResource(id = R.drawable.golem_logo),
                        contentDescription = "Golem network logo"
                    )
                }
                ImageWithWrapper(
                    imageId = R.drawable.vpn_logo,
                    wrapperColor = Cyan500,
                )
                Text(
                    modifier = Modifier.padding(top = 16.dp, start = 36.dp, end = 36.dp),
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.bodyMedium,
                    text = "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters quis ipsum suspendisse ultrices gravida risus commodo viverra",
                )
            }
        }
    )

    //    TODO - move content to component
    object Second : OnBoardingPage(
        buttonText = "Next",
        content = {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                Text(
                    modifier = Modifier.padding(bottom = 54.dp),
                    style = MaterialTheme.typography.headlineSmall,
                    text = "Features"
                )
                repeat(featureRows.size) { iteration ->
                    Row(modifier = Modifier.padding(horizontal = 16.dp, vertical = 24.dp)) {
                        Column {
                            ResizableImage(
                                modifier = Modifier.padding(end = 16.dp),
                                imageResId = featureRows[iteration].iconId,
                                imageSize = 75.dp,
                                contentDescription = "Feature icon: ${featureRows[iteration].title}",
                            )
                        }
                        Column {
                            Text(
                                style = MaterialTheme.typography.titleMedium,
                                text = featureRows[iteration].title
                            )
                            Text(
                                style = MaterialTheme.typography.bodySmall,
                                text = featureRows[iteration].description
                            )
                        }
                    }
                }
            }
        }
    )

    object Third : OnBoardingPage(
        buttonText = "Go to App",
        content = {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                Text(
                    modifier = Modifier.padding(bottom = 54.dp),
                    style = MaterialTheme.typography.headlineSmall,
                    text = "Benefits"
                )
                repeat(benefitRows.size) { iteration ->
                    Row(modifier = Modifier.padding(horizontal = 16.dp, vertical = 24.dp)) {
                        Column {
                            ResizableImage(
                                modifier = Modifier.padding(end = 16.dp),
                                imageResId = benefitRows[iteration].iconId,
                                imageSize = 75.dp,
                                contentDescription = "Benefit icon: ${benefitRows[iteration].title}",
                            )
                        }
                        Column {
                            Text(
                                style = MaterialTheme.typography.titleMedium,
                                text = benefitRows[iteration].title
                            )
                            Text(
                                style = MaterialTheme.typography.bodySmall,
                                text = benefitRows[iteration].description
                            )
                        }
                    }
                }
            }
        }
    )
}

@Preview
@Composable
fun OnBoardingFirstPagePreview() {
    MaterialTheme {
        OnBoardingPage.First.content()
    }
}

@Preview
@Composable
fun OnBoardingSecondPagePreview() {
    MaterialTheme {
        OnBoardingPage.Second.content()
    }
}

@Preview
@Composable
fun OnBoardingThirdPagePreview() {
    MaterialTheme {
        OnBoardingPage.Third.content()
    }
}
