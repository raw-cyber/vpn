package com.raw.golem_vpn.utils

class TextUtils {
    companion object {
        fun shortenText(text: String, maxLength: Int): String {
            return if (text.length > maxLength) {
                text.substring(0, maxLength) + "..."
            } else {
                text
            }
        }
    }
}