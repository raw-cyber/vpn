package com.raw.golem_vpn.utils

class DoubleUtils {
    companion object {
        fun roundDouble(value: Double, places: Int): Double {
            var value = value
            if (places < 0) throw IllegalArgumentException()
            var factor = 1.0
            for (i in 0 until places) {
                factor *= 10.0
            }
            value *= factor
            val tmp = value.toLong()
            return tmp.toDouble() / factor
        }
    }
}