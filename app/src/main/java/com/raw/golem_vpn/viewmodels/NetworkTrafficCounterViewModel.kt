package com.raw.golem_vpn.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.R
import com.raw.golem_vpn.data.NetworkTraffic
import com.raw.golem_vpn.data.source.NetworkTrafficCounterDataSource
import com.raw.golem_vpn.utils.Async
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

data class NetworkTrafficCounterUiState(
    val networkTraffic: NetworkTraffic = NetworkTraffic(0, 0, 0, 0),
    val loading: Boolean = false,
    val userMessage: Int? = null
)

@HiltViewModel
class NetworkTrafficCounterViewModel @Inject constructor(
    networkTrafficCounterDataSource: NetworkTrafficCounterDataSource
) : ViewModel() {

    private val _isLoading = MutableStateFlow(false)
    private val _networkTrafficCounterAsync =
        networkTrafficCounterDataSource.getNetworkTrafficStream()
            .map { Async.Success(it) }
            .catch<Async<NetworkTraffic>> { Async.Error(R.string.loading_error) }

    val uiState = combine(_isLoading, _networkTrafficCounterAsync) { loading, networkTrafficAsync ->
        when (networkTrafficAsync) {
            Async.Loading -> NetworkTrafficCounterUiState(loading = true)
            is Async.Error -> NetworkTrafficCounterUiState(userMessage = networkTrafficAsync.errorMessage)
            is Async.Success -> NetworkTrafficCounterUiState(
                networkTraffic = networkTrafficAsync.data,
                loading = loading
            )
        }
    }.stateIn(viewModelScope, SharingStarted.Lazily, NetworkTrafficCounterUiState())
}