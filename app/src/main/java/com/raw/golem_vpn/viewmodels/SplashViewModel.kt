package com.raw.golem_vpn.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.data.source.DataStoreRepository
import com.raw.golem_vpn.ui.main.VpnDestinations
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    repository: DataStoreRepository
) : ViewModel() {

    val uiState: StateFlow<SplashState> = repository.onBoardingState().map {
        SplashState.Success(
            StartDestination(
                if (it) VpnDestinations.VPN_CONFIGURATION_ROUTE else VpnDestinations.ON_BOARDING_ROUTE
            )
        )
    }.stateIn(
        scope = viewModelScope,
        initialValue = SplashState.Loading,
        started = SharingStarted.WhileSubscribed(5_000),
    )

}

data class StartDestination(val destination: String)

sealed interface SplashState {
    object Loading : SplashState
    data class Success(val startData: StartDestination) : SplashState
}