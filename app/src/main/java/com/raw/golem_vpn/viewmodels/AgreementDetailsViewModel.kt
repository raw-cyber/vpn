package com.raw.golem_vpn.viewmodels

import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.R
import com.raw.golem_vpn.data.Agreement
import com.raw.golem_vpn.data.source.AgreementDataSource
import com.raw.golem_vpn.utils.Async
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

data class AgreementDetailsUiState(
    val agreement: Agreement? = null,
    val loading: Boolean = false,
    val userMessage: Int? = null
)

@HiltViewModel
class AgreementDetailsViewModel @Inject constructor(
    private val agreementDataSource: AgreementDataSource
) : BaseViewModel() {
    private val _isLoading = MutableStateFlow(false)
    private val _userMessage: MutableStateFlow<Int?> = MutableStateFlow(null)

    private val _agreementDetailsAsync = agreementDataSource.getAgreementStream()
        .map { Async.Success(it) }
        .catch<Async<Agreement?>> { emit(Async.Error(R.string.loading_error)) }

    val uiState = combine(
        _isLoading,
        _userMessage,
        _agreementDetailsAsync
    ) { isLoading, userMessage, agreementDetails ->
        when (agreementDetails) {
            Async.Loading -> AgreementDetailsUiState(loading = true)
            is Async.Success -> AgreementDetailsUiState(
                agreement = agreementDetails.data,
                loading = isLoading,
                userMessage = userMessage
            )

            is Async.Error -> AgreementDetailsUiState(userMessage = agreementDetails.errorMessage)
        }
    }.stateIn(viewModelScope, SharingStarted.Lazily, AgreementDetailsUiState(loading = true))

    override fun snackbarMessageShown() {
        _userMessage.value = null
    }
}