package com.raw.golem_vpn.viewmodels

import android.app.Application
import androidx.activity.result.contract.ActivityResultContract
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.R
import com.raw.golem_vpn.core.vpn.CommandManagerConstructor
import com.raw.golem_vpn.data.DemandProperties
import com.raw.golem_vpn.data.DemandRequest
import com.raw.golem_vpn.data.Network
import com.raw.golem_vpn.data.ProposalStatusData
import com.raw.golem_vpn.data.VpnPreferences
import com.raw.golem_vpn.data.VpnStatusData
import com.raw.golem_vpn.data.source.AccountDataSource
import com.raw.golem_vpn.data.source.AgreementHandlerRepository
import com.raw.golem_vpn.data.source.VpnPreferenceKeys
import com.raw.golem_vpn.data.source.VpnServiceManager
import com.raw.golem_vpn.data.source.VpnStatusManager
import com.raw.golem_vpn.utils.RawLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.BufferedReader
import java.io.InputStreamReader
import java.time.LocalDateTime
import java.time.ZoneId
import javax.inject.Inject

// Used to save the current vpn connection status in SavedStateHandle.
const val VPN_SAVED_STATE_KEY = "VPN_SAVED_STATE_KEY"

data class VpnUiState(
    val isLoading: Boolean = false,
    val userMessage: String? = null,
    val address: String = "",
    val vpnIsActivated: Boolean = false,
    val yagnaIsRunning: Boolean = false,
    val vpnStatusData: VpnStatusData = VpnStatusData(),
    val proposalStatusData: ProposalStatusData = ProposalStatusData(),
    val permissionsGranted: Boolean = true,
    val vpnData: VpnPreferences = VpnPreferences(),
)

@HiltViewModel
class VpnViewModel @Inject constructor(
    private val coroutineScope: CoroutineScope,
    private val vpnServiceManager: VpnServiceManager,
    private val accountDataSource: AccountDataSource,
    private val agreementHandlerRepository: AgreementHandlerRepository,
    vpnStatusManager: VpnStatusManager,
) : BaseViewModel() {

    private val _vpnPreferencesAsync = vpnServiceManager.vpnPreferencesFlow
    private val _yagnaStatusAsync = vpnServiceManager.yagnaFlow
    private val _vpnStatusAsync = vpnStatusManager.getVpnStatusStream()

    private val _userMessagesFlow = combine(
        vpnStatusManager.getAgreementStatusStream(),
        vpnStatusManager.getProposalStatusStream()
    ) { agreementStatus, proposalStatus ->
        agreementStatus.message ?: proposalStatus.message
    }

    private val _uiState = MutableStateFlow(VpnUiState(isLoading = true))

    val uiState: StateFlow<VpnUiState> = combine(
        _uiState,
        _yagnaStatusAsync.asLiveData().asFlow(),
        _vpnStatusAsync,
        _userMessagesFlow,
        _vpnPreferencesAsync
    ) { state, yagnaStatus, vpnStatus, userMessages, vpnPreferencesAsync ->
        VpnUiState(
            address = state.address,
            isLoading = false,
            yagnaIsRunning = yagnaStatus.isRunning,
            vpnIsActivated = state.vpnIsActivated,
            permissionsGranted = state.permissionsGranted,
            vpnStatusData = vpnStatus,
            vpnData = vpnPreferencesAsync,
            userMessage = userMessages,
        )
    }.stateIn(viewModelScope, SharingStarted.Lazily, VpnUiState(isLoading = true))


    init {
        viewModelScope.launch {
            combine(_vpnPreferencesAsync, _yagnaStatusAsync) { vpnPrefs, yagnaStatus ->
                _uiState.update {
                    it.copy(
                        address = vpnPrefs.address,
                        isLoading = false,
                        yagnaIsRunning = yagnaStatus.isRunning,
                    )
                }
            }
            checkVpnPermissions()
        }
    }

    fun checkVpnPermissions() {
        val permissionsGranted = vpnServiceManager.checkVpnPermissions()
        _uiState.update {
            it.copy(permissionsGranted = permissionsGranted)
        }
    }

    fun getVpnConfiguration(): ActivityResultContract<String, Boolean> {
        return vpnServiceManager.getVpnPermissionAction()
    }

    fun saveConfiguration() = viewModelScope.launch {
        if (uiState.value.address.isNotEmpty()) {
            vpnServiceManager.updatePreference(VpnPreferenceKeys.ADDRESS, uiState.value.address)
        } else {
//            _uiState.update { it.copy(userMessage = R.string.empty_configuration) }
            RawLog.d("Tag", "Empty configuration")
        }
    }

    fun updateAddress(newIp: String) {
        _uiState.update {
            it.copy(address = newIp)
        }
    }

    fun connect() {
        coroutineScope.launch {
            val subnet = DEFAULT_VPN.toString().lowercase()

            val nineHoursLater = LocalDateTime.now().plusHours(9)
            val instant = nineHoursLater.atZone(ZoneId.systemDefault()).toInstant()

            // convert to milliseconds
            val unixTimeStamp = instant.epochSecond * 1000

            // get sender address
            val senderAddress = accountDataSource.getAccountInfo()?.identity ?: ""

            val demand = DemandRequest(
                properties = DemandProperties(
                    acceptTimeout = 240,
                    subnet = subnet,
                    chosenPlatform = "erc20-rinkeby-tglm",
                    senderAddress = senderAddress,
                    expiration = unixTimeStamp
                ),
                constraints = DemandRequest.getConstraints(
                    subnet
                )
            )
            agreementHandlerRepository.findProvider(demand)
        }
    }

    fun disconnect() {
        vpnServiceManager.disconnectFromVpn()
    }

    fun runYagna() {
        vpnServiceManager.startVpnService()
    }

    fun toggleVpn(value: Boolean) {
        _uiState.update {
            it.copy(vpnIsActivated = value)
        }
        if (uiState.value.vpnIsActivated) {
            connect()
        } else {
            disconnect()
        }
    }

    fun initPayment() {
        viewModelScope.launch {
            accountDataSource.initPayment()
        }
    }

    fun fundAccount() {
        viewModelScope.launch {
            accountDataSource.fundAccount()
        }
    }

    override fun snackbarMessageShown() {
        _uiState.update {
            it.copy(userMessage = null)
        }
    }

}