package com.raw.golem_vpn.viewmodels

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.data.AgreementRequest
import com.raw.golem_vpn.data.Allocation
import com.raw.golem_vpn.data.ProposalEvent
import com.raw.golem_vpn.data.source.AgreementHandlerRepository
import com.raw.golem_vpn.data.source.VpnServiceManager
import com.raw.golem_vpn.ui.main.VpnDestinationsArgs
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import javax.inject.Inject

/**
 * UiState for the Details screen.
 */
data class ProposalEventDetailUiState(
    val proposalEvent: ProposalEvent? = null,
    val proposalDrafts: List<ProposalEvent> = emptyList(),
    val isLoading: Boolean = false,
    val userMessage: Int? = null,
    val isTaskDeleted: Boolean = false
)

/**
 * ViewModel for the Details screen.
 */
@HiltViewModel
class ProposalDetailsViewModel @Inject constructor(
    private val agreementHandlerRepository: AgreementHandlerRepository,
    private val vpnServiceManager: VpnServiceManager,
    savedStateHandle: SavedStateHandle,
) : BaseViewModel() {

    private val proposalId: String = savedStateHandle[VpnDestinationsArgs.PROPOSAL_ID_ARG]!!
    private val _userMessage: MutableStateFlow<Int?> = MutableStateFlow(null)
    private val _isLoading = MutableStateFlow(false)
    private val _proposalAsync =
        agreementHandlerRepository.getProposalStream(proposalId).map { it }.catch { }

    private val _draftsAsync = agreementHandlerRepository.getProposalsStream()
        .map { it.filter { proposalEvent -> proposalEvent.proposal.state == "Draft" } }.catch { }
    val uiState: StateFlow<ProposalEventDetailUiState> = combine(
        _userMessage, _isLoading, _proposalAsync, _draftsAsync
    ) { message, loading, proposalEvent, drafts ->
        ProposalEventDetailUiState(
            proposalEvent = proposalEvent,
            isLoading = loading,
            userMessage = message,
            proposalDrafts = drafts
        )
    }.stateIn(viewModelScope, SharingStarted.Lazily, ProposalEventDetailUiState())

    fun sendCounterProposal() {
        viewModelScope.launch {
            _isLoading.value = true
            agreementHandlerRepository.makeCounterProposal(proposalId)
            val ret = agreementHandlerRepository.demandId.take(1).first()
            agreementHandlerRepository.refreshProposals(ret)
            _isLoading.value = false
        }
    }

    fun onProposalDraftClicked(proposalEvent: ProposalEvent) {
//        viewModelScope.launch {
//            val agreementValidTo =
//                LocalDateTime.now(ZoneOffset.UTC).plusSeconds(60)
//
//            // Format the current time as a string in the desired format
//            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'")
//
//            val agreementValidToFormatted = agreementValidTo.format(formatter).toString()
//            val agreementRequest = AgreementRequest(proposalEvent.proposal.proposalId, agreementValidToFormatted)
//
//
//            val allocationValidaTo = LocalDateTime.now(ZoneOffset.UTC).plusHours(24)
//            val allocationValidToFormatted = allocationValidaTo.format(formatter).toString()
//            val identity = vpnServiceManager.yagnaFlow.take(1).first().identity
//            val allocation = Allocation(
//                address = identity,
//                totalAmount = "10",
//                timeout = allocationValidToFormatted
//            )
//            _isLoading.value = true
//            agreementHandlerRepository.makeAgreement(allocation, agreementRequest)
//            _isLoading.value = false
//        }
    }

    override fun snackbarMessageShown() {
        _userMessage.value = null
    }
}