package com.raw.golem_vpn.viewmodels

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {
    abstract fun snackbarMessageShown()
}