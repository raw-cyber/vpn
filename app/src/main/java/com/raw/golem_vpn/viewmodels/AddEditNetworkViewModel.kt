package com.raw.golem_vpn.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.R
import com.raw.golem_vpn.data.Network
import com.raw.golem_vpn.data.createBaseExecCommand
import com.raw.golem_vpn.data.source.LOCAL_IP
import com.raw.golem_vpn.data.source.NetworksRepository
import com.raw.golem_vpn.data.source.REMOTE_IP
import com.raw.golem_vpn.data.source.VpnPreferenceKeys
import com.raw.golem_vpn.data.source.VpnServiceManager
import com.raw.golem_vpn.di.IoDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import javax.inject.Inject

data class AddEditNetworkUiState(
    val ip: String = "192.168.8.0/24",
    val mask: String = "255.255.255.0",
    val gateway: String = "192.168.8.7",
    val isLoading: Boolean = false,
    val userMessage: Int? = null,
    val isNetworkSaved: Boolean = false
)


@HiltViewModel
class AddEditNetworkViewModel @Inject constructor(
    private val networksRepository: NetworksRepository,
    private val vpnServiceManager: VpnServiceManager,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : ViewModel() {
    private val _uiState = MutableStateFlow(AddEditNetworkUiState(isLoading = true))
    val uiState: StateFlow<AddEditNetworkUiState> = _uiState.asStateFlow()

    fun configureNetwork() = viewModelScope.launch(ioDispatcher) {
        if (uiState.value.ip.isEmpty() || uiState.value.mask.isEmpty() || uiState.value.gateway.isEmpty()) {
            _uiState.update {
                it.copy(userMessage = R.string.empty_network_fields)
            }
        } else {
            val ret: Network? = networksRepository.configureNetwork(
                uiState.value.ip, uiState.value.mask, uiState.value.gateway
            )
            if (ret == null) {
                _uiState.update {
                    it.copy(userMessage = R.string.network_not_created)
                }
            } else {
//                TODO - Move this from UI layer to repository
                vpnServiceManager.agreementFlow.take(1).collect { networkActivityPreferences ->
                    val localIp = LOCAL_IP
                    val remoteIp = REMOTE_IP

                    val execCommand = createBaseExecCommand(ret.id, localIp, remoteIp)
                    val command = Json.encodeToJsonElement(execCommand).toString()
                    networksRepository.execCommandInActivity(
                        networkActivityPreferences.activityId, command
                    )
                    val providerId = vpnServiceManager.agreementFlow.take(1).first().providerId
                    println("Provider id: $providerId")
                    val networkAssignmentRes = networksRepository.assignIpToNetwork(
                        ret.id,
                        providerId,
                        remoteIp
                    )
                    println("Network assignment result: $networkAssignmentRes")
                    val networkAddress =
                        "ws://127.0.0.1:7465/net-api/v2/vpn/net/${ret.id}/raw/from/${localIp}/to/${remoteIp}"
                    vpnServiceManager.updatePreference(VpnPreferenceKeys.ADDRESS, networkAddress)
                    _uiState.update {
                        it.copy(userMessage = R.string.network_created)
                    }
                }
            }
        }
    }

    fun onIpChanged(ip: String) = viewModelScope.launch {
        _uiState.update {
            it.copy(ip = ip)
        }
    }

    fun onMaskChanged(mask: String) = viewModelScope.launch {
        _uiState.update {
            it.copy(mask = mask)
        }
    }

    fun onGatewayChanged(gateway: String) = viewModelScope.launch {
        _uiState.update {
            it.copy(gateway = gateway)
        }
    }


    fun snackbarMessageShown() {
        _uiState.update {
            it.copy(userMessage = null)
        }
    }
}