package com.raw.golem_vpn.viewmodels

import android.app.Application
import android.content.ClipboardManager
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.R
import com.raw.golem_vpn.data.Account
import com.raw.golem_vpn.data.source.AccountDataSource
import com.raw.golem_vpn.di.IoDispatcher
import com.raw.golem_vpn.utils.Async
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject


data class AccountInfoUiState(
    val account: Account? = null,
    val loading: Boolean = false,
    val userMessage: Int? = null
)

@HiltViewModel
class AccountInfoViewModel @Inject constructor(
    private val application: Application,
    private val accountDataSource: AccountDataSource,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : BaseViewModel() {
    private val _isLoading = MutableStateFlow(false)
    private val _userMessage: MutableStateFlow<Int?> = MutableStateFlow(null)

    private val _accountInfoAsync = accountDataSource.getAccountInfoStream()
        .map { Async.Success(it) }
        .catch<Async<Account?>> { emit(Async.Error(R.string.loading_account_info_error)) }

    val uiState = combine(
        _isLoading,
        _userMessage,
        _accountInfoAsync
    ) { loading, userMessages, accountInfoAsync ->
        when (accountInfoAsync) {
            Async.Loading -> AccountInfoUiState(loading = true)
            is Async.Success -> AccountInfoUiState(
                account = accountInfoAsync.data,
                loading = loading,
                userMessage = userMessages
            )

            is Async.Error -> AccountInfoUiState(userMessage = accountInfoAsync.errorMessage)
        }
    }.stateIn(viewModelScope, SharingStarted.Lazily, AccountInfoUiState(loading = true))

    fun refresh() {
        _isLoading.value = true
        viewModelScope.launch(ioDispatcher) {
            accountDataSource.refreshAccountInfo()
            _isLoading.value = false
        }
    }

    fun copyWalletAddress(walletAddress: String) {
        val clipboardManager =
            application.applicationContext.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = android.content.ClipData.newPlainText("walletAddress", walletAddress)
        // Copy text to clipboard
        clipboardManager.setPrimaryClip(clipData)
        _userMessage.value = R.string.wallet_address_copied
    }

    override fun snackbarMessageShown() {
        _userMessage.value = null
    }
}