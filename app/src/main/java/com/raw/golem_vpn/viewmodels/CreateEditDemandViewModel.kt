package com.raw.golem_vpn.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.data.DemandProperties
import com.raw.golem_vpn.data.DemandRequest
import com.raw.golem_vpn.data.source.AgreementHandlerRepository
import com.raw.golem_vpn.di.IoDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import javax.inject.Inject

enum class Subnet {
    VPN, PUBLIC, VPN_1, OUTBOUND
}

val DEFAULT_VPN = Subnet.VPN_1

data class CreateEditDemandUiState(
    val acceptTimeout: Int = 240,
    val subnet: Subnet = DEFAULT_VPN,
    val chosenPlatform: String = "erc20-rinkeby-tglm",
    val senderAddress: String = "0x7f78ea04c4803373a5281a6c593706fef5107f8f",
    val expirationDate: String = "",
    val expirationTime: String = "",
    val loading: Boolean = false,
    val userMessage: Int? = null,
    val isNetworkSaved: Boolean = false
)

@HiltViewModel
class CreateEditDemandViewModel @Inject constructor(
    private val agreementHandlerRepository: AgreementHandlerRepository,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : ViewModel() {
    private val _uiState = MutableStateFlow(CreateEditDemandUiState())
    val uiState = _uiState

    fun createDemand() {
        if (validate()) {
            viewModelScope.launch(ioDispatcher) {
                val subnet: String = uiState.value.subnet.toString().lowercase()
                val unixTimestamp = getUnixTimestamp()
                val demand = DemandRequest(
                    properties = DemandProperties(
                        acceptTimeout = uiState.value.acceptTimeout,
                        subnet = subnet,
                        chosenPlatform = uiState.value.chosenPlatform,
                        senderAddress = uiState.value.senderAddress,
                        expiration = unixTimestamp
                    ),
                    constraints = DemandRequest.getConstraints(
                        subnet
                    )
                )
                agreementHandlerRepository.findProvider(
                    demand
                )
            }
        }
    }

    private fun getUnixTimestamp(): Long {
        val dateTimeStr = uiState.value.expirationDate + " " + uiState.value.expirationTime
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
        val localDateTime = LocalDateTime.parse(dateTimeStr, formatter)
        val instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant()
        return instant.epochSecond * 1000 // convert to milliseconds
    }

    private fun validate(): Boolean {
        return true
    }

    fun updateAcceptTimeout(acceptTimeout: Int) {
        _uiState.update {
            it.copy(acceptTimeout = acceptTimeout)
        }
    }

    fun updateSubnet(subnet: Subnet) {
        _uiState.update {
            it.copy(subnet = subnet)
        }
    }

    fun updateChosenPlatform(chosenPlatform: String) {
        _uiState.update {
            it.copy(chosenPlatform = chosenPlatform)
        }
    }

    fun updateExpirationDate(expirationDate: String) {
        _uiState.update {
            it.copy(expirationDate = expirationDate)
        }
    }

    fun updateExpirationTime(expirationString: String) {
        _uiState.update {
            it.copy(expirationTime = expirationString)
        }
    }

}
