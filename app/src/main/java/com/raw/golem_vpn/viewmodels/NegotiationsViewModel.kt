package com.raw.golem_vpn.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.R
import com.raw.golem_vpn.data.ProposalEvent
import com.raw.golem_vpn.data.source.AgreementHandlerRepository
import com.raw.golem_vpn.di.IoDispatcher
import com.raw.golem_vpn.utils.Async
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

data class NegotiationsUiState(
    val proposals: List<ProposalEvent> = emptyList(),
    val loading: Boolean = false,
    val userMessage: Int? = null
)

@HiltViewModel
class NegotiationsViewModel @Inject constructor(
    private val agreementHandlerRepository: AgreementHandlerRepository,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : ViewModel() {
    private val _isLoading = MutableStateFlow(false)
    private val _userMessage: MutableStateFlow<Int?> = MutableStateFlow(null)
    private val _proposalsAsync =
        agreementHandlerRepository.getProposalsStream()
            .map { proposalsEvents ->
                val initEvents = proposalsEvents.filter { proposalEvent ->
                    proposalEvent.proposal.state == "Initial"
                }
                Async.Success(initEvents)
            }
            .catch<Async<List<ProposalEvent>>> { emit(Async.Error(R.string.loading_proposal_events_error)) }
    val uiState = combine(
        _isLoading,
        _proposalsAsync,
        _userMessage
    ) { loading, proposalsAsync, userMessage ->
        when (proposalsAsync) {
            Async.Loading -> NegotiationsUiState(loading = true)
            is Async.Success -> NegotiationsUiState(
                proposals = proposalsAsync.data,
                loading = loading,
                userMessage = userMessage
            )
            is Async.Error -> NegotiationsUiState(userMessage = proposalsAsync.errorMessage)
        }
    }.stateIn(viewModelScope, SharingStarted.Lazily, NegotiationsUiState())

    fun refresh() {
        _isLoading.value = true
        viewModelScope.launch(ioDispatcher) {
            agreementHandlerRepository.demandId.take(1).collect {
                agreementHandlerRepository.refreshProposals(it)
                _isLoading.value = false
            }
        }
    }

    fun snackbarMessageShown() {
        _userMessage.value = null
    }

}
