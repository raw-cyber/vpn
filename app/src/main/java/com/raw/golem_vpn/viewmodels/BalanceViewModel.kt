package com.raw.golem_vpn.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.R
import com.raw.golem_vpn.data.BalanceResponse
import com.raw.golem_vpn.data.source.AccountDataSource
import com.raw.golem_vpn.di.IoDispatcher
import com.raw.golem_vpn.utils.Async
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

data class BalanceUiState(
    val balance: BalanceResponse? = null,
    val loading: Boolean = false,
    val userMessage: Int? = null
)

@HiltViewModel
class BalanceViewModel @Inject constructor(
    private val accountDataSource: AccountDataSource,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : ViewModel() {
    private val _isLoading = MutableStateFlow(false)
    private val _userMessage: MutableStateFlow<Int?> = MutableStateFlow(null)
    private val _balanceInfoAsync = accountDataSource.getBalanceStream()
        .map { Async.Success(it) }
        .catch<Async<BalanceResponse?>> { emit(Async.Error(R.string.loading_balance_error)) }

    val uiState = combine(
        _isLoading,
        _userMessage,
        _balanceInfoAsync
    ) { loading, userMessages, balanceInfoAsync ->
        when (balanceInfoAsync) {
            Async.Loading -> BalanceUiState(loading = true)
            is Async.Success -> BalanceUiState(
                balance = balanceInfoAsync.data,
                loading = loading,
                userMessage = userMessages
            )

            is Async.Error -> BalanceUiState(userMessage = balanceInfoAsync.errorMessage)
        }
    }.stateIn(viewModelScope, SharingStarted.Lazily, BalanceUiState(loading = true))

    fun refresh() {
        _isLoading.value = true
        viewModelScope.launch(ioDispatcher) {
            accountDataSource.refreshBalance()
            _isLoading.value = false
        }
    }

    fun snackbarMessageShown() {
        _userMessage.value = null
    }
}