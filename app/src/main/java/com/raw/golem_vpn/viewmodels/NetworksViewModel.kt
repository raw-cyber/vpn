package com.raw.golem_vpn.viewmodels

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raw.golem_vpn.R
import com.raw.golem_vpn.data.Network
import com.raw.golem_vpn.data.source.NetworksRepository
import com.raw.golem_vpn.di.IoDispatcher
import com.raw.golem_vpn.utils.Async
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.net.ConnectException
import javax.inject.Inject


data class NetworksUiState(
    val items: List<Network> = emptyList(),
    val loading: Boolean = false,
    val userMessage: Int? = null
)

@HiltViewModel
class NetworksViewModel @Inject constructor(
    private val networksRepository: NetworksRepository,
    private val savedStateHandle: SavedStateHandle,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : ViewModel() {
    private val _isLoading = MutableStateFlow(false)
    private val _networksAsync = networksRepository.getNetworksStream()
        .map { Async.Success(it) }
        .catch<Async<List<Network>>> { Async.Error(R.string.loading_error) }

    val uiState: StateFlow<NetworksUiState> = combine(
        _networksAsync,
        _isLoading
    ) { networksAsync, loading ->
        when (networksAsync) {
            Async.Loading -> NetworksUiState(loading = true)
            is Async.Error -> NetworksUiState(userMessage = networksAsync.errorMessage)
            is Async.Success -> NetworksUiState(items = networksAsync.data, loading = loading)
        }

    }.stateIn(viewModelScope, SharingStarted.Lazily, NetworksUiState())

    fun refreshNetworks() {
        _isLoading.value = true
        viewModelScope.launch(ioDispatcher) {
            try {
                networksRepository.refreshNetworks()
            } catch (e: ConnectException) {
                e.printStackTrace()
            }
            _isLoading.value = false
        }
    }
}

